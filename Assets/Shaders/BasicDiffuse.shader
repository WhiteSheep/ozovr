﻿Shader "Custom/BasicDiffuse" {
	Properties {
		_EmissiveColor ("Emissve Color", Color) = (1, 1, 1, 1) 
		_AmbientColor ("Ambient Color", Color) = (1, 1, 1, 1)
		_DiffuseColor ("Diffuse Color", Color) = (1, 1, 1, 1)
		_RampTex("Ramp Texture", 2D) = "white"{}

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf BasicDiffuse
		
		//Standard fullforwardshadows
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0


		float4 _EmissiveColor;
		float4 _AmbientColor;
		float4 _DiffuseColor;
		sampler2D _RampTex;

		inline float4 LightingBasicDiffuse (SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float difLight = max(0, dot(lightDir, s.Normal));
			float rimLight = dot(s.Normal, viewDir);
			float hLambert = difLight * 0.5 + 0.5;
			float3 ramp = tex2D(_RampTex, float2(hLambert, rimLight)).rgb;
			
			float4 col;
			col.rgb = s.Albedo * _LightColor0.rgb * ramp;
			col.a = s.Alpha;
			return col;
		}
		
		struct Input
		{
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			float4 c;
			
			float4 ambientFinal = _AmbientColor;
			c = _EmissiveColor + ambientFinal;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
