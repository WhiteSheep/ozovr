﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace VBGames.PlayMaker.Actions
{
    public class ExportSceneToConfig : Editor
    {
        [MenuItem("Ozovr/BuildAssetBundle/TargetPlatform/Standalone")]
        private static void BuildAssetBundle()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Standalone");
        }

        [MenuItem("Ozovr/BuildAssetBundle/TargetPlatform/WebGL")]
        private static void BuildAssetBundleForWebGL()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Webgl",
                BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.DeterministicAssetBundle,
                BuildTarget.WebGL);
        }

        [MenuItem("Ozovr/ExportSceneToXML")]
        private static void ExportToXML()
        {
            string filePath = Application.dataPath + @"/StreamingAssets/scene.xml";
            if (!File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            //  Create XmlDocument
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement root = xmlDoc.CreateElement("scenes");
            xmlDoc.AppendChild(root);
            DebugEx.Log(UnityEditor.EditorBuildSettings.scenes.Length);

            foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
            {
                DebugEx.Log(S.path);
                //  when scene enabled export the config
                if (S.enabled)
                {
                    string name = S.path;
                    EditorApplication.OpenScene(name);

                    XmlElement scene = xmlDoc.CreateElement("scene");
                    scene.SetAttribute("name", name);
                    root.AppendChild(scene);

                    // We only export what we need but not all gameObject
                    GameObject[] allGameObjects = GameObject.FindGameObjectsWithTag("ExportGameObject");
                    //Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
                    DebugEx.Log(" " + allGameObjects.Length + " in scene " + name);

                    foreach (GameObject obj in allGameObjects)
                    {
                        //  Create gameObject
                        XmlElement gameObject = xmlDoc.CreateElement("gameObject");
                        gameObject.SetAttribute("name", obj.name);
                        gameObject.SetAttribute("asset", obj.name + ".prefab");
                        scene.AppendChild(gameObject);

                        //  Create gameObject transform
                        XmlElement transform = xmlDoc.CreateElement("transform");
                        gameObject.AppendChild(transform);

                        //  create transform - position
                        XmlElement position = xmlDoc.CreateElement("position");
                        XmlElement positionX = xmlDoc.CreateElement("x");
                        XmlElement positionY = xmlDoc.CreateElement("y");
                        XmlElement positionZ = xmlDoc.CreateElement("z");
                        positionX.InnerText = obj.transform.position.x.ToString();
                        positionY.InnerText = obj.transform.position.y.ToString();
                        positionZ.InnerText = obj.transform.position.z.ToString();
                        position.AppendChild(positionX);
                        position.AppendChild(positionY);
                        position.AppendChild(positionZ);
                        transform.AppendChild(position);

                        //  create transform - rotation
                        XmlElement rotation = xmlDoc.CreateElement("rotation");
                        XmlElement rotationX = xmlDoc.CreateElement("x");
                        XmlElement rotationY = xmlDoc.CreateElement("y");
                        XmlElement rotationZ = xmlDoc.CreateElement("z");
                        rotationX.InnerText = obj.transform.rotation.eulerAngles.x.ToString();
                        rotationY.InnerText = obj.transform.rotation.eulerAngles.y.ToString();
                        rotationZ.InnerText = obj.transform.rotation.eulerAngles.z.ToString();
                        rotation.AppendChild(rotationX);
                        rotation.AppendChild(rotationY);
                        rotation.AppendChild(rotationZ);
                        transform.AppendChild(rotation);

                        //  Create transform - scale
                        XmlElement scale = xmlDoc.CreateElement("scale");
                        XmlElement scaleX = xmlDoc.CreateElement("x");
                        XmlElement scaleY = xmlDoc.CreateElement("y");
                        XmlElement scaleZ = xmlDoc.CreateElement("z");
                        scaleX.InnerText = obj.transform.localScale.x.ToString();
                        scaleY.InnerText = obj.transform.localScale.y.ToString();
                        scaleZ.InnerText = obj.transform.localScale.z.ToString();
                        scale.AppendChild(scaleX);
                        scale.AppendChild(scaleY);
                        scale.AppendChild(scaleZ);
                        transform.AppendChild(scale);
                    }
                }
            }

            //  save xmlDoc and refresh AssetDataBase
            xmlDoc.Save(filePath);
            AssetDatabase.Refresh();
        }
    }
}