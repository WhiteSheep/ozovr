﻿using UnityEngine;
using System.Collections;

namespace VBGames.PlayMaker.Actions
{
    public class HttpServerManager : MonoBehaviour
    {

        private static HttpServerManager _instance = null;

        public static HttpServerManager Instance
        {
            get { return _instance; }
        }

        private void Awake()
        {
            _instance = this;
        }

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {

        }

        public void UploadScreenShot(byte[] screenshotBytes, string uploadFileName)
        {
            StartCoroutine(UploadCo(screenshotBytes, uploadFileName));
        }

        private IEnumerator UploadCo(byte[] screenshotBytes, string uploadFileName)
        {

            WWWForm postForm = new WWWForm();
            postForm.AddBinaryData("screenshot", screenshotBytes, uploadFileName + ".png", "text/plain");
            WWW upload = new WWW("http://192.168.1.102:80/U3DFileServer/UploadScreenShot.php", postForm);

            yield return upload;

            if (upload.error == null)
            {
                DebugEx.Log("Upload done : " + upload.text);
            }
            else
            {
                DebugEx.Log("Upload Failed : <" + uploadFileName + ">");
            }
        }
    }
}