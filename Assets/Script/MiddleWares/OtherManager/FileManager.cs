﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace VBGames.PlayMaker.Actions
{
    public class OS
    {
        public static OperatingSystem operationSystem = Environment.OSVersion;

        public static PlatformID GetPlatformId()
        {
            return operationSystem.Platform;
        }
    }

    public class FileOutput : ThreadJob
    {
        private struct FileInfo
        {
            public string filePath;
            public byte[] fileBytes;

            public FileInfo(string prmFilePath, byte[] prmFileBytes)
            {
                filePath = prmFilePath;
                fileBytes = prmFileBytes;
            }
        }

        private LinkedList<FileInfo> _lOutputFileCache = new LinkedList<FileInfo>();

        protected override void ThreadFunction()
        {
            while (true)
            {
                int outputedFileCount = 0;
                while (_lOutputFileCache.Count > 0 && outputedFileCount < AppSetting.MAX_OUTPUTFILES_ONCE)
                {
                    FileInfo fileInfo = _lOutputFileCache.First.Value;
                    _lOutputFileCache.RemoveFirst();

                    FileManager.Instance.Write(fileInfo.filePath, fileInfo.fileBytes);
                    outputedFileCount++;
                }
                Thread.Sleep(10);
            }
        }

        protected override void OnFinished()
        {
            DebugEx.Log("write complete!");
        }

        public void WriteToCache(string prmFilePath, byte[] prmAllBytes)
        {
            FileInfo fileInfo = new FileInfo(prmFilePath, prmAllBytes);
            _lOutputFileCache.AddLast(fileInfo);
        }
    }


    public class FileManager
    {
        protected static FileManager _instance;
        protected FileOutput _trFileOutput;

        public static FileManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    switch (OS.GetPlatformId())
                    {
                        case PlatformID.WinCE:
                        case PlatformID.Win32Windows:
                        case PlatformID.Win32S:
                        case PlatformID.Win32NT:
                            _instance = new WindowsFileManager();
                            break;
                        case PlatformID.Unix:
                            DebugEx.Log("Doesn't support Unix yet");
                            break;
                        case PlatformID.MacOSX:
                            DebugEx.Log("Doesn't support MacOSX yet");
                            break;
                        default:
                            _instance = new FileManager();
                            break;
                    }
                }
                return _instance;
            }
        }

        protected FileManager()
        {
            _trFileOutput = new FileOutput();
            _trFileOutput.Start();
            _trFileOutput.AutoControl();
        }

        public virtual void Write(string prmPath, byte[] prmAllBytes)
        {
        }

        public virtual void WriteAsync(string prmPath, byte[] prmAllBytes)
        {
        }
    }

    public class WindowsFileManager : FileManager
    {
        public override void Write(string prmPath, byte[] prmAllBytes)
        {
            File.WriteAllBytes(prmPath, prmAllBytes);
        }

        public override void WriteAsync(string prmPath, byte[] prmAllBytes)
        {
            _trFileOutput.WriteToCache(prmPath, prmAllBytes);
        }
    }
}