﻿using UnityEngine;
using System.Threading;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class ThreadJob
    {
        private bool _bIsDone;
        private object _object;
        private Thread _coreThread;
        private object _handle = new object();

        public object ThreadParam
        {
            get { return _object; }
            set { _object = value; }
        }

        public bool IsDone
        {
            get
            {
                bool temp;
                lock (_handle)
                {
                    temp = _bIsDone;
                }
                return temp;
            }

            set
            {
                lock (_handle)
                {
                    _bIsDone = value;
                }
            }
        }

        public ThreadJob()
        {
            _bIsDone = false;
            _coreThread = new Thread(Run);
        }

        public void Start()
        {
            _coreThread.Start();
        }

        public bool Update()
        {
            if (IsDone)
            {
                OnFinished();
                return true;
            }
            else
                return false;
        }

        public void Abort()
        {
            _coreThread.Abort();
        }

        public void AutoControl()
        {
            ThreadManager.Instance.AddThreadJob(this);
        }

        protected virtual void ThreadFunction()
        {
        }

        protected virtual void OnFinished()
        {
        }

        private void Run(object obj)
        {
            ThreadFunction();
            _bIsDone = true;
        }
    }

    public class ThreadManager : MonoBehaviour
    {
        private static ThreadManager _instance;
        private List<ThreadJob> _lThreadJobs;

        public static ThreadManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ThreadManager();
                return _instance;
            }
        }

        protected ThreadManager()
        {
            _lThreadJobs = new List<ThreadJob>();
            Debug.Log("ThreadManager created");
        }

        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        // Use this for initialization
        public void Start()
        {

        }

        // Update is called once per frame
        public void Update()
        {
            List<ThreadJob> removedThread = new List<ThreadJob>();

            //  update all threads and check state
            foreach (ThreadJob thread in _lThreadJobs)
            {
                if (thread.Update())
                    removedThread.Add(thread);
            }

            //  remove done
            foreach (ThreadJob thread in removedThread)
                _lThreadJobs.Remove(thread);
            removedThread.Clear();
        }

        public void OnDestroy()
        {
            DebugEx.Log(string.Format("Removed {0} thread(s) before exit", _lThreadJobs.Count));
            foreach (ThreadJob thread in _lThreadJobs)
                thread.Abort();
            _lThreadJobs.Clear();
        }

        public void AddThreadJob(ThreadJob prmThreadJob)
        {
            _lThreadJobs.Add(prmThreadJob);
        }

    }
}