﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using VBGames.Network;
using VBGames.OS;
using VBGames.Utils.Logger;

namespace VBGames.PlayMaker.Actions
{
    public class CommandProcessor
    {
        //-----------------------------------------------------------------------
        public static void handleMessage(VBSocket myClientSocket, string message)
        {
            try
            {
                // 分解消息
                string[] msgs = message.Split(RequestHeader.SPLIT);

                // 判断消息长度是否合法
                if (msgs.Length < 2)
                    throw new ArgumentException(string.Format("Message [{0}] is too short (at least 2)", message));

                if (message.Contains(RequestHeader.KEEP_ALIVE))
                {
                    keepAlive(myClientSocket, msgs[0]);
                } 
                else if (message.Contains(RequestHeader.CHECK_STATUS))
                {
                    checkStatus(myClientSocket, msgs[0]);
                }
                else if (message.Contains(RequestHeader.RENDER_MACHINE))
                {
                    RenderCommand cmd = new RenderCommand(msgs[0], msgs[1]);
                    renderRoom(myClientSocket, cmd);
                }
            }
            catch (Exception e)
            {
                VBLogger.Err(string.Format("Undefined command [{0}],e={1}", message, e.Message));
            }
        }
        //-----------------------------------------------------------------------

        public static void renderRoom(VBSocket prmClientSocket, RenderCommand prmCmd)
        {
            GameManagerStd.Instance.GetRMSocketServer().EnqueueCommand(prmCmd);
        }

        //-----------------------------------------------------------------------

        public static void keepAlive(VBSocket prmClientSocket, string messageId)
        {
            prmClientSocket.Send(string.Format("{0}|{1}|<EOF>", messageId, RequestHeader.KEEP_ALIVE), 10);
            VBLogger.Debug(string.Format("Send [{0}] to server (KeepAlive). Message ID={1}", prmClientSocket.socket.RemoteEndPoint, messageId));
        }

        //-----------------------------------------------------------------------

        public static void checkStatus(VBSocket prmClientSocket, string messageId)
        {

            string systemHealthyInfo = string.Format("{0}|{1}|{2}|{3}|<EOF>",
                messageId,
                RequestHeader.CHECK_STATUS,
                VBOperationSystem.GetCurrentCpuUsage(),
                GameManagerStd.Instance.GetRenderSceneManager().GetRenderPlaceUsage()
            );

            prmClientSocket.Send(systemHealthyInfo, 10);
            VBLogger.Debug(string.Format("Send [{0}] to server (CheckStatus). Message ID={1}", prmClientSocket.socket.RemoteEndPoint, messageId));
        }
    }
}
