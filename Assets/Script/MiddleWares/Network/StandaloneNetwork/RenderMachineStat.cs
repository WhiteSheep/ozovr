﻿using UnityEngine;
using System.Collections;


namespace VBGames.PlayMaker.Actions
{

    /// <summary>
    /// 渲染机的状态，同java，不全用
    /// </summary>
    /// 
    public enum RenderMachineStat
    {
        OK, //0
        FULL, //1
        DISCONNECTED, //2
        NOT_ALIVE, //3
        UNKNOWN_ERROR, //4
        BUSY //5
        // 从java 包中拷贝来，部分状态不会被使用
        // 可能被使用的有FULL BUSY 和 OK
    }
}