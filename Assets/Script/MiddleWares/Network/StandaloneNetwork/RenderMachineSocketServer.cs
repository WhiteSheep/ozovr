﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;
using System.Linq;
using System.Net.Configuration;
using VBGames.Network;
using VBGames.Utils.Logger;
using Object = System.Object;

namespace VBGames.PlayMaker.Actions
{
    public class RenderMachineSocketServer
    {
        // 用于存储所有工作线程接收到的指令(Thread-Safe)
        private Queue _commands;
        
        // 监听线程和工作线程
        private Thread _monitorThread;
        private VBSocket _serverSocket;
        private bool _isWorking;

        public void Setup()
        {
            //  创建线程安全的Queue
            //  Net2.0环境下没有ConcurrentQueue，如果Unity升级到5.5正式版之后就可以使用Concurrent组件了
            _commands = Queue.Synchronized(new Queue());
            
            _isWorking = true;
            _OpenMonitorThread();
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            RenderSceneMgr sceneMgr = GameManagerStd.Instance.GetRenderSceneManager();

            //  只有存在空余场景时，指令才会出队
            while (sceneMgr.EmptyRenderPlaceExist() && _commands.Count != 0)
            {
                RenderCommand cmd = DequeueCommand<RenderCommand>();
                if (cmd != null)
                {
                    VBLogger.Debug(string.Format("Render command [{0}-{1}] dequeue", cmd.cmdID, cmd.roomID));
                    GameManagerStd.Instance.GetRenderSceneManager().TransferCommand(cmd);    
                }
            }
        }

        //-----------------------------------------------------------------------
        public void EnqueueCommand(Object prmCmd)
        {
            if (prmCmd != null)
                _commands.Enqueue(prmCmd);
        }

        //-----------------------------------------------------------------------
        public T DequeueCommand<T>()
        {
            return (_commands.Count != 0) ? (T) _commands.Dequeue() : default(T);
        }

        //-----------------------------------------------------------------------
        private void _OpenMonitorThread()
        {
            _serverSocket = new VBSocket(Config.MonitorIp, Config.MonitorPort);

            // 启动监听线程，接收调度服务器的请求
            _monitorThread = new Thread(ListenClientConnect);
            _monitorThread.Start();

        }

        //-----------------------------------------------------------------------
        /// <summary>  
        /// 监听客户端连接  
        /// </summary>  
        private void ListenClientConnect()
        {
            VBLogger.Log(string.Format("RenderMachine Server start listening port: [{0}]", _serverSocket.socket.LocalEndPoint));

            while (_isWorking)
            {
                // 接收调度服务器的新申请
                VBSocket vbSocket = _serverSocket.Accept();

                // 加入到线程池
                ThreadPool.QueueUserWorkItem(ReceiveMessage, vbSocket);
            }
        }

        //-----------------------------------------------------------------------
        /// <summary>  
        /// 接收消息  
        /// </summary>  
        /// <param name="clientSocket"></param>  
        private void ReceiveMessage(object args)
        {
            VBSocket vbSocket = args as VBSocket;
            if (vbSocket == null)
            {
                VBLogger.Log("Socket illegal，work thread stop");
                return;
            }

            while (_isWorking)
            {
                //  当远程服务器断开时自动结束线程
                if (!vbSocket.SocketConnected())
                {
                    VBLogger.Log(string.Format("Remote client {0} disconnect", vbSocket.socket.RemoteEndPoint));
                    break;
                }

                //  接收并处理消息，没有接受完的消息会在缓存中继续等待
                List<string> commands = vbSocket.ReceiveAndProcess("[EOF]", 500);
                for (int idx = 0; idx < commands.Count; ++idx)
                {
                    VBLogger.Log(string.Format("Recieve client [{0}] message :[{1}]", vbSocket.socket.RemoteEndPoint, commands[idx]));
                    CommandProcessor.handleMessage(vbSocket, commands[idx]);
                }
            }

            vbSocket.Close();
            VBLogger.Log(string.Format("Client {0} listening thread is over", vbSocket.socket.RemoteEndPoint));
        }
        

        //-----------------------------------------------------------------------
        public void OnDestroy()
        {
            if (_serverSocket != null)
            {
                // 释放主监听线程资源
                _serverSocket.Close();
                VBLogger.Log("Master thread over");

                // 所有Accept线程自动退出并释放资源
                _isWorking = false;
                VBLogger.Log("All worker thread over");

                VBLogger.Log("RenderMachine Server shutdown");
            }
        }
    }
}