﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 消息头
/// </summary>

namespace VBGames.PlayMaker.Actions
{
    public static class RequestHeader
    {
        public static string KEEP_ALIVE = "ka";
        public static string CHECK_STATUS = "cs";
        public static string RENDER_MACHINE = "rm";
        public static char SPLIT = '|';
    }
}