﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class DownloadManager : MonoBehaviour
    {
        [SerializeField] private bool _openRedownload = true;
        [SerializeField] private int _RedownloadTimes = 0; // 0 - No Limit

        private Queue<DownloadingRequest> _requests;
        private List<DownloadingRequest> _downloadings;

        private static DownloadManager _instance;

        public static DownloadManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DownloadManager();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        //-----------------------------------------------------------------------
        public void Start()
        {
            _requests = new Queue<DownloadingRequest>();
            _downloadings = new List<DownloadingRequest>();
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            List<DownloadingRequest> removedDlReq = new List<DownloadingRequest>();

            //  Check each DownloadingRequest in list
            foreach (DownloadingRequest dlReq in _downloadings)
            {
                if (dlReq.IsDone())
                    removedDlReq.Add(dlReq);

                //  Downloading is done
                if (dlReq.www.isDone)
                {
                    if (dlReq.www.error != null)
                        _DownloadError(dlReq);
                    else
                    {
                        dlReq.OnFinish();
                        DebugEx.Log(string.Format("Download successful:<{0}>", dlReq.url));
                    }
                    // dlReq.Dispose();
                }
                else if (dlReq.IsTimeout())
                {
                    DebugEx.Log(string.Format("Download timeout:<{0}>", dlReq.url));
                    _DownloadError(dlReq, true);
                    // dlReq.Dispose();
                }
            }

            //  Remove downloaded request
            foreach (DownloadingRequest dlReq in removedDlReq)
                _downloadings.Remove(dlReq);
            removedDlReq.Clear();

            // Ready to download a new one
            while (_requests.Count > 0 && _downloadings.Count < AppSetting.DOWNLOADING_MAX_COUNT)
            {
                DownloadingRequest dlReq = _requests.Dequeue();
                dlReq.StartDownload();
                _downloadings.Add(dlReq);

                DebugEx.Log(string.Format("Start to download <{0}>", dlReq.url));
            }
        }

        //-----------------------------------------------------------------------
        private void _Push(Request prmReq)
        {
            DownloadingRequest downloading = new DownloadingRequest(prmReq);
            _requests.Enqueue(downloading);
        }

        //-----------------------------------------------------------------------
        private void _DownloadError(DownloadingRequest prmDlReq, bool prmForceReDownload = false)
        {
            DebugEx.Log(prmDlReq.www.error);

            //  Force ReDownloading
            if (prmForceReDownload)
            {
                _Redownload(prmDlReq);
                return;
            }

            //  Some error needn't Redownload
            if (!_NeedRedownload(prmDlReq.www.error))
                return;

            //  Check if need Redownload
            if (_openRedownload && ((_RedownloadTimes == 0) || prmDlReq.redownloadTimes < _RedownloadTimes))
            {
                _Redownload(prmDlReq);
                DebugEx.Log(string.Format("Redownloading <{0}> for {1} times", prmDlReq.url, prmDlReq.redownloadTimes));
            }
            else
            {
                DebugEx.Log(string.Format("Downloading <{0}> failed", prmDlReq.url));
            }
        }

        //-----------------------------------------------------------------------
        private void _Redownload(DownloadingRequest prmDlReq)
        {
            ++prmDlReq.redownloadTimes;
            _requests.Enqueue(prmDlReq);
        }

        //-----------------------------------------------------------------------
        private bool _NeedRedownload(string prmErrorMsg)
        {
            return !(prmErrorMsg.Equals(WWWErrorType.CACHED_ASSETBUNDLE) || prmErrorMsg.Equals(WWWErrorType.NOT_FOUND));
        }

        //-----------------------------------------------------------------------
        public DownloadingRequest Download(Request prmReq)
        {
            DownloadingRequest dlReq = new DownloadingRequest(prmReq);
            _requests.Enqueue(dlReq);

            return dlReq;
        }

        //-----------------------------------------------------------------------
        public static IEnumerator DownloadSync(Request prmReq)
        {
            DownloadingRequest dlReq = new DownloadingRequest(prmReq);
            dlReq.StartDownload();

            bool downloadComplete = false;
            while (!downloadComplete)
            {
                yield return dlReq.www;

                if (dlReq.www.error != null)
                {
                    //  Stop download
                    DebugEx.Log(dlReq.www.error);
                    dlReq.Dispose();

                    //  Restart download or give up
                    if (dlReq.redownloadTimes < Instance._RedownloadTimes && Instance._NeedRedownload(dlReq.www.error))
                    {
                        ++dlReq.redownloadTimes;
                        DebugEx.Log(string.Format("Redownloading <{0}> for {1} times", dlReq.url, dlReq.redownloadTimes));
                        dlReq.StartDownload();
                    }
                    else
                    {
                        DebugEx.Log(string.Format("Downloading <{0}> failed", dlReq.url));
                        downloadComplete = true;
                    }
                }
                else
                {
                    downloadComplete = true;
                    dlReq.OnFinish();
                }
            }
        }

        //-----------------------------------------------------------------------
        public static void DownloadAsync(Request prmReq)
        {
            DebugEx.Log("Request come : " + prmReq.url);
            Instance._Push(prmReq);
        }
    }
}