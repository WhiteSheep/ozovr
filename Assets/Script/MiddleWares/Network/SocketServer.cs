﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace VBGames.PlayMaker.Actions
{
    public class SocketServer
    {
        private Thread serverThread;
        private Socket serverSocket;
        private List<Command> _commands;

        public void Setup()
        {
            _commands = new List<Command>();
            serverThread = new Thread(_OpenServer);
            serverThread.Start();
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            /*if (GameManagerStd.Instance.GetSceneManager().IsIdle() && _commands.Count != 0)
            {
                Command cmd = _commands[0];
                _commands.RemoveAt(0);
                DebugEx.Log("command recieved at frame " + Time.frameCount);

                GameManagerStd.Instance.GetSceneManager().TransferCommand(cmd);
            }*/
        }

        //-----------------------------------------------------------------------
        private void _OpenServer()
        {
            IPAddress ipAddr = IPAddress.Parse(Config.MonitorIp);
            IPEndPoint ipEP = new IPEndPoint(ipAddr, Config.MonitorPort);

            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(ipEP);
            serverSocket.Listen(20);

            Debug.Log("server start");
            while (true)
            {
                Socket client = serverSocket.Accept();

                byte[] request = new byte[512];
                int bytesRead = client.Receive(request);
                string input = Encoding.UTF8.GetString(request, 0, bytesRead);
                Debug.Log("server request : " + input);
                _SaveCommand(input);

                string output = "connect finished!";
                byte[] concent = Encoding.UTF8.GetBytes(output);
                client.Send(concent);
                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
        }

        //-----------------------------------------------------------------------
        private void _SaveCommand(string cmd)
        {
            Command command = new Command(cmd);
            _commands.Add(command);
        }

        //-----------------------------------------------------------------------
        public Command GetCommand()
        {
            Command retCmd = null;

            if (_commands.Count > 0)
            {
                retCmd = _commands[0];
                _commands.RemoveAt(0);
            }

            return retCmd;

        }

        //-----------------------------------------------------------------------
        public void OnDestroy()
        {
            serverThread.Abort();

            if (serverSocket != null)
                serverSocket.Close();

            DebugEx.Log("Release");
        }
    }
}