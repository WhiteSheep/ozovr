﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Networking;

namespace VBGames.PlayMaker.Actions
{
    public class WWWErrorType
    {
        public static string CACHED_ASSETBUNDLE =
            "Cannot load cached AssetBundle. A file of the same name is already loaded from another AssetBundle.";

        public static string NOT_FOUND = "404 Not Found";
    }

    /// <summary>
    /// Create Request to make connection
    /// </summary>
    public class Request
    {
        //  WWW basic parameter
        public string url;
        public Action<WWW> callback;

        public Request() { }

        public Request(string prmUrl, Action<WWW> prmCallback = null)
        {
            url = prmUrl;
            callback = prmCallback;
        }

        public static Request Create(string prmUrl)
        {
            Request request = new Request();
            request.url = prmUrl;

            return request;
        }

        public static Request Create(string prmUrl, Action<WWW> prmCallback)
        {
            Request request = Create(prmUrl);
            request.callback = prmCallback;

            return request;
        }

        public virtual WWW CreateWWW()
        {
            return new WWW(url);
        }

        public virtual DownloadingRequest Start()
        {
            return DownloadManager.Instance.Download(this);
        }
    }

    public class RequestWWW : Request
    {
        public WWWForm form;
        public byte[] bytes;
        public Dictionary<string, string> header;

        public RequestWWW(string prmUrl, Action<WWW> prmCallback = null)
            : base(prmUrl, prmCallback)
        {
        }

        public RequestWWW(string prmUrl, Action<WWW> prmCallback, WWWForm prmForm)
            : base(prmUrl, prmCallback)
        {
            form = prmForm;
        }

        public RequestWWW(string prmUrl, Action<WWW> prmCallback, byte[] prmBytes)
            : base(prmUrl, prmCallback)
        {
            bytes = prmBytes;
        }

        public RequestWWW(string prmUrl, Action<WWW> prmCallback, byte[] prmBytes, Dictionary<string, string> prmHeader)
            : this(prmUrl, prmCallback, prmBytes)
        {
            header = prmHeader;
        }

        public override WWW CreateWWW()
        {
            if (header != null)
                return new WWW(url, bytes, header);
            if (bytes != null)
                return new WWW(url, bytes);
            if (form != null)
                return new WWW(url, form);

            return new WWW(url);
        }
    }

    public class RequestWWWLoadFromCacheOrDownload : Request
    {
        private Hash128 _hash128;
        private uint _crc;
        private int _version;

        private bool _hashChanged;
        private bool _crcChanged;
        private bool _versionChanged;

        public Hash128 hash128
        {
            get { return _hash128; }
            set
            {
                _hash128 = value;
                _hashChanged = true;
            }
        }

        public uint crc
        {
            get { return _crc; }
            set
            {
                _crc = value;
                _crcChanged = true;
            }
        }

        public int version
        {
            get { return _version; }
            set
            {
                _version = value;
                _versionChanged = true;
            }
        }

        public RequestWWWLoadFromCacheOrDownload(string prmUrl, Action<WWW> prmCallback, Hash128 prmHash128)
            : base(prmUrl, prmCallback)
        {
            hash128 = prmHash128;
        }

        public RequestWWWLoadFromCacheOrDownload(string prmUrl, Action<WWW> prmCallback, Hash128 prmHash128, uint prmCrc)
            : this(prmUrl, prmCallback, prmHash128)
        {
            crc = prmCrc;
        }

        public RequestWWWLoadFromCacheOrDownload(string prmUrl, Action<WWW> prmCallback, int prmVersion)
            : base(prmUrl, prmCallback)
        {
            version = prmVersion;
        }

        public RequestWWWLoadFromCacheOrDownload(string prmUrl, Action<WWW> prmCallback, int prmVersion, uint prmCrc)
            : this(prmUrl, prmCallback, prmVersion)
        {
            crc = prmCrc;
        }

        public override WWW CreateWWW()
        {
            if (_crcChanged)
                if (_hashChanged)
                    return WWW.LoadFromCacheOrDownload(url, _hash128, _crc);
                else if (_versionChanged)
                    return WWW.LoadFromCacheOrDownload(url, _version, crc);

            if (_hashChanged)
                return WWW.LoadFromCacheOrDownload(url, _hash128);
            if (_versionChanged)
                return WWW.LoadFromCacheOrDownload(url, _version);

            return null;
        }
    }

    public class TimeOut
    {
        public float beforeProgress;
        public float beforeTime;

        public bool CheckTimeOut(float progress)
        {
            float now = Time.time;
            if ((now - beforeTime) > AppSetting.WWW_TIME_OUT)
                return true;

            return false;
        }

        public void Start()
        {
            beforeTime = Time.time;
        }
    }

    public class DownloadingRequest
    {
        public WWW www;
        public TimeOut timeout;
        public Request request;

        public int redownloadTimes;

        public string url
        {
            get { return request.url; }
        }

        public DownloadingRequest(Request prmRequest)
        {
            request = prmRequest;
        }

        public void StartDownload()
        {
            timeout = new TimeOut();
            timeout.Start();

            www = request.CreateWWW();
        }

        public bool IsDone()
        {
            if (www != null)
                return www.isDone || IsTimeout();
            
            return false;
        }

        public void OnFinish()
        {
            if (request.callback != null)
                request.callback(www);
        }

        public void Dispose()
        {
            if (www != null)
                www.Dispose();
        }

        public bool IsTimeout()
        {
            return timeout.CheckTimeOut(www.progress);
        }
    }

}
