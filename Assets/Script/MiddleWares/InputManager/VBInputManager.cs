﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public static class VbInputManager
    {
        private static VirtualInput _standaloneInput;
        private static VirtualInput _mobileInput;
        private static VirtualInput _activeInput;

        public enum InputPlatform
        {
            Standalone,
            Mobile,
            JoyStick
        }

        static VbInputManager()
        {
            _standaloneInput = new StandaloneInput();

            //  In demo, we select StandaloneHelper to default input
            _activeInput = _standaloneInput;
        }

        //-----------------------------------------------------------------------
        public static bool ButtonExist(string prmButton)
        {
            return _activeInput.ButtonExist(prmButton);
        }

        //-----------------------------------------------------------------------
        public static VirtualButton RegistVirtualButton(string prmName)
        {
            return _activeInput.RegistVirtualButton(prmName);
        }

        //-----------------------------------------------------------------------
        public static void UnRegistVirtualButton(string prmName)
        {
            _activeInput.UnRegistVirtualButton(prmName);
        }

        //-----------------------------------------------------------------------
        public static bool GetKeyDown(KeyCode prmKeyCode)
        {
            return _activeInput.GetKeyDown(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetKeyUp(KeyCode prmKeyCode)
        {
            return _activeInput.GetKeyUp(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetKey(KeyCode prmKeyCode)
        {
            return _activeInput.GetKey(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetButtonDown(string prmButton)
        {
            return _activeInput.GetButtonDown(prmButton);
        }

        //-----------------------------------------------------------------------
        public static bool GetButtonUp(string prmButton)
        {
            return _activeInput.GetButtonUp(prmButton);
        }

        //-----------------------------------------------------------------------
        public static bool GetButton(string prmButton)
        {
            return _activeInput.GetButton(prmButton);
        }
    }
}