﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;

namespace VBGames.PlayMaker.Actions
{

    [RequireComponent(typeof (AudioSource))]
    public class AudioManager : MonoBehaviour
    {
        private static AudioManager _instance = null;

        public AudioSource efxSource;
        public AudioSource soundSource;

        public static float PITCH_MIN_BORDER = .95f;
        public static float PITCH_MAX_BORDER = 1.05f;

        public static AudioManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AudioManager();
                return _instance;
            }
        }

        // Use this for initialization
        private void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        public void PlayEffectSound(params AudioClip[] prmClip)
        {
            int randClipIdx = Random.Range(0, prmClip.Length - 1);
            float randPitch = Random.Range(PITCH_MIN_BORDER, PITCH_MAX_BORDER);

            efxSource.clip = prmClip[randClipIdx];
            efxSource.pitch = randPitch;
            efxSource.Play();
        }

        public void PlaySound(AudioClip prmClips)
        {
            if (prmClips == null)
            {
                soundSource.Play();
                return;
            }

            soundSource.clip = prmClips;
            soundSource.Play();
        }

        public void StopSound()
        {
            soundSource.Stop();
        }
    }
}