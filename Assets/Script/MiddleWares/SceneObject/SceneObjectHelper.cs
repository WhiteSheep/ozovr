﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ozovr.SceneObject
{
    public class SceneObjectHelper
    {
        public static SceneObjectType GetSceneObjectTypeByName(string name)
        {
            return (SceneObjectType) Enum.Parse(typeof (SceneObjectType), name);
        }
    }
}
