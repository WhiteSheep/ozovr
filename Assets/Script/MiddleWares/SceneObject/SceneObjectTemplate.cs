﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;

namespace Ozovr.SceneObject
{
    public class SceneObjectTemplate
    {
        public SceneObjectType type;
        public string id;
        public bool isDefault;

        public virtual GameObject CreateSceneObject()
        {
            return null;
        }

        public virtual void MakeTemplatePrefab()
        {
            
        }

        public virtual SceneObjectTemplate Setup(XElement prmXElement, XElement prmDefaultXElement)
        {
            type = SceneObjectType.Particle;
            id = prmXElement.Attribute("id").Value;

            if (prmXElement.Attribute("default") != null)
                isDefault = Convert.ToBoolean(prmXElement.Attribute("default").Value);

            return this;
        }
    }
}
