﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VBGames.PlayMaker.Actions;

namespace Ozovr.SceneObject
{
    [Serializable]
    public class SceneObjectTexCtrl : MonoBehaviour
    {
        public TexturePathType pathType;
        public string texturePath;
        public bool locked = true;
        public string roomId = "";

        public bool xMirror = false;
        public bool yMirror = false;
        public float xTiling = 1f;
        public float yTiling = 1f;

        public IEnumerator UpdateTexture()
        {
            WWWForm form = new WWWForm();
            form.AddField("userId", User.Id);
            form.AddField("virtualMourningHall_id", roomId);

            switch (pathType)
            {
                case TexturePathType.MainPicture:
                    yield return
                        StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(Config.GetRoomInfoPort, (www) =>
                        {
                            XMLDom xmlDom = new XMLDom();
                            xmlDom.LoadWithData(www.text);

                            int errCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                            if (errCode == Config.InterfaceNoError)
                                texturePath = Config.FileServerBasePath +
                                                           xmlDom.GetElementValue("./portrait");
                        }, form)));
                    break;
                case TexturePathType.TabletPicture:
                    yield return
                        StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(Config.GetRoomInfoPort, (www) =>
                        {
                            XMLDom xmlDom = new XMLDom();
                            xmlDom.LoadWithData(www.text);

                            int errCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                            if (errCode == Config.InterfaceNoError)
                                texturePath = Config.FileServerBasePath +
                                                           xmlDom.GetElementValue("./tabletImage");
                        }, form)));
                    break;
                case TexturePathType.Path:
                    break;
                default:
                    DebugEx.Log("Unknown picture frame tag :" + pathType);
                    break;
            }

            if (!string.IsNullOrEmpty(texturePath))
                yield return
                    StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(texturePath, (www) =>
                    {
                        Texture2D loadedTexture2D = www.texture;

                        GameObject pic = gameObject.transform.Find("Picture").gameObject;
                        Renderer rend = pic.GetComponent<Renderer>();
                        if (rend.material.mainTexture == null)
                            rend.material.mainTexture = loadedTexture2D;
                    })));
        }
    }
}
