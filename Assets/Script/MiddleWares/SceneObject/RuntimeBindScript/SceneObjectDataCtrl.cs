﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ozovr.SceneObject
{
    public class SceneObjectDataCtrl : MonoBehaviour
    {
        public UnitTemplate srcTemplate;

        public bool IsReplacable()
        {
            return srcTemplate.unitFlags != null && srcTemplate.unitFlags.Contains(UnitFlag.Replacable);
        }
    }
}
