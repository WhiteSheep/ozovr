﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using VBGames.PlayMaker.Actions;

namespace Ozovr.SceneObject
{
    public class SceneObjectTemplateMgr
    {
        private static SceneObjectTemplateMgr _instance = null;
        private Dictionary<string, SceneObjectTemplate> _templates;
        private Dictionary<SceneObjectType, XElement> _defaultTemplates;

        public static SceneObjectTemplateMgr Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SceneObjectTemplateMgr();
                return _instance;
            }
        }

        protected SceneObjectTemplateMgr()
        {
            _templates = new Dictionary<string, SceneObjectTemplate>();
            _defaultTemplates = new Dictionary<SceneObjectType, XElement>();
        }

        public void Init()
        {
            XElement root = Config.SceneObjectConfig.Root;

            //  选择所有的Default类型节点进行第一轮初始化
            IEnumerable<XElement> defaultXElements = from item in root.Elements()
                where item.Attribute("default") != null && Convert.ToBoolean(item.Attribute("default").Value)
                select item;

            foreach (var item in defaultXElements)
            {
                var itemType = SceneObjectHelper.GetSceneObjectTypeByName(item.Name.ToString());
                var template = TemplateCreatorHelper.CreateSceneObjectTemplate(itemType);

                //  初始化模板配置资源，生成模板预制体
                template.Setup(item, item);

                _defaultTemplates.Add(itemType, item);
                _templates.Add(template.id, template);
            }

            //  选择所有非Default类型节点进行第二轮初始化
            IEnumerable<XElement> otherXElements = from item in root.Elements()
                where item.Attribute("default") == null || !Convert.ToBoolean(item.Attribute("default").Value)
                select item;

            foreach (var item in otherXElements)
            {
                var itemType = SceneObjectHelper.GetSceneObjectTypeByName(item.Name.ToString());
                var template = TemplateCreatorHelper.CreateSceneObjectTemplate(itemType);

                //  初始化模板配置资源，生成模板预制体
                template.Setup(item, _GetDefaultSceneObjectXElement(itemType));
                _templates.Add(template.id, template);
            }

            //  在所有的模板信息都加载完毕以后，开始创建模板Prefab
            foreach (var kv in _templates)
            {
                var template = kv.Value;
                if (!template.isDefault)
                    template.MakeTemplatePrefab();
            }
        }

        public SceneObjectTemplate GetTemplateById(string prmId)
        {
            SceneObjectTemplate retValue;
            _templates.TryGetValue(prmId, out retValue);

            return retValue;
        }

        public T GetTemplateById<T>(string prmId) where T : SceneObjectTemplate
        {
            return GetTemplateById(prmId) as T;
        }

        public IEnumerable<T> GetAllTemplateByType<T>(bool prmIncludeDefault = false) where T : SceneObjectTemplate
        {
            List<T> retValues = new List<T>();

            foreach (var kv in _templates)
            {
                var template = kv.Value;
                if (template.isDefault && !prmIncludeDefault)
                    continue;

                if (typeof (T) == template.GetType())
                {
                    retValues.Add((T) template);
                }
            }

            return retValues;
        }

        private XElement _GetDefaultSceneObjectXElement(SceneObjectType objType)
        {
            XElement retValue;
            _defaultTemplates.TryGetValue(objType, out retValue);

            return retValue;
        }
    }
}