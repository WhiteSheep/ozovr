﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;
using Resource = VBGames.PlayMaker.Actions.Resource;

namespace Ozovr.SceneObject
{
    public class ParticleTemplate : SceneObjectTemplate
    {
        public string name;
        public string modelInfo;
        public Vector3 bindingPos;

        public GameObject particlePrefab;

        public override SceneObjectTemplate Setup(XElement prmXElement, XElement prmDefaultXElement)
        {
            base.Setup(prmXElement, prmDefaultXElement);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "Name", out name))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "Name", out name);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "ModelStruct", out modelInfo))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "ModelStruct", out modelInfo);

            if (!TemplateCreatorHelper.LoadVector3(prmXElement, "Vec3fStruct", out bindingPos))
                TemplateCreatorHelper.LoadVector3(prmDefaultXElement, "Vec3fStruct", out bindingPos);

            return this;
        }

        public override void MakeTemplatePrefab()
        {
            string[] assetInfo = modelInfo.Split('|');
            particlePrefab = Resource.LoadAsset<GameObject>(assetInfo[0], assetInfo[1]);
        }

        public override GameObject CreateSceneObject()
        {
            return base.CreateSceneObject();
        }
    }
}
