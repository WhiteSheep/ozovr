﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ozovr.SceneObject
{
    /// <summary>Stretching Config
    /// <para>-----The default value is Auto-----</para>
    /// <para>Auto - Stretching gameobject as much as possible and control gameobject in the customed capacity</para>
    /// <para>X - Stretching gameobject with X-Axis</para>
    /// <para>Y - Stretching gameobject with Y-Axis</para>
    /// <para>Z - Stretching gameobject with Z-Axis</para>
    /// <para>Full - Stretching gameobject with X,Y,Z-Axis independently</para>
    /// </summary>
    public enum ScaleDirection
    {
        Auto = 0,
        X = 1,
        Y = 2,
        Z = 3,
        Full = 4,
        Counts = 5
    }
}
