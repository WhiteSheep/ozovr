﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ozovr.SceneObject
{
    public enum UnitFlag
    {
        Rotatable,
        Deletable,
        Stickable,
        Replacable,
    }
}
