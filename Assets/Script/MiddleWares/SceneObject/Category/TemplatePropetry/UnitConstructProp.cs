﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ozovr.SceneObject
{
    public enum UnitConstructProp
    {
        DesktopDecoration,
        SustainDecoration,
        GroundDecoration,
    }
}
