﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;

namespace Ozovr.SceneObject
{
    public class CapacityTemplate : SceneObjectTemplate
    {
        public bool customed;
        public string name;
        public Vector3 size;
        public ScaleDirection scaleDirection;

        public override SceneObjectTemplate Setup(XElement prmXElement, XElement prmDefaultXElement)
        {
            base.Setup(prmXElement, prmDefaultXElement);

            if (!TemplateCreatorHelper.LoadBoolean(prmXElement, "Customed", out customed))
                TemplateCreatorHelper.LoadBoolean(prmDefaultXElement, "Customed", out customed);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "Name", out name))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "Name", out name);

            if (!TemplateCreatorHelper.LoadVector3(prmXElement, "Vec3dStruct", out size))
                TemplateCreatorHelper.LoadVector3(prmDefaultXElement, "Vec3dStruct", out size);

            if (!TemplateCreatorHelper.LoadEnum<ScaleDirection>(prmXElement, out scaleDirection))
                TemplateCreatorHelper.LoadEnum<ScaleDirection>(prmDefaultXElement, out scaleDirection);

            return this;
        }

        public override GameObject CreateSceneObject()
        {
            return base.CreateSceneObject();
        }
    }
}