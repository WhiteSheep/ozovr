﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;

namespace Ozovr.SceneObject
{
    /**
     *  该类用于辅助SceneObjectTemplate的创建工作
     */

    public class TemplateCreatorHelper
    {
        public static SceneObjectTemplate CreateSceneObjectTemplate(SceneObjectType objType)
        {
            SceneObjectTemplate retValue = null;
            switch (objType)
            {
                case SceneObjectType.Particle:
                    retValue = new ParticleTemplate();
                    break;
                case SceneObjectType.Capacity:
                    retValue = new CapacityTemplate();
                    break;
                case SceneObjectType.Unit:
                    retValue = new UnitTemplate();
                    break;
                case SceneObjectType.Item:
                    retValue = new ItemTemplate();
                    break;
                default:
                    Debug.Log("Unsupported SceneObjectType : " + objType);
                    break;
            }

            return retValue;
        }

        public static bool LoadEnum<T>(XElement prmXElement, out T outValue
            , bool thisLayer = false) where T : struct, IConvertible
        {
            outValue = default(T);
            string typename = (typeof (T)).Name;

            //  检查模板参数T的类型
            if (!typeof (T).IsEnum)
                throw new ArgumentException("T must be a enumrated type!");

            //  检查是否存在提取项
            XElement enumElement = (thisLayer) ? prmXElement : prmXElement.Element(typename);
            if (enumElement == null)
                return false;

            //  检查枚举参数是否合法
            string enumValue = enumElement.Attribute("value").Value;
            if (string.IsNullOrEmpty(enumValue))
                return false;
            else
                outValue = (T) Enum.Parse(typeof (T), enumValue);

            return true;
        }

        public static bool LoadString(XElement prmXElement, string prmElementName,
            out string outValue, bool thisLayer = false)
        {
            outValue = default(string);

            XElement xElement = thisLayer ? prmXElement : prmXElement.Element(prmElementName);
            if (xElement == null)
                return false;

            outValue = xElement.Attribute("value").Value;
            return true;
        }

        public static bool LoadBoolean(XElement prmXElement, string prmElementName,
            out bool outValue, bool thisLayer = false)
        {
            outValue = default(bool);

            XElement boolXElement = thisLayer ? prmXElement : prmXElement.Element(prmElementName);
            if (boolXElement == null)
                return false;

            outValue = Convert.ToBoolean(boolXElement.Attribute("value").Value);
            return true;
        }

        public static bool LoadVector3(XElement prmXElement, string prmElementName,
            out Vector3 outValue)
        {
            outValue = Vector3.zero;

            IEnumerable<XElement> vec3Element = prmXElement.Elements(prmElementName);
            if (!vec3Element.Any())
                return false;

            foreach (var item in vec3Element)
            {
                string index = item.Attribute("index").Value;
                float value = Convert.ToSingle(item.Attribute("value").Value);

                if (index == "x")
                    outValue.x = value;
                else if (index == "y")
                    outValue.y = value;
                else if (index == "z")
                    outValue.z = value;
            }

            return true;
        }

        public static bool LoadEnumArray<T>(XElement prmXElement, string prmElementName,
            out List<T> outValue) where T : struct, IConvertible
        {
            outValue = new List<T>();

            //  检查模板参数T的类型
            if (!typeof (T).IsEnum)
                throw new ArgumentException("T must be a enumrated type!");

            //  检查是否存在提取项
            IEnumerable<XElement> arrayElement = prmXElement.Elements(prmElementName);
            if (!arrayElement.Any())
                return false;

            foreach (var item in arrayElement)
            {
                T val = default(T);

                //  只有读取成功时才会添加
                if (LoadEnum<T>(item, out val, true))
                    outValue.Add(val);
            }

            return true;
        }

        public static bool LoadStringArray(XElement prmXElement, string prmElementName,
            out List<string> outValue)
        {
            outValue = new List<string>();

            IEnumerable<XElement> arrayElement = prmXElement.Elements(prmElementName);
            if (!arrayElement.Any())
                return false;

            foreach (var item in arrayElement)
            {
                string val = default(string);

                //  读取成功的时候才会添加到列表中
                if (LoadString(item, "", out val, true))
                {
                    //  数组中放空字符串是没有意义的
                    if (!string.IsNullOrEmpty(val))
                        outValue.Add(val);
                }
            }

            return true;
        }
    }
}