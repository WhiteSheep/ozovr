﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;
using VBGames.PlayMaker.Actions;

namespace Ozovr.SceneObject
{
    public class UnitTemplate : SceneObjectTemplate
    {
        public string name;
        public string modelInfo;
        public bool pictureFrameMode;
        public string capacityBindId;
        public List<string> particleBindIds;
        public List<UnitFlag> unitFlags;
        public string itemBindId;
        public UnitConstructProp constructProp;

        public GameObject unitPrefab;

        public override SceneObjectTemplate Setup(XElement prmXElement, XElement prmDefaultXElement)
        {
            base.Setup(prmXElement, prmDefaultXElement);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "Name", out name))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "Name", out name);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "ModelStruct", out modelInfo))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "ModelStruct", out modelInfo);

            if (!TemplateCreatorHelper.LoadBoolean(prmXElement, "PictureFrameMode", out pictureFrameMode))
                TemplateCreatorHelper.LoadBoolean(prmDefaultXElement, "PictureFrameMode", out pictureFrameMode);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "CapacityBind", out capacityBindId))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "CapacityBind", out capacityBindId);

            if (!TemplateCreatorHelper.LoadStringArray(prmXElement, "ParticleBindArray", out particleBindIds))
                TemplateCreatorHelper.LoadStringArray(prmDefaultXElement, "ParticleBindArray", out particleBindIds);

            if (!TemplateCreatorHelper.LoadEnumArray(prmXElement, "UnitFlagArray", out unitFlags))
                TemplateCreatorHelper.LoadEnumArray(prmDefaultXElement, "UnitFlagArray", out unitFlags);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "ItemBind", out itemBindId))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "ItemBind", out itemBindId);

            if (!TemplateCreatorHelper.LoadEnum(prmXElement, out constructProp))
                TemplateCreatorHelper.LoadEnum(prmDefaultXElement, out constructProp);

            return this;
        }

        public override void MakeTemplatePrefab()
        {
            //  读取模板的Prefab信息
            string[] assetInfo = modelInfo.Split('|');
            if (assetInfo.Length == 1 || string.IsNullOrEmpty(assetInfo[0]) || string.IsNullOrEmpty(assetInfo[1]))
                return;

            GameObject prefab = Resource.LoadAsset<GameObject>(assetInfo[0], assetInfo[1]);
            unitPrefab = Assist.Instantiate(prefab);

            //  获取AABB信息
            Vector3 minPoint, maxPoint;
            Assist.GetAABBSize(unitPrefab, out minPoint, out maxPoint);

            //  添加BoxCollider组件
            if (!Assist.ContainCollider(unitPrefab))
            {
                BoxCollider boxCollider = unitPrefab.AddComponent<BoxCollider>();
                boxCollider.size = Assist.Vec3Divide(maxPoint - minPoint, unitPrefab.transform.localScale);
                boxCollider.isTrigger = true;
            }

            //  根据配置信息更改Prefab体积
            _AdjustCapacity(minPoint, maxPoint);

            //  隐藏Prefab
            unitPrefab.SetActive(false);
        }

        private void _AdjustCapacity(Vector3 minPoint, Vector3 maxPoint)
        {
            if (string.IsNullOrEmpty(capacityBindId))
                return;

            var capacity = SceneObjectTemplateMgr.Instance.GetTemplateById<CapacityTemplate>(capacityBindId);
            if (capacity.customed)
            {
                Vector3 scaleRatio;

                scaleRatio.x = capacity.size.x/(maxPoint.x - minPoint.x);
                scaleRatio.y = capacity.size.y/(maxPoint.y - minPoint.y);
                scaleRatio.z = capacity.size.z/(maxPoint.z - minPoint.z);
                switch (capacity.scaleDirection)
                {
                    case ScaleDirection.X:
                        unitPrefab.transform.localScale =
                            unitPrefab.transform.localScale*scaleRatio.x;
                        break;
                    case ScaleDirection.Y:
                        unitPrefab.transform.localScale =
                            unitPrefab.transform.localScale*scaleRatio.y;
                        break;
                    case ScaleDirection.Z:
                        unitPrefab.transform.localScale =
                            unitPrefab.transform.localScale*scaleRatio.z;
                        break;
                    case ScaleDirection.Full:
                        unitPrefab.transform.localScale = Vector3.Scale(unitPrefab.transform.localScale,
                            scaleRatio);
                        break;
                    case ScaleDirection.Auto:
                        unitPrefab.transform.localScale *= Mathf.Min(scaleRatio.x, scaleRatio.y, scaleRatio.z);
                        break;
                    default:
                        break;
                }
            }
        }

        public override GameObject CreateSceneObject()
        {
            GameObject copyObject = Assist.Instantiate(unitPrefab);
            copyObject.name = name;
            copyObject.tag = Tag.ExportGameObject;

            //  设置构造体为激活状态，否则后续的函数会无法正常捕获构造体组件
            Assist.SetActive(copyObject, true);

            //  绑定粒子效果
            _BindParticles(copyObject);

            //  绑定贴图控制脚本
            if (pictureFrameMode)
                _BindTexCtrlScript(copyObject);

            //  绑定触发器控制脚本
            _BindTriggerCtrlScript(copyObject);
            
            //  绑定数据控制脚本
            _BindDataCtrlScript(copyObject);

            return copyObject;
        }


        private void _BindTexCtrlScript(GameObject prmObj)
        {
            var texCtrlScript = prmObj.AddComponent<SceneObjectTexCtrl>();

            texCtrlScript.locked = true;
            texCtrlScript.pathType = TexturePathType.Path;
        }

        private void _BindTriggerCtrlScript(GameObject prmObj)
        {
            var triggerCtrlScrpit = prmObj.AddComponent<SceneObjectTriggerCtrl>();
        }

        private void _BindDataCtrlScript(GameObject prmObj)
        {
            var dataCtrlScript = prmObj.AddComponent<SceneObjectDataCtrl>();
            dataCtrlScript.srcTemplate = this;
        }

        private void _BindParticles(GameObject prmObj)
        {
            //  获取被绑定体的长宽高信息
            Vector3 minPoint, maxPoint, lwh;
            Assist.GetAABBSize(prmObj, out minPoint, out maxPoint);
            lwh = Assist.Vec3Divide(maxPoint - minPoint, prmObj.transform.localScale) / 2f;

            //  绑定粒子
            foreach (var particleId in particleBindIds)
            {
                var particleTemplate = SceneObjectTemplateMgr.Instance.
                    GetTemplateById<ParticleTemplate>(particleId);

                if (particleTemplate == null)
                    continue;

                GameObject BindSource = Assist.Instantiate(particleTemplate.particlePrefab);

                BindSource.transform.SetParent(prmObj.transform, false);
                BindSource.transform.localPosition = Vector3.Scale(lwh, particleTemplate.bindingPos);
            }
        }
    }
}