﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VBGames.PlayMaker.Actions;

namespace Ozovr.SceneObject
{
    public class ItemTemplate : SceneObjectTemplate
    {
        public string name;
        public string templateId;
        public string description;
        public string iconInfo;
        public ItemType type;

        public Texture2D iconTex2D;

        public override SceneObjectTemplate Setup(System.Xml.Linq.XElement prmXElement, System.Xml.Linq.XElement prmDefaultXElement)
        {
            base.Setup(prmXElement, prmDefaultXElement);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "Name", out name))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "Name", out name);
            
            if (!TemplateCreatorHelper.LoadString(prmXElement, "Description", out description))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "Description", out description);

            if (!TemplateCreatorHelper.LoadString(prmXElement, "ModelStruct", out iconInfo))
                TemplateCreatorHelper.LoadString(prmDefaultXElement, "ModelStruct", out iconInfo);

            return this;
        }

        public override void MakeTemplatePrefab()
        {
            string[] texABInfo = iconInfo.Split('|');

            if (texABInfo.Length >= 2)
                iconTex2D = Resource.LoadAsset<Texture2D>(texABInfo[0], texABInfo[1]);
        }
    }
}
