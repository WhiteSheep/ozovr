﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ozovr.SceneObject
{
    public enum SceneObjectType
    {
        Particle,
        Capacity,
        Unit,
        Item,
    }
}
