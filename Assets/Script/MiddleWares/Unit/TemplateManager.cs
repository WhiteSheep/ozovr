﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace VBGames.PlayMaker.Actions
{
    public class TemplateManager
    {
        private static TemplateManager _instance;
        private readonly Dictionary<string, Template> _unitTemplates;

        public TemplateManager()
        {
            _unitTemplates = new Dictionary<string, Template>();
        }

        //-----------------------------------------------------------------------
        public static TemplateManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new TemplateManager();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public T GetTemplate<T>(string prmTemplateName) where T : Template, new()
        {
            Template retTemplate;
            bool exist = _unitTemplates.TryGetValue(prmTemplateName, out retTemplate);

            if (!exist)
            {
                InitTemplate<T>(prmTemplateName);
                _unitTemplates.TryGetValue(prmTemplateName, out retTemplate);
            }

            return retTemplate as T;
        }

        //-----------------------------------------------------------------------
        public void InitTemplate<T>(string prmUnitTmpName) where T : Template, new()
        {
            if (_unitTemplates.ContainsKey(prmUnitTmpName))
                return;

            //  Find the template config
            XElement root = Config.UnitConfig.Root;
            IEnumerable<XElement> nodes = from item in root.Elements("Unit")
                where item.Element("Text").Element("Name").Value == prmUnitTmpName
                select item;

            //  Initialize the Template
            try
            {
                Template unitTemplate = new T();
                unitTemplate.Setup(nodes.FirstOrDefault());
                _unitTemplates.Add(prmUnitTmpName, unitTemplate);
            }
            catch (Exception e)
            {
                DebugEx.Log(e.Message);
            }
        }

        //-----------------------------------------------------------------------
        public UnitTemplate GetTemplate(string prmTemplateName)
        {
            Template retTemplate;
            bool exist = _unitTemplates.TryGetValue(prmTemplateName, out retTemplate);

            if (!exist)
            {
                InitTemplate(prmTemplateName);
                _unitTemplates.TryGetValue(prmTemplateName, out retTemplate);
            }

            return retTemplate as UnitTemplate;
        }

        //-----------------------------------------------------------------------
        public void InitTemplate(string prmUnitTmpName)
        {
            if (_unitTemplates.ContainsKey(prmUnitTmpName))
                return;

            //  Find the template config
            XElement root = Config.UnitConfig.Root;
            IEnumerable<XElement> nodes = from item in root.Elements("Unit")
                                          where item.Element("Text").Element("Name").Value == prmUnitTmpName
                                          select item;

            //  Initialize the Template
            try
            {
                UnitTemplate unitTemplate = new UnitTemplate();
                unitTemplate.Setup(nodes.FirstOrDefault());
                _unitTemplates.Add(prmUnitTmpName, unitTemplate);
            }
            catch (Exception e)
            {
                DebugEx.Log(e.Message);
            }
        }

    }
}