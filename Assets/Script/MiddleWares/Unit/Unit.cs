﻿using System;
using UnityEngine;
using System.Collections;

namespace VBGames.PlayMaker.Actions
{
    [Serializable]
    public class Unit : MonoBehaviour
    {
        public UnitTemplate template;
        public PictureFrame pictureFrame = new PictureFrame();

        public bool drawBounds = false;

        public IEnumerator UpdatePictureFrame()
        {
            WWWForm form = new WWWForm();
            form.AddField("userId", User.Id);
            form.AddField("virtualMourningHall_id", User.virtualMourningHallId);

            switch (pictureFrame.frameTag)
            {
                case PictureFrameTag.MainPicture:
                    yield return
                        StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(Config.GetRoomInfoPort, (www) =>
                        {
                            XMLDom xmlDom = new XMLDom();
                            xmlDom.LoadWithData(www.text);

                            int errCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                            if (errCode == Config.InterfaceNoError)
                                pictureFrame.texturePath = Config.FileServerBasePath +
                                                           xmlDom.GetElementValue("./portrait");
                        }, form)));
                    break;
                case PictureFrameTag.TabletPicture:
                    yield return
                        StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(Config.GetRoomInfoPort, (www) =>
                        {
                            XMLDom xmlDom = new XMLDom();
                            xmlDom.LoadWithData(www.text);

                            int errCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                            if (errCode == Config.InterfaceNoError)
                                pictureFrame.texturePath = Config.FileServerBasePath +
                                                           xmlDom.GetElementValue("./tabletImage");
                        }, form)));
                    break;
                case PictureFrameTag.Path:
                    break;
                default:
                    DebugEx.Log("Unknown picture frame tag :" + pictureFrame.frameTag);
                    break;
            }

            if (!string.IsNullOrEmpty(pictureFrame.texturePath))
                yield return
                    StartCoroutine(DownloadManager.DownloadSync(new RequestWWW(pictureFrame.texturePath, (www) =>
                    {
                        Texture2D loadedTexture2D = www.texture;

                        GameObject pic = gameObject.transform.Find("Picture").gameObject;
                        Renderer rend = pic.GetComponent<Renderer>();
                        if (rend.material.mainTexture == null)
                            rend.material.mainTexture = loadedTexture2D;
                    })));
        }
    }
}