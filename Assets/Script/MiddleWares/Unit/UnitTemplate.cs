﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Xml.Linq;

namespace VBGames.PlayMaker.Actions
{
    public class UnitTemplate : Template
    {
        public GameObject templateGo;
        public Capacity capacity = new Capacity();
        public List<Particle> particles = new List<Particle>();
        public string name;
        public string description;
        public string assetBundleName;
        public string assetName;
        public bool createShadow = true;
        public bool recieveShadow = true;
        public bool rotatable;
        public bool deletable;
        public bool isReady;
        public bool stickable;
        public bool pictureFrameMode;

        public override void Setup(XElement prmProperties)
        {
            //  Set Template properties
            _InitProperties(prmProperties);

            //  Set origin properties
            _InitTemplateObject();

            templateGo.SetActive(false);
            isReady = true;
        }

        //-----------------------------------------------------------------------
        public override GameObject CreateEntity()
        {
            GameObject entity = Assist.Instantiate(templateGo);
            entity.name = name;
            entity.tag = Tag.ExportGameObject;

            //  Active entity
            Assist.SetActive(entity, true);

            //  Bind script on entity
            Unit unitScript = entity.AddComponent<Unit>();
            _InitUnitScript(unitScript);

            //  Bind particles on entity
            if (particles.Count != 0)
                _BindParticles(entity);

            return entity;
        }

        //-----------------------------------------------------------------------
        private void _InitProperties(XElement prmProperties)
        {
            //  Properties area
            XElement text = prmProperties.Element("Text");
            XElement graph = prmProperties.Element("Graph");
            XElement physic = prmProperties.Element("Physic");
            XElement status = prmProperties.Element("Status");

            //  More complex properties
            XElement pb = graph.Element("ParticleBinding");
            XElement cap = physic.Element("Capacity");

            //  Load Text
            name = text.Element("Name").Value;

            //  Load Graph
            assetBundleName = graph.Element("AssetBundleName").Value;
            assetName = graph.Element("AssetName").Value;
            pictureFrameMode = Convert.ToBoolean(graph.Element("PictureFrameMode").Value);

            foreach (XElement child in pb.Elements("Particle"))
            {
                XElement pos = child.Element("Position");
                float x = float.Parse(pos.Element("x").Value);
                float y = float.Parse(pos.Element("y").Value);
                float z = float.Parse(pos.Element("z").Value);

                Particle particle = new Particle(child.Element("Name").Value, new Vector3(x, y, z));
                particles.Add(particle);
            }

            //  Load Physic
            capacity.customed = Convert.ToBoolean(cap.Element("Customed").Value);
            capacity.x = Convert.ToInt32(cap.Element("x").Value);
            capacity.y = Convert.ToInt32(cap.Element("y").Value);
            capacity.z = Convert.ToInt32(cap.Element("z").Value);
            capacity.scaleDirection =
                (ScaleDirection) Enum.Parse(typeof (ScaleDirection), cap.Element("ScaleDirection").Value);

            //  Load Status
            rotatable = Convert.ToBoolean(status.Element("Rotatable").Value);
            deletable = Convert.ToBoolean(status.Element("Deletable").Value);
            stickable = Convert.ToBoolean(status.Element("Stickable").Value);
        }

        //-----------------------------------------------------------------------
        private void _InitTemplateObject()
        {
            Vector3 minPoint, maxPoint;

            //  Load GameObject
            GameObject prefab = Resource.LoadAsset<GameObject>(assetBundleName, assetName);
            templateGo = Assist.Instantiate(prefab);
            Assist.GetAABBSize(templateGo, out minPoint, out maxPoint);

            //  Add BoxCollider
            if (!Assist.ContainCollider(templateGo))
            {
                BoxCollider boxCollider = templateGo.AddComponent<BoxCollider>();
                boxCollider.size = Assist.Vec3Divide(maxPoint - minPoint, templateGo.transform.localScale);
                boxCollider.isTrigger = true;
            }

            //  Change Size
            if (capacity.customed)
            {
                Vector3 scaleRatio;

                scaleRatio.x = capacity.x/(maxPoint.x - minPoint.x);
                scaleRatio.y = capacity.y/(maxPoint.y - minPoint.y);
                scaleRatio.z = capacity.z/(maxPoint.z - minPoint.z);
                switch (capacity.scaleDirection)
                {
                    case ScaleDirection.X:
                        templateGo.transform.localScale =
                            Assist.ChangeVec3X(templateGo.transform.localScale,
                                templateGo.transform.localScale.x*scaleRatio.x);
                        break;
                    case ScaleDirection.Y:
                        templateGo.transform.localScale =
                            Assist.ChangeVec3Y(templateGo.transform.localScale,
                                templateGo.transform.localScale.y*scaleRatio.y);
                        break;
                    case ScaleDirection.Z:
                        templateGo.transform.localScale =
                            Assist.ChangeVec3Z(templateGo.transform.localScale,
                                templateGo.transform.localScale.z*scaleRatio.z);
                        break;
                    case ScaleDirection.Full:
                        templateGo.transform.localScale = Vector3.Scale(templateGo.transform.localScale,
                            scaleRatio);
                        break;
                    case ScaleDirection.Auto:
                        templateGo.transform.localScale *= Mathf.Min(scaleRatio.x, scaleRatio.y, scaleRatio.z);
                        break;
                    default:
                        break;
                }
            }
        }
        //-----------------------------------------------------------------------
        private void _InitUnitScript(Unit prmUnit)
        {
            prmUnit.template = this;

            prmUnit.pictureFrame.enabled = pictureFrameMode;
            prmUnit.pictureFrame.locked = true;
            prmUnit.pictureFrame.frameTag = PictureFrameTag.Path;
        }

        //-----------------------------------------------------------------------
        private void _BindParticles(GameObject prmGameObject)
        {
            //  Load particle assets
            List<AssetInfo> particlesAssetInfo = new List<AssetInfo>();
            foreach (Particle item in particles)
                particlesAssetInfo.Add(new AssetInfo(AssetBundleName.Particle, item.name, typeof (GameObject)));
            AssetMap particlesMap = Resource.LoadAssets(particlesAssetInfo);

            //  Calculate real size
            Vector3 minPoint, maxPoint, lwh;
            Assist.GetAABBSize(prmGameObject, out minPoint, out maxPoint);
            lwh = Assist.Vec3Divide(maxPoint - minPoint, prmGameObject.transform.localScale)/2f;

            //  Bind particles
            foreach (Particle particle in particles)
            {
                GameObject bindPrefab = particlesMap.GetAsset<GameObject>(AssetBundleName.Particle, particle.name);
                DebugEx.Log(particle.name);
                if (bindPrefab != null)
                    DebugEx.Log(true);
                GameObject bindSource = Assist.Instantiate(bindPrefab);

                bindSource.transform.parent = prmGameObject.transform;
                bindSource.transform.localPosition = Vector3.Scale(lwh, particle.position);
            }
        }
    }
}