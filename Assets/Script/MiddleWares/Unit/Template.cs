﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class Template
    {
        public virtual void Setup(XElement prmProperties) { }
        //-----------------------------------------------------------------------
        public virtual GameObject CreateEntity()
        {
            return null;
        }
    }
}
