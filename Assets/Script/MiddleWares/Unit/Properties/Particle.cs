﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class Particle
    {
        public string name;
        public Vector3 position;

        public Particle(string prmName)
        {
            name = prmName;
            position = Vector3.zero;
        }

        //-----------------------------------------------------------------------
        public Particle(string prmName, Vector3 prmPos)
        {
            name = prmName;
            position = prmPos;
        }
    }
}