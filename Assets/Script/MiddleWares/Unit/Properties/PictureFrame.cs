﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    public enum PictureFrameTag
    {
        Path,
        AssetBundle,
        MainPicture,
        TabletPicture
    }

    [Serializable]
    public class PictureFrame
    {
        public bool enabled = false;
        public bool locked = true;

        public PictureFrameTag frameTag = PictureFrameTag.Path;
        public string texturePath = "";

        public bool xMirror = false;
        public bool yMirror = false;
        public float xTiling = 1f;
        public float yTiling = 1f;
    }
}
