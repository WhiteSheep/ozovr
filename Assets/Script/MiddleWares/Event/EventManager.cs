﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class EventManager
    {
        private List<TriggerEvent> _lTriggers;

        public EventManager()
        {
            _lTriggers = new List<TriggerEvent>();
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            //  Update all triggers and refresh triggerlist
            for (int i = 0; i < _lTriggers.Count;)
            {
                _lTriggers[i].Update(Time.deltaTime);

                if (_lTriggers[i].IsAlive())
                    ++i;
                else
                {
                    _lTriggers.RemoveAt(i);
                    DebugEx.Log("Trigger removed");
                }
            }
        }

        //-----------------------------------------------------------------------
        public void AddTrigger(TriggerEvent prmTrigger)
        {
            if (prmTrigger != null)
                _lTriggers.Add(prmTrigger);
        }
    }
}