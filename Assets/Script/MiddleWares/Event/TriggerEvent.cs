﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    public class TriggerEvent
    {
        //  Trigger time control
        private float _fElapsedTime;
        private float _fTriggerTime;

        //  Trigger run times, -1 means run forever
        private int _iRunTimes;
        private int _iHasRanTimes;

        public TriggerEvent(float prmTriggerTime, int prmRunTimes = -1)
        {
            _fTriggerTime = prmTriggerTime;
            _iRunTimes = prmRunTimes;

            _fElapsedTime = 0f;
            _iHasRanTimes = 0;
        }

        //-----------------------------------------------------------------------
        public void Update(float prmDeltaTime)
        {
            _fElapsedTime += prmDeltaTime;

            if ((_fElapsedTime > _fTriggerTime) && IsAlive())
            {
                _fElapsedTime = 0f;
                ++_iHasRanTimes;

                Callback();
            }
        }

        //-----------------------------------------------------------------------
        public bool IsAlive()
        {
            return (_iRunTimes == -1) || (_iHasRanTimes < _iRunTimes);
        }

        //-----------------------------------------------------------------------
        public void Reset(bool prmResetAll = false)
        {
            _fElapsedTime = 0f;

            if (prmResetAll)
            {
                _iHasRanTimes = 0;
            }
        }

        //-----------------------------------------------------------------------
        public virtual void Callback()
        {
        }
    }

    public class TriggerEventCallback : TriggerEvent
    {
        public delegate void CallbackDelegate();

        private CallbackDelegate _funcCallback;

        public CallbackDelegate FuncCallback
        {
            get { return _funcCallback; }
            set { _funcCallback = value; }
        }

        //-----------------------------------------------------------------------
        public TriggerEventCallback(CallbackDelegate prmCallbackFunc, float prmTriggerTime, int prmRunTimes = -1)
            : base(prmTriggerTime, prmRunTimes)
        {
            _funcCallback = prmCallbackFunc;
        }

        //-----------------------------------------------------------------------
        public override void Callback()
        {
            if (_funcCallback != null)
                _funcCallback();
        }
    }

    public class TriggerEventCallbackWithObj<T> : TriggerEvent
    {
        public delegate void CallbackDelegate(T obj);

        private CallbackDelegate _funcCallback;
        private T _object;

        public CallbackDelegate FuncCallback
        {
            get { return _funcCallback; }
            set { _funcCallback = value; }
        }

        //-----------------------------------------------------------------------
        public T Obj
        {
            get { return _object; }
            set { _object = value; }
        }

        //-----------------------------------------------------------------------
        public TriggerEventCallbackWithObj(CallbackDelegate prmCallbackFunc, T prmObject, float prmTriggerTime,
            int prmTriggerRunTimes = -1)
            : base(prmTriggerTime, prmTriggerRunTimes)
        {
            _funcCallback = prmCallbackFunc;
            _object = prmObject;
        }

        //-----------------------------------------------------------------------
        public override void Callback()
        {
            if (_funcCallback != null)
                _funcCallback(_object);
        }
    }
}