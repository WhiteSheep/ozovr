﻿using UnityEngine;
using System.Collections;

namespace VBGames.PlayMaker.Actions
{
    public abstract class AssetBundleLoadOperation
    {
        public abstract bool Update();
        public abstract bool IsDone();
        public abstract string GetName();
    }

    public abstract class AssetBundleLoadAssetOperation : AssetBundleLoadOperation
    {
        public abstract T GetAsset<T>() where T : UnityEngine.Object;
    }

    public class AssetBundleLoadAssetOperationFull : AssetBundleLoadAssetOperation
    {
        protected string _sAssetBundleName;
        protected string _sAssetName;
        protected string _sDownloadingError;
        protected System.Type _type;
        protected AssetBundleRequest _request = null;

        public AssetBundleLoadAssetOperationFull(string prmBundleName, string prmAssetName, System.Type prmType)
        {
            _sAssetBundleName = prmBundleName;
            _sAssetName = prmAssetName;
            _type = prmType;
        }
        //-----------------------------------------------------------------------
        public override T GetAsset<T>()
        {
            if (_request != null && _request.isDone)
                return _request.asset as T;
            else
                return null;
        }
        //-----------------------------------------------------------------------
        public override bool Update()
        {
            if (_request != null)
                return false;

            LoadedAssetBundle bundle = Resource.Instance.GetAssetManager().GetLoadedAssetBundle(_sAssetBundleName,
                out _sDownloadingError);
            if (bundle != null)
            {
                _request = bundle.assetBundle.LoadAssetAsync(_sAssetName, _type);

                return false;
            }
            else
                return true;
        }
        //-----------------------------------------------------------------------
        public override bool IsDone()
        {
            //  request occur error and it must return true
            if (_request == null && _sDownloadingError != null)
            {
                DebugEx.LogError(_sDownloadingError);
                return true;
            }

            return _request != null && _request.isDone;
        }
        //-----------------------------------------------------------------------
        public override string GetName()
        {
            return _sAssetName;
        }
    }

    public class AssetBundleLoadManifestOperation : AssetBundleLoadAssetOperationFull
    {
        public AssetBundleLoadManifestOperation(string prmBundleName, string prmAssetName, System.Type prmType)
            : base(prmBundleName, prmAssetName, prmType)
        {
        }
        //-----------------------------------------------------------------------
        public override bool Update()
        {
            base.Update();

            if (_request != null && _request.isDone)
            {
                Resource.Instance.GetAssetManager().BundleManifest = GetAsset<AssetBundleManifest>();
                return false;
            }
            else
                return true;
        }
    }
}