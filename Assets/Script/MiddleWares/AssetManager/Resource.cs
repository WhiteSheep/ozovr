﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace VBGames.PlayMaker.Actions
{
    public class AssetBundleName
    {
        public static string Particle = "particle";
        public static string Audio = "audio";
        public static string RoomObject = "roomobject";
    }

    public class Resource : MonoBehaviour
    {
        private static Resource _instance;
        private ResourceCache _cache;
        private AssetManager _assetManager;
        private LoadingState _loadingState;

        [SerializeField] private bool _preloadAssetBundles = true;
        [SerializeField] private bool _preloadAudio = true;

        public delegate IEnumerator AsyncCallback(AssetMap prmAssetMap, System.Object prmObject);

        protected Resource()
        {
            _loadingState = new LoadingState();
            _loadingState.AddDlCount();
        }

        //-----------------------------------------------------------------------
        public static Resource Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Resource();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public ResourceCache Cache
        {
            get { return _cache; }
        }

        //-----------------------------------------------------------------------
        public bool PreloadAssetBundles
        {
            get { return _preloadAssetBundles; }
            set { _preloadAssetBundles = value; }
        }

        //-----------------------------------------------------------------------
        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        //-----------------------------------------------------------------------
        private void Update()
        {
            if (_assetManager != null)
            {
                _assetManager.Update();
            }
        }

        //-----------------------------------------------------------------------
        public bool FinishedSetup()
        {
            return _loadingState.IsFinished();
        }

        //-----------------------------------------------------------------------
        public AssetManager GetAssetManager()
        {
            return _assetManager;
        }

        //-----------------------------------------------------------------------
        public IEnumerator Setup()
        {
            _cache = new ResourceCache();
            _assetManager = new AssetManager();

            //  Initialize AssetManager
#if UNITY_EDITOR
            _assetManager.Initialize("Standalone");
#else
        switch (AppSetting.TARGET_PLATFORM_TYPE)
        {
            case TargetPlatformType.Webgl:
                _assetManager.Initialize("Webgl");           
                break;
            case TargetPlatformType.Pc:
                _assetManager.Initialize("Standalone");
                break;
            default:
                DebugEx.Log("Unsupported Platform, AssetBundle can't find");
                break;
        }
#endif
            //  Wait AssetManager init complete
            while (!_assetManager.IsInitialized)
                yield return null;

            //  Check and pre-download all AssetBundles
            if (_preloadAssetBundles)
            {
                _assetManager.LoadAllAssetBundles();
                while (_assetManager.LoadedAssetBundleCount <= _assetManager.AssetBundleCount)
                    yield return null;

                if (_preloadAudio)
                    yield return StartCoroutine(_PreloadAudio());
            }

            _loadingState.RemoveDlCount();
        }

        //-----------------------------------------------------------------------
        public static Object LoadAsset(string prmAssetBundleName, string prmAssetName, Type prmType)
        {
            Object retObj = Instance.Cache.LoadAsset(prmAssetBundleName, prmAssetName);

            if (retObj == null)
            {
                retObj = Instance.GetAssetManager().LoadAssetSync(prmAssetBundleName, prmAssetName, prmType);
                Instance.Cache.AddAsset(prmAssetBundleName, prmAssetName, retObj);
            }

            return retObj;
        }

        //-----------------------------------------------------------------------
        public static T LoadAsset<T>(string prmAssetBundleName, string prmAssetName)
            where T : Object
        {
            Object retObj = Instance.Cache.LoadAsset<T>(prmAssetBundleName, prmAssetName);

            if (retObj == null)
            {
                retObj = Instance.GetAssetManager().LoadAssetSync(prmAssetBundleName, prmAssetName, typeof (T));
                Instance.Cache.AddAsset(prmAssetBundleName, prmAssetName, retObj);
            }

            return retObj as T;
        }

        //-----------------------------------------------------------------------
        public static AssetMap LoadAssets(List<AssetInfo> prmAssetInfo)
        {
            AssetMap retAssetMap = new AssetMap();

            foreach (AssetInfo child in prmAssetInfo)
            {
                Object o = LoadAsset(child.sAssetBundleName, child.sAssetName, child.tType);
                child.asset = o;

                retAssetMap.Add(child);
            }

            return retAssetMap;
        }

        //-----------------------------------------------------------------------
        public static AssetMap LoadAssetAsync(string prmAssetBundleName, string prmAssetName,
            Type prmType)
        {
            AssetInfo assetInfo = new AssetInfo(prmAssetBundleName, prmAssetName, prmType);
            return LoadAssetAsync(assetInfo);
        }

        //-----------------------------------------------------------------------
        public static AssetMap LoadAssetAsync(AssetInfo prmAssetInfo)
        {
            return LoadAssetsAsync(new List<AssetInfo>() {prmAssetInfo});
        }

        //-----------------------------------------------------------------------
        public static AssetMap LoadAssetsAsync(List<AssetInfo> prmAssetInfos)
        {
            AssetMap assetMap = new AssetMap();

            //  Load asset from cache or AssetBundle
            foreach (AssetInfo child in prmAssetInfos)
            {
                Object o = Instance.Cache.LoadAsset(child.sAssetBundleName, child.sAssetName);

                if (o)
                    child.asset = o;
                else
                {
                    AssetBundleLoadAssetOperation op = Instance.GetAssetManager().LoadAssetAsync(child.sAssetBundleName,
                        child.sAssetName, child.tType);
                    child.AssetLoadOp = op;
                }
                assetMap.Add(child);
            }

            return assetMap;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _PreloadAudio()
        {
            AssetBundle audioAB = _assetManager.LoadAssetBundle(AssetBundleName.Audio);

            //  Create load list
            List<AssetInfo> audioList = new List<AssetInfo>();
            foreach (string child in audioAB.GetAllAssetNames())
            {
                AssetInfo assetInfo = new AssetInfo(AssetBundleName.Audio, child, typeof (AudioClip));
                audioList.Add(assetInfo);
            }

            //  Load all audios with async
            AssetMap audioMap = LoadAssetsAsync(audioList);
            while (!audioMap.IsDone())
                yield return null;

            //  Deal with AudioClip's loading state
            foreach (var kv in audioMap.All)
            {
                AudioClip clip = kv.Value.GetAsset<AudioClip>();
                while (clip.loadState == AudioDataLoadState.Loading)
                    yield return null;

                if (clip.loadState == AudioDataLoadState.Failed)
                    DebugEx.LogError(string.Format("Loading audio <{0}> failed", kv.Key.Value));
            }
        }
    }
}