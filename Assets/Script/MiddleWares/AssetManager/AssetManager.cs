﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class LoadedAssetBundle
    {
        public AssetBundle assetBundle;
        public int iRefCount;

        public LoadedAssetBundle(AssetBundle prmAseetBundle)
        {
            assetBundle = prmAseetBundle;
            iRefCount = 1;
        }
    }

    public class AssetManager : VBGameObject
    {
        private Dictionary<string, LoadedAssetBundle> _dicLoadedAssetBundles;
        private Dictionary<string, string> _dicDownloadingError;
        private List<AssetBundleLoadOperation> _lAssetBundleLoadOperation;
        private List<string> _lDownloadingAssetBundles;
        private bool _bInitialized = false;
        private int _iAssetBundleCount = 0;
        private AssetBundleManifest _assetBundleManifest = null;

        public AssetManager()
        {
            //  initialize the loaded assetbundle dictionary
            _dicLoadedAssetBundles = new Dictionary<string, LoadedAssetBundle>();

            //  initialize the error dictionary
            _dicDownloadingError = new Dictionary<string, string>();

            //  initialize the AssetBundleLoadOperation list
            _lAssetBundleLoadOperation = new List<AssetBundleLoadOperation>();

            //  initialize the downloading list
            _lDownloadingAssetBundles = new List<string>();
        }
        //-----------------------------------------------------------------------
        public bool IsInitialized
        {
            get { return _bInitialized; }
        }
        //-----------------------------------------------------------------------
        public AssetBundleManifest BundleManifest
        {
            set
            {
                _assetBundleManifest = value;
                _bInitialized = true;
                _iAssetBundleCount = _assetBundleManifest.GetAllAssetBundles().Length;
            }
        }
        //-----------------------------------------------------------------------
        public int AssetBundleCount
        {
            get { return _iAssetBundleCount; }
        }
        //-----------------------------------------------------------------------
        public int LoadedAssetBundleCount
        {
            get { return _dicLoadedAssetBundles.Count; }
        }
        //-----------------------------------------------------------------------
        public override void Update()
        {
            //  update all operations
            for (int idx = 0; idx < _lAssetBundleLoadOperation.Count; ++idx)
                if (!_lAssetBundleLoadOperation[idx].Update())
                {
                    DebugEx.Log(string.Format("asset [FFFF00]<{0}>[-] load successful",
                        _lAssetBundleLoadOperation[idx].GetName()));
                    _lAssetBundleLoadOperation.RemoveAt(idx);
                }
                else
                    ++idx;
        }
        //-----------------------------------------------------------------------
        public AssetBundleLoadManifestOperation Initialize(string prmManifestAssetBundleName)
        {
            _bInitialized = false;

            //  load AssetBundle and dependence
            _LoadAssetBundle(prmManifestAssetBundleName, true);

            var loadManifestOp = new AssetBundleLoadManifestOperation(prmManifestAssetBundleName, "AssetBundleManifest",
                typeof (AssetBundleManifest));
            _lAssetBundleLoadOperation.Add(loadManifestOp);

            return loadManifestOp;
        }
        //-----------------------------------------------------------------------
        protected void _LoadAssetBundle(string prmAssetBundleName, bool prmIsLoadingAssetBundleManifest = false)
        {
            // check if assetbundle has loaded or downloading now
            bool isAlreadyProcessed = _LoadAssetBundleInternal(prmAssetBundleName, prmIsLoadingAssetBundleManifest);

            //  load dependencies need two conditions:
            //  1. this AssetBundle hasn't been loaded, which means it's dependencies haven't been loaded yet
            //  2. is not loading AssetBundleManifest
            if (!isAlreadyProcessed && !prmIsLoadingAssetBundleManifest)
                _LoadDependencies(prmAssetBundleName);
        }
        //-----------------------------------------------------------------------
        protected bool _LoadAssetBundleInternal(string prmAssetBundleName, bool prmIsDownloadingAssetBundleManifest)
        {
            LoadedAssetBundle bundle = null;

            //  check if exist assetbundle
            if (_dicLoadedAssetBundles.TryGetValue(prmAssetBundleName, out bundle))
            {
                ++bundle.iRefCount;
                return true;
            }

            //  if Downloading contains assetbudle, that means loading mission has already ran, there is no need do things repeatly
            if (_lDownloadingAssetBundles.Contains(prmAssetBundleName))
                return true;

            //  create www
            WWW download = null;
            string url = "";

#if UNITY_EDITOR
            url = Config.AssetBundleDownloadBasePath + "Standalone/" + prmAssetBundleName;
#else
            switch (AppSetting.TARGET_PLATFORM_TYPE)
            {
                case TargetPlatformType.Pc:
                    url = Config.AssetBundleDownloadBasePath + "Standalone/" + prmAssetBundleName;
                    break;
                case TargetPlatformType.Webgl:
                    url = Config.AssetBundleDownloadBasePath + "Webgl/" + prmAssetBundleName;
                    break;
            }
#endif


            if (prmIsDownloadingAssetBundleManifest)
            {
                Request request = new RequestWWW(url, (www) =>
                {
                    LoadedAssetBundle loadedAssetBundle = new LoadedAssetBundle(www.assetBundle);

                    _dicLoadedAssetBundles.Add(prmAssetBundleName, loadedAssetBundle);
                    _lDownloadingAssetBundles.Remove(prmAssetBundleName);
                });

                DownloadManager.DownloadAsync(request);
            }
            else
            {
                Request request = new RequestWWWLoadFromCacheOrDownload(url,
                    (www) =>
                    {
                        LoadedAssetBundle loadedAssetBundle = new LoadedAssetBundle(www.assetBundle);

                        _dicLoadedAssetBundles.Add(prmAssetBundleName, loadedAssetBundle);
                        _lDownloadingAssetBundles.Remove(prmAssetBundleName);
                    }
                    , _assetBundleManifest.GetAssetBundleHash(prmAssetBundleName), 0);

                DownloadManager.DownloadAsync(request);
            }

            _lDownloadingAssetBundles.Add(prmAssetBundleName);
            return false;
        }
        //-----------------------------------------------------------------------
        protected void _LoadDependencies(string prmAssetBundleName)
        {
            if (!_assetBundleManifest)
            {
                DebugEx.Log("Please load AssetBundleManifest - " + prmAssetBundleName);
                return;
            }

            string[] dependencies = _assetBundleManifest.GetAllDependencies(prmAssetBundleName);
            foreach (string dependence in dependencies)
                _LoadAssetBundleInternal(dependence, false);
        }
        //-----------------------------------------------------------------------
        public LoadedAssetBundle GetLoadedAssetBundle(string prmBundleName, out string prmLoadingError)
        {
            //  check downloading error info if exist
            if (_dicDownloadingError.TryGetValue(prmBundleName, out prmLoadingError))
                return null;

            //  check bundle if loaded
            LoadedAssetBundle retBundle = null;
            if (!_dicLoadedAssetBundles.TryGetValue(prmBundleName, out retBundle))
                return null;

            //  check dependencies - if no dependencies return AssetBundleDirectly
            if (_assetBundleManifest != null)
            {
                string[] dependencies = _assetBundleManifest.GetAllDependencies(prmBundleName);
                if (dependencies.Length == 0)
                    return retBundle;

                foreach (string dependence in dependencies)
                {
                    //  something error and muse return retbundle
                    if (_dicDownloadingError.ContainsKey(dependence))
                        return retBundle;

                    //  check dependence assetbundle has loaded
                    LoadedAssetBundle bundle = null;
                    _dicLoadedAssetBundles.TryGetValue(dependence, out bundle);

                    if (bundle == null)
                        return null;
                }
            }

            return retBundle;
        }
        //-----------------------------------------------------------------------
        public AssetBundleLoadAssetOperation LoadAssetAsync(string prmAssetBundleName, string prmAssetName,
            System.Type prmType)
        {
            _LoadAssetBundle(prmAssetBundleName);
            AssetBundleLoadAssetOperation operation = new AssetBundleLoadAssetOperationFull(prmAssetBundleName,
                prmAssetName,
                prmType);

            _lAssetBundleLoadOperation.Add(operation);

            return operation;
        }
        //-----------------------------------------------------------------------
        public Object LoadAssetSync(string prmAssetBundleName, string prmAssetName, System.Type prmType)
        {
            string errmsg;
            LoadedAssetBundle ab = GetLoadedAssetBundle(prmAssetBundleName, out errmsg);

            if (ab != null)
            {
                return ab.assetBundle.LoadAsset(prmAssetName, prmType);
            }
            else
            {
                DebugEx.Log(errmsg);
                return null;
            }
        }
        //-----------------------------------------------------------------------
        public void LoadAllAssetBundles()
        {
            if (_assetBundleManifest)
            {
                string[] abNames = _assetBundleManifest.GetAllAssetBundles();
                foreach (string child in abNames)
                    _LoadAssetBundle(child);
            }
        }
        //-----------------------------------------------------------------------
        public AssetBundle LoadAssetBundle(string prmAssetBundleName)
        {
            LoadedAssetBundle loadedAB;
            if (_dicLoadedAssetBundles.TryGetValue(prmAssetBundleName, out loadedAB))
                return loadedAB.assetBundle;
            
            return null;
        }
    }
}