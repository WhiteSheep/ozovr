﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>
    /// AssetMap used to get asset by AssetBundleName and AssetName, because AssetBundleName-AssetName is the only
    /// key to certain one Asset
    /// </summary>
    public class AssetMap : DoubleDictionary<string, string, AssetInfo>
    {
        public AssetMap() : base() { }

        //-----------------------------------------------------------------------
        public T GetAsset<T>(string prmAssetBundleName, string prmAssetName) where T : UnityEngine.Object
        {
            AssetInfo assetInfo = GetValue(prmAssetBundleName, prmAssetName);
            return assetInfo.GetAsset<T>();
        }
        //-----------------------------------------------------------------------
        public void Add(AssetInfo prmAssetInfo)
        {
            base.Add(prmAssetInfo.sAssetBundleName, prmAssetInfo.sAssetName, prmAssetInfo);
        }
        //-----------------------------------------------------------------------
        public bool IsDone()
        {
            foreach (var kv in All)
            {
                if (!kv.Value.IsDone())
                    return false;
            }

            return true;
        }
    }

}