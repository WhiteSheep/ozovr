﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace VBGames.PlayMaker.Actions
{
    public class ResourceCache
    {
        private DoubleDictionary<string, string, Object> _cache;

        public ResourceCache()
        {
            _cache = new DoubleDictionary<string, string, Object>();
        }
        //-----------------------------------------------------------------------
        public Object LoadAsset(string prmAssetBundleName, string prmAssetName)
        {
            return _cache.GetValue(prmAssetBundleName, prmAssetName);
        }
        //-----------------------------------------------------------------------
        public T LoadAsset<T>(string prmAssetBundleName, string prmAssetName) where T : Object
        {
            Object retValue = LoadAsset(prmAssetBundleName, prmAssetName);
            return retValue as T;
        }
        //-----------------------------------------------------------------------
        public void AddAsset(string prmAssetBundleName, string prmAssetName, Object prmGo)
        {
            if (prmGo != null)
                _cache.Add(prmAssetBundleName, prmAssetName, prmGo);
        }
    }
}
