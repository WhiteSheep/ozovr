﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>
    /// AssetInfo used to load asset when using Loader, and it can also store the loading operation
    /// </summary>
    public class AssetInfo
    {
        public string sAssetBundleName;
        public string sAssetName;
        public Type tType;
        public ArrayList arrParams;
        public Object asset = null;
        private AssetBundleLoadAssetOperation _loadAssetOp = null;

        public AssetBundleLoadAssetOperation AssetLoadOp
        {
            set { _loadAssetOp = value; }
            get { return _loadAssetOp; }
        }
        //-----------------------------------------------------------------------
        public AssetInfo(string prmAssetBundleName, string prmAssetName, Type prmType)
        {
            sAssetBundleName = prmAssetBundleName;
            sAssetName = prmAssetName;
            tType = prmType;

            arrParams = new ArrayList();
        }
        //-----------------------------------------------------------------------
        public T GetAsset<T>() where T : UnityEngine.Object
        {
            if (_loadAssetOp != null)
            {
                T retObj = _loadAssetOp.GetAsset<T>();
                Resource.Instance.Cache.AddAsset(sAssetBundleName, sAssetName, retObj);

                return retObj;
            }
            else if (asset != null)
                return asset as T;
            else
                return null;
        }
        //-----------------------------------------------------------------------
        public void AddParam(object prmObj)
        {
            arrParams.Add(prmObj);
        }
        //-----------------------------------------------------------------------
        public object GetParam(int prmIdx)
        {
            return arrParams[prmIdx];
        }
        //-----------------------------------------------------------------------
        public bool IsDone()
        {
            return _loadAssetOp.IsDone() || asset != null;
        }
    }
}
