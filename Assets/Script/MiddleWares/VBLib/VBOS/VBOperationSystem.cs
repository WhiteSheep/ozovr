﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using VBGames.Utils.Logger;

namespace VBGames.OS
{
    public static class VBOperationSystem
    {
        static TimeSpan prevCPUPc , currCPUPc;
        private static DateTime lastTime = DateTime.Now;

        private static PerformanceCounter cpuCounter;
        private static PerformanceCounter ramCounter;
        static VBOperationSystem()
        {
            prevCPUPc = new TimeSpan(0);
            currCPUPc = new TimeSpan(0);

            cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "0,0");
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        }

        public static float GetCurrentCpuUsage()
        {
            float retValue;

            //  计算两次调用间隔
            DateTime callTime = DateTime.Now;
            double refreshInterval = (callTime - lastTime).Ticks / 10000f;
            lastTime = callTime;

            prevCPUPc = currCPUPc;
            currCPUPc = new TimeSpan(0);

            //  统计CPU时间
            Process[] allProcesses = Process.GetProcesses();
            for (int cnt = 0; cnt < allProcesses.Length; cnt++)
            {
                currCPUPc += allProcesses[cnt].TotalProcessorTime;
            }
                
            TimeSpan newCPUTime = currCPUPc - prevCPUPc;

            VBLogger.Debug(string.Format("Cpu Usage:{0}, Calling Interval:{1}, Cpu Cores:{2}", newCPUTime.TotalMilliseconds, refreshInterval, Environment.ProcessorCount));

            //  计算CPU占用率
            retValue = (float) ((1.0f * newCPUTime.TotalMilliseconds / refreshInterval) / Environment.ProcessorCount);

            return retValue > 1.0f ? 1.0f : retValue;
        }

        public static long GetCurrentMemoryUsage()
        {
            long allMemoryUsed = 0;

            //  统计内存占用率
            Process[] allProcesses = Process.GetProcesses();
            for (int cnt = 0; cnt < allProcesses.Length; ++cnt)
            {
                allMemoryUsed += allProcesses[cnt].WorkingSet64;
            }

            return allMemoryUsed;
        }

    }
}
