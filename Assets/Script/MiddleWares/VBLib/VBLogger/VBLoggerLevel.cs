﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.Utils.Logger
{
    public enum VBLoggerLevel
    {
        None = 0,
        Error,
        Log,
        Debug
    }
}
