﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/**
 * VBLogger Lib v0.1
 * 
 * Yet another library for log system
 */
namespace VBGames.Utils.Logger
{
    public class VBLogger
    {
        public static VBLoggerLevel level = VBLoggerLevel.Log ;
        public static string timestamp = "yyyy-MM-dd HH:mm:ss:ffff";
        public static void Err(string logInfo)
        {
            if (VBLoggerLevel.Error <= level)
            {
                UnityEngine.Debug.LogError(string.Format("[Error][{0}][{1}]", DateTime.Now.ToString(timestamp), logInfo));
            }
        }

        public static void Log(string logInfo)
        {
            if (VBLoggerLevel.Log <= level)
            {
                UnityEngine.Debug.Log(string.Format("[Log][{0}][{1}]", DateTime.Now.ToString(timestamp), logInfo));
            }
        }

        public static void Debug(string logInfo)
        {
            if (VBLoggerLevel.Debug <= level)
            {
                UnityEngine.Debug.Log(string.Format("[Debug][{0}][{1}]", DateTime.Now.ToString(timestamp), logInfo));
            }
        }
    }
}
