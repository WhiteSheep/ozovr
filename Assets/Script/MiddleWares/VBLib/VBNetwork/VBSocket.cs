﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using VBGames.Utils.Logger;

/**
 * VBSocketServer Lib v0.1
 * 
 * Yet another library for C# socket programming
 */
namespace VBGames.Network
{
    public class VBSocket
    {
        public Socket socket;
        public byte[] buffer;
        public string remainData;
        
        /**
         * Create VBSocket with C# socket
         */
        public VBSocket(Socket prmSocket, int prmBufferSize = 8)
        {
            socket = prmSocket;
            buffer = new byte[1024 * prmBufferSize]; // Pre-Buffer 8K
            remainData = String.Empty;
        }

        /**
         * Create VBSocket with ip and port
         */
        public VBSocket(string prmIp, int prmPort, int prmListen = 100, int prmBufferSize = 8)
        {
            //  Create socket
            IPAddress ip = IPAddress.Parse(prmIp);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(ip, prmPort));
            socket.Listen(prmListen);

            //  Create socket buffer
            buffer = new byte[1024 * prmBufferSize]; // Pre-Buffer 8K
            remainData = String.Empty;
        }

        /**
         * Accept and return a new VBSocket with buffer
         */
        public VBSocket Accept(int prmBufferSize = 8)
        {
            Socket newSocket = socket.Accept();
            return new VBSocket(newSocket, prmBufferSize);
        }

        /**
         * Return true if socket connected or something need to read
         */
        public bool SocketConnected()
        {
            bool socketClosed = socket.Poll(1, SelectMode.SelectRead);
            bool nothingToRead = (socket.Available == 0);

            return !(socketClosed && nothingToRead);
        }

        /**
         * Receive with sync, timeout default is 0
         */
        public void Receive(int prmTimeout = 0)
        {
            socket.ReceiveTimeout = prmTimeout;
            Array.Clear(buffer, 0, buffer.Length);

            try
            {
                socket.Receive(buffer);
                remainData += Encoding.UTF8.GetString(buffer).TrimEnd('\0');
            }
            catch (Exception e)
            {
                
            }
        }

        /**
         * Process receive info, split and save remain
         */
        public List<string> AfterReceive(string prmCommandSplit)
        {
            String message = remainData;
            List<string> splitResult = new List<string>();

            //  remainData need to be split
            if (message.Contains(prmCommandSplit))
            {
                //  Split remainData by split
                splitResult = message.Split(new[] {prmCommandSplit}, StringSplitOptions.RemoveEmptyEntries).ToList();

                //  Last command is receiving now
                if (message.Substring(message.Length - prmCommandSplit.Length) != prmCommandSplit)
                {
                    remainData = splitResult.Last();
                    splitResult.RemoveAt(splitResult.Count - 1);
                }
                //  Remove split empty end
                else
                {
                    remainData = String.Empty;
                }
            }

            return splitResult;
        }

        /**
         * Receive and process together
         */
        public List<string> ReceiveAndProcess(string prmCommandSplit, int prmTimeout = 0)
        {
            Receive(prmTimeout);
            return AfterReceive(prmCommandSplit);
        }

        /**
         * Send data to remote service
         * return 0 success
         * return -1 connection timeout
         * return -2 send data error
         */
        public int Send(byte[] prmSendData, int prmTimeout = 1)
        {
            int sendFlag = 0;

            if (prmSendData == null || prmSendData.Length == 0)
            {
                VBLogger.Debug("Nothing to send");
                return 0;
            }

            int restLen = prmSendData.Length;
            int hasSend = 0;

            while (true)
            {
                if (socket.Poll(prmTimeout, SelectMode.SelectWrite))
                {
                    int sndLen = socket.Send(prmSendData, hasSend, restLen, SocketFlags.None);
                    restLen -= sndLen;
                    hasSend += sndLen;

                    if (restLen == 0)
                    {
                        break;
                    }
                    else
                    {
                        if (sndLen > 0)
                        {
                            continue;
                        }
                        else
                        {
                            sendFlag = -2;
                            break;
                        }
                    }
                }
                else
                {
                    sendFlag = -1;
                    break;
                }
            }

            return sendFlag;
        }

        /**
         * Send string to remote server
         */
        public int Send(string prmSendData, int prmTimeout)
        {
            return Send(Encoding.ASCII.GetBytes(prmSendData), prmTimeout);
        }

        /**
         * Release all socket resource
         */
        public void Close(SocketShutdown prmShutdownDirection = SocketShutdown.Both)
        {
            if (socket != null)
            {
                if (socket.Connected)
                {
                    socket.Shutdown(prmShutdownDirection);
                }            

                socket.Close();    
            }       
        }

        /**
         * Forcing close socket without shutdown operation
         */
        public void ForceClose()
        {
            if (socket != null)
            {
                socket.Close();
            }
        }
    }
}
