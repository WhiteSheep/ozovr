﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames
{
    public class VBLib
    {
        public static T ParseEnum<T>(string prmValue, T prmDefaultValue)
        {
            if (string.IsNullOrEmpty(prmValue))
            {
                return prmDefaultValue;
            }

            return (T) Enum.Parse(typeof (T), prmValue, true);
        }
    }
}
