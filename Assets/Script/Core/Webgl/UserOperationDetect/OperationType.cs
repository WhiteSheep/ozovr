﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>Operation Config
    /// <para>-----The default value is Idle-----</para>
    /// <para>Idle - Nothing will happen</para>
    /// <para>Create - User can create gameobject and add it to the world</para>
    /// <para>Erase - User can destroy gameobject from the world</para>
    /// <para>Modify - User can open modify gameobject properties in the world</para>
    /// </summary>
    public enum OperationType
    {
        Idle,
        Create,
        Erase,
        Modify,
        MenuSelect
    }
}
