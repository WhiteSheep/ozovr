﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Ozovr.SceneObject;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>
    /// 管理员使用的操作侦测模块
    /// </summary>
    public class OperationSensor
    {
        //  OperationSensor State
        public static OperationType operationType = OperationType.Idle;
        private bool _isEnable = false;

        //  basic var for RaycastWork
        private RaycastHit _raycastHit;
        private SceneObjectTemplate _holdUnitTemplate;
        private GameObject _holdToken;
        private GameObject _selectedObj;
        private bool _bIsCollide;
        private LayerMask _layerMask;

        public void Setup()
        {
            _layerMask = ~(1 << LayerMask.NameToLayer("Ignore Raycast"));
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            if (!_isEnable)
                return;

            //  Operation Sensor can't run
            if (!Camera.main)
                return;

            //  Raycast Calcluate
            Ray cameraRay = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2f, Screen.height/2f));
            if (Physics.Raycast(cameraRay, out _raycastHit, 1000, _layerMask))
            {
                //  Get selected object
                _selectedObj = _raycastHit.collider.gameObject;

                //  Draw selected object size
                GridOverlay.DrawBox(_selectedObj);

                if (operationType != OperationType.Create)
                {
                    _holdUnitTemplate = null;
                    if (_holdToken != null)
                    {
                        Assist.DestroyObject(_holdToken);
                        _holdToken = null;
                    }
                }

                if (operationType == OperationType.Create)
                {
                    //  Get Raycast position, normal and belong cube
                    Vector3 basePoint = World.GetCubeIndex(_raycastHit.point, _raycastHit.normal);

                    //  When hold UnitTemplate isn't null, the system will show token
                    if (_holdUnitTemplate != null)
                    {
                        Vector3 centerPoint = World.CalConstructionPlace(_raycastHit, basePoint, _holdToken);
                        _holdToken.transform.position = centerPoint;

                        //  Change color to show construction valid
                        //  Hack - Webgl doesn't support semi-transparent
                        float opacityColor = Mathf.Abs(0.4f*Mathf.Sin(Time.time*2)) + 0.4f;
                        if (AppSetting.TARGET_PLATFORM_TYPE != TargetPlatformType.Webgl)
                            Assist.ChangeOpacity(_holdToken, opacityColor);

                        _bIsCollide = World.IsCollide(_holdToken);

                        if (!_bIsCollide)
                            Assist.ChangeColor(_holdToken, AppSetting.VALID_SETTING_COLOR);
                        else
                            Assist.ChangeColor(_holdToken, AppSetting.INVALID_SETTING_COLOR);
                    }
                }
            }
        }

        //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            if (!_isEnable)
                return;

            //  Detect input
            _DetectInput();

            //  FeedBack user Operation
            _FeedBack();
        }

        //-----------------------------------------------------------------------
        private void _DetectInput()
        {
            if (Input.GetKeyDown(KeyCode.C))
                operationType = OperationType.Create;
            else if (Input.GetKeyDown(KeyCode.E))
                operationType = OperationType.Erase;
            else if (Input.GetKeyDown(KeyCode.I))
                operationType = OperationType.Idle;

            if (Input.anyKeyDown)
            {
                bool isUseable = false;

                if (operationType == OperationType.Create)
                {
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("WoodenDesk");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Flower");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Fruits");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Candle");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha5))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Cense");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha6))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Tablet");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Flower_DP");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha8))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("PictureFrame");
                        isUseable = true;
                    }
                    else if (Input.GetKeyDown(KeyCode.Alpha9))
                    {
                        _holdUnitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById("Wreath");
                        isUseable = true;
                    }
                }

                if (isUseable)
                {
                    if (_holdToken)
                        Assist.DestroyObject(_holdToken);

                    _holdToken = _holdUnitTemplate.CreateSceneObject();
                    _holdToken.tag = Tag.Untagged;
                    _holdToken.SetActive(true);
                    Assist.ChangeLayer(_holdToken, LayerMask.NameToLayer("Ignore Raycast"));
                }
            }
        }

        //-----------------------------------------------------------------------
        private void _FeedBack()
        {
            //  rotate the unit by scroll
            if (_holdToken != null)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    _holdToken.transform.Rotate(Vector3.up, 90);
                else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                    _holdToken.transform.Rotate(Vector3.up, -90);
            }

            //  place the unit by click left button
            if (Input.GetMouseButtonDown(1))
            {
                if (operationType == OperationType.Erase)
                {
                    //  Check if this object can be erased
                    if (_selectedObj.tag == Tag.ExportGameObject)
                    {
                        if (_selectedObj.GetComponent<SceneObjectDataCtrl>().srcTemplate.unitFlags.Contains(UnitFlag.Deletable))
                        {
                            World.Remove(_selectedObj);
                            Assist.DestroyObject(_selectedObj);
                            _selectedObj = null;
                        }
                    }
                }
                else if (operationType == OperationType.Create)
                {
                    if (_holdUnitTemplate != null && !_bIsCollide)
                    {
                        GameObject placeObject = _holdUnitTemplate.CreateSceneObject();
                        placeObject.name = _holdToken.name;
                        placeObject.SetActive(true);
                        placeObject.transform.position = _holdToken.transform.position;
                        placeObject.transform.rotation = _holdToken.transform.rotation;

                        World.Add(placeObject);
                    }
                }
            }
        }

        //-----------------------------------------------------------------------
        public void SetEnable(bool prmEnable)
        {
            _isEnable = prmEnable;
        }
    }
}