﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ozovr.SceneObject;
using UnityEngine;
using VBGames.PlayMaker.UI;

namespace VBGames.PlayMaker.Actions
{
    public class UserOperationDectector
    {
        private RaycastHit _raycastHit;
        private LayerMask _layerMask;
        private GameObject _selectedGameObject;

        private bool _enabled = false;

        public UserOperationDectector()
        {
            
        }

        public UserOperationDectector Setup()
        {
            _layerMask = ~(1 << LayerMask.NameToLayer("Ignore Raycast"));
            return this;
        }

        public void Update()
        {
            //  没有主摄像机或者未开启时不进行任何捕捉操作
            if (!Camera.main || !_enabled)
                return;

            //  捕获选择的场景对象
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(cameraRay, out _raycastHit, 1000, _layerMask))
            {
                _selectedGameObject = _raycastHit.collider.gameObject;

                var dataCtrl = _selectedGameObject.GetComponent<SceneObjectDataCtrl>();
                if (dataCtrl != null && dataCtrl.IsReplacable())
                {
                    //  被选择物品显示体积框（能换的）
                    GridOverlay.DrawBox(_selectedGameObject);

                    //  替换模型操作
                    if (VbInputManager.GetKeyDown(KeyCode.Mouse1))
                    {
                        UIManager.Instance.SetUserObjectReplaceMenuState(true, _selectedGameObject);
                    }
                }
            }
        }

        public void LateUpdate()
        {
            
        }

        public void SetEnable(bool prmEnable)
        {
            _enabled = prmEnable;
        }
    }
}
