﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public abstract class VBGameObject
    {
        public abstract void Update();
    }
}
