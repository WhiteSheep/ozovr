﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ozovr.SceneObject;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>
    /// World is a static class, it contains all units build in the world, and provide all methods for World calculate
    /// </summary>
    public class World : MonoBehaviour
    {
        private static List<GameObject> _dicUnitsInWorld = new List<GameObject>();

        /// <summary>
        /// Clear all things in the world
        /// </summary>
        public static void Clear()
        {
            foreach (GameObject obj in _dicUnitsInWorld)
                Destroy(obj);

            _dicUnitsInWorld.Clear();
        }

        /// <summary>
        /// Return true if gameobject out of world
        /// </summary>
        /// <param name="prmGameObject"></param>
        /// <returns></returns>
        public static bool OutOfWorld(GameObject prmGameObject)
        {
            return false;
        }

        /// <summary>
        /// Add gameobject to world
        /// </summary>
        /// <param name="prmGameObject"></param>
        public static void Add(GameObject prmGameObject)
        {
            _dicUnitsInWorld.Add(prmGameObject);
        }

        /// <summary>
        /// Remove gameobject from world
        /// </summary>
        /// <param name="prmGameObject"></param>
        public static void Remove(GameObject prmGameObject)
        {
            _dicUnitsInWorld.Remove(prmGameObject);
        }

        /// <summary>
        /// Return true if gameobject is collide in the world
        /// </summary>
        /// <param name="prmGameObject"></param>
        /// <returns></returns>
        public static bool IsCollide(GameObject prmGameObject)
        {
            bool retCollide = false;

            foreach (GameObject unit in _dicUnitsInWorld)
            {
                //  two gameObject Intersect, but one condition is removed, we allow share face
                retCollide =
                    unit.GetComponent<Collider>().bounds.Intersects(prmGameObject.GetComponent<Collider>().bounds) &&
                    (!IfShareFace(prmGameObject, unit));

                if (retCollide) break;
            }

            return retCollide;
        }

        /// <summary>
        /// Return true if two gameObject share a plane (xOy, yOz, xOz)
        /// </summary>
        /// <param name="prmObjA"></param>
        /// <param name="prmObjB"></param>
        /// <returns></returns>
        public static bool IfShareFace(GameObject prmObjA, GameObject prmObjB)
        {
            Vector3 minA, minB, maxA, maxB;
            Assist.GetAABBSize(prmObjA, out minA, out maxA);
            Assist.GetAABBSize(prmObjB, out minB, out maxB);

            return (Assist.FloatEqual(maxA.x, minB.x) ||
                    Assist.FloatEqual(maxB.x, minA.x) ||
                    Assist.FloatEqual(maxA.y, minB.y) ||
                    Assist.FloatEqual(maxB.y, minA.y) ||
                    Assist.FloatEqual(maxA.z, minB.z) ||
                    Assist.FloatEqual(minA.z, maxB.z));
        }

        /// <summary>
        /// Return a Vector3, which is the center of the cube, this cube is beside to the hit point cube according to normal
        /// </summary>
        /// <param name="prmHitPoint"></param>
        /// <param name="prmNormal"></param>
        /// <returns></returns>
        public static Vector3 GetCubeIndex(Vector3 prmHitPoint, Vector3 prmNormal)
        {
            Vector3 retPoint = prmHitPoint;

            if (Assist.Vec3Equal(prmNormal, Vector3.up))
            {
                retPoint = new Vector3(MathfEx.Round(retPoint.x), Mathf.Ceil(retPoint.y),
                    MathfEx.Round(retPoint.z));
            }
            else if (Assist.Vec3Equal(prmNormal, Vector3.down))
            {
                retPoint = new Vector3(MathfEx.Round(retPoint.x), Mathf.Floor(retPoint.y),
                    MathfEx.Round(retPoint.z));
            }
            else if (Assist.Vec3Equal(prmNormal, Vector3.left))
            {
                retPoint = new Vector3(Mathf.Floor(retPoint.x), MathfEx.Round(retPoint.y),
                    MathfEx.Round(retPoint.z));
            }
            else if (Assist.Vec3Equal(prmNormal, Vector3.right))
            {
                retPoint = new Vector3(Mathf.Ceil(retPoint.x), MathfEx.Round(retPoint.y),
                    MathfEx.Round(retPoint.z));
            }
            else if (Assist.Vec3Equal(prmNormal, Vector3.forward))
            {
                retPoint = new Vector3(MathfEx.Round(retPoint.x), MathfEx.Round(retPoint.y),
                    Mathf.Ceil(retPoint.z));
            }
            else if (Assist.Vec3Equal(prmNormal, Vector3.back))
            {
                retPoint = new Vector3(MathfEx.Round(retPoint.x), MathfEx.Round(retPoint.y),
                    Mathf.Floor(retPoint.z));
            }

            return retPoint;
        }


        /// <summary>
        /// Calculate the construction place of gameobject with RaycastHit, Basepoint in world
        /// </summary>
        /// <param name="prmRaycastHit"></param>
        /// <param name="prmBasePoint"></param>
        /// <param name="prmGameObject"></param>
        /// <returns></returns>
        public static Vector3 CalConstructionPlace(RaycastHit prmRaycastHit, Vector3 prmBasePoint,
            GameObject prmGameObject)
        {
            GameObject obj = prmGameObject;
            RaycastHit raycastHit = prmRaycastHit;
            Vector3 basePoint = prmBasePoint;

            //  point for center
            Vector3 centerPoint = Vector3.zero;
            Vector3 maxPoint, minPoint;
            Assist.GetAABBSize(obj, out minPoint, out maxPoint);

            //  Calculate center
            //  First - Calculate main direction
            //  Second - Calculate fix for other direction, this method is used for even-cube-line direction
            if (Assist.Vec3Equal(raycastHit.normal, Vector3.up) || Assist.Vec3Equal(raycastHit.normal, Vector3.down))
            {
                if (Assist.Vec3Equal(raycastHit.normal, Vector3.up))
                {
                    basePoint += Vector3.down / 2;
                    centerPoint = basePoint + new Vector3(
                        0f, (maxPoint.y - minPoint.y) / 2f, 0f);
                }
                else if (Assist.Vec3Equal(raycastHit.normal, Vector3.down))
                {
                    basePoint += Vector3.up / 2;
                    centerPoint = basePoint + new Vector3(
                        0f, -(maxPoint.y - minPoint.y) / 2f, 0f);
                }

                if (Assist.IsEven(maxPoint.x - minPoint.x))
                    centerPoint += Vector3.right / 2f;
                if (Assist.IsEven(maxPoint.z - minPoint.z))
                    centerPoint += Vector3.forward / 2f;
            }
            else if (Assist.Vec3Equal(raycastHit.normal, Vector3.left) ||
                     Assist.Vec3Equal(raycastHit.normal, Vector3.right))
            {
                if (Assist.Vec3Equal(raycastHit.normal, Vector3.left))
                {
                    basePoint += Vector3.right / 2;
                    centerPoint = basePoint + new Vector3(
                        -(maxPoint.x - minPoint.x) / 2f, 0f, 0f);
                }
                else if (Assist.Vec3Equal(raycastHit.normal, Vector3.right))
                {
                    basePoint += Vector3.left / 2;
                    centerPoint = basePoint + new Vector3(
                        (maxPoint.x - minPoint.x) / 2f, 0f, 0f);
                }

                if (Assist.IsEven(maxPoint.y - minPoint.y))
                    centerPoint += Vector3.up / 2f;
                if (Assist.IsEven(maxPoint.z - minPoint.z))
                    centerPoint += Vector3.forward / 2f;
            }
            else if (Assist.Vec3Equal(raycastHit.normal, Vector3.forward) ||
                     Assist.Vec3Equal(raycastHit.normal, Vector3.back))
            {
                if (Assist.Vec3Equal(raycastHit.normal, Vector3.forward))
                {
                    basePoint += Vector3.back / 2;
                    centerPoint = basePoint + new Vector3(
                        0f, 0f, (maxPoint.z - minPoint.z) / 2f);
                }
                else if (Assist.Vec3Equal(raycastHit.normal, Vector3.back))
                {
                    basePoint += Vector3.forward / 2;
                    centerPoint = basePoint + new Vector3(
                        0f, 0f, -(maxPoint.z - minPoint.z) / 2f);
                }

                if (Assist.IsEven(maxPoint.x - minPoint.x))
                    centerPoint += Vector3.right / 2f;
                if (Assist.IsEven(maxPoint.y - minPoint.y))
                    centerPoint += Vector3.up / 2f;
            }
            else
            {
                DebugEx.Log("This plane is not flat and can't set anything");
            }

            //  If Stickable, it should be moved to surface and chane rotate
            if (prmGameObject.GetComponent<SceneObjectDataCtrl>().srcTemplate.unitFlags.Contains(UnitFlag.Stickable))
            {
                if (Assist.Vec3Equal(MathfEx.Vec3Abs(raycastHit.normal), Vector3.forward))
                {
                    prmGameObject.transform.rotation = Quaternion.Euler(prmGameObject.transform.rotation.x,
                        (raycastHit.normal.z + 1) * -90f, prmGameObject.transform.rotation.z);

                    centerPoint.z = raycastHit.point.z + raycastHit.normal.z * (maxPoint.z - minPoint.z);
                }
                else if (Assist.Vec3Equal(MathfEx.Vec3Abs(raycastHit.normal), Vector3.up))
                    centerPoint.y = raycastHit.point.y + raycastHit.normal.y * (maxPoint.y - minPoint.y);
                else if (Assist.Vec3Equal(MathfEx.Vec3Abs(raycastHit.normal), Vector3.right))
                {
                    prmGameObject.transform.rotation = Quaternion.Euler(prmGameObject.transform.rotation.x,
                        raycastHit.normal.x * -90f, prmGameObject.transform.rotation.z);
                    centerPoint.x = raycastHit.point.x + raycastHit.normal.x * (maxPoint.x - minPoint.x);
                }
            }

            return centerPoint;
        }
    }

}
