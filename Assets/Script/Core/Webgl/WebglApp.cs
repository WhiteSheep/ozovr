﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class WebglApp : SimpleApp
    {
        public override IEnumerator LateSetup()
        {
            GameManager.Instance.Setup();
            yield break;
        }
    }
}