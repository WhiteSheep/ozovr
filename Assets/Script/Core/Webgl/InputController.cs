﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VBGames.Utils.Logger;

namespace VBGames.PlayMaker.Actions
{
    public static class ComponentController
    {

        public static bool IsNeedLockAndHideCursor()
        {
            return VbInputManager.GetKeyDown(KeyCode.Mouse0) &&
                   mouseLookMode != MouseLookMode.DragMode;
        }

        //-----------------------------------------------------------------------
        public static void LockAndHideCursor()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        //-----------------------------------------------------------------------
        public static void UnLockAndShowCursor()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        //-----------------------------------------------------------------------
        public static void ConfineAndShowCursor()
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }

        //-----------------------------------------------------------------------
        public static void SetCameraInputEnable(bool prmEnable)
        {
            GameObject fpsCtrl = GameManager.Instance.GetMainCameraController();
            if (fpsCtrl)
            { 
                fpsCtrl.GetComponent<FirstPersonController>().enabled = prmEnable;
            }
        }

        //-----------------------------------------------------------------------
        public static MouseLookMode mouseLookMode
        {
            get
            {
                return GameManager.Instance.GetMainCameraController().GetComponent<FirstPersonController>().LookMode;
            }
            set
            {
                GameManager.Instance.GetMainCameraController().GetComponent<FirstPersonController>().LookMode = value;
            }
        }
    }
}