﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Ozovr.SceneObject;
using VBGames.PlayMaker.UI;

namespace VBGames.PlayMaker.Actions
{
    public class GameManager : MonoBehaviour
    {
        private GameObject _mainCameraController;
        private static GameManager _instance;
        private SceneCreator _sceneCreator;
        private OperationSensor _operationSensor;
        private UserOperationDectector _userOptDectector;

        private GameState _gameState;

        private bool _consoleOpened;

        protected GameManager()
        {
        }

        //-----------------------------------------------------------------------
        public static GameManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameManager();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        private void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        //-----------------------------------------------------------------------
        private void Start()
        {
        }

        //-----------------------------------------------------------------------
        private void Update()
        {
            //  Update game state
            if (_gameState != null)
            {
                GameState state = _gameState.OnHandleInput();
                _ChangeState(state);

                state = _gameState.Update();
                _ChangeState(state);
            }

            //  Open/Close Console Menu
            if (VbInputManager.GetKeyDown(KeyCode.BackQuote))
            {
                _consoleOpened = !_consoleOpened;
                //UIController.Instance.SetConsoleActive(_consoleOpened);
                ComponentController.SetCameraInputEnable(!_consoleOpened);

                if (_consoleOpened)
                    ComponentController.UnLockAndShowCursor();
            }

            //  Debug System
            if (VbInputManager.GetKeyDown(KeyCode.Escape))
            {
                Debug.Break();
                Application.Quit();
            }
        }

        //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            if (_gameState != null)
                _gameState.LateUpdate();
        }

        //-----------------------------------------------------------------------
        public void Setup()
        {
            //  Create SceneCreator
            _sceneCreator = new SceneCreator();

            //  Setup OperationSensor
            _operationSensor = new OperationSensor();
            _operationSensor.Setup();

            //  Setup UserOperationDetector
            _userOptDectector = new UserOperationDectector().Setup();

            //  Setup SceneObjectTemplateMgr
            SceneObjectTemplateMgr.Instance.Init();

            //  Set InitialState to start game
            _ChangeState(new InitialState());
        }

        //-----------------------------------------------------------------------
        public SceneCreator GetSceneCreator()
        {
            return _sceneCreator;
        }

        //-----------------------------------------------------------------------
        public OperationSensor GetOperationSensor()
        {
            return _operationSensor;
        }

        //-----------------------------------------------------------------------
        public UserOperationDectector GetUserOperationDectector()
        {
            return _userOptDectector;
        }

        //-----------------------------------------------------------------------
        public GameObject GetMainCameraController()
        {
            return _mainCameraController;
        }

        //-----------------------------------------------------------------------
        public void SetUserOperationDetectorActive(bool prmValue)
        {
            _userOptDectector.SetEnable(prmValue);
        }


        //-----------------------------------------------------------------------
        public void RegistMainCameraController(GameObject prmMainCamCtrl)
        {
            if (_mainCameraController != null)
                Destroy(_mainCameraController);

            _mainCameraController = prmMainCamCtrl;
        }

        //-----------------------------------------------------------------------
        private void _ChangeState(GameState prmGamestate)
        {
            if (_gameState != prmGamestate)
            {
                if (_gameState != null)
                    _gameState.OnExit();

                _gameState = prmGamestate;
                _gameState.OnEnter();
            }
        }

        //-----------------------------------------------------------------------
        public void SaveScene(string prmName)
        {
            if (prmName != "")
            {
                UIManager.Instance.SetDataLoadingWindowState(true);
                StartCoroutine(_sceneCreator.SaveSceneToXml(prmName));
            }
        }

        //-----------------------------------------------------------------------
        public void LoadScene(string prmName)
        {
            if (prmName != "")
            {
                UIManager.Instance.SetBigLoadingWindowState(true);
                ComponentController.SetCameraInputEnable(false);
                _sceneCreator.LoadScene(prmName);
            }
        }

        //-----------------------------------------------------------------------
        /// Application.ExternalCall by javascript on Index.html
        //-----------------------------------------------------------------------
        public void RecieveRoomId(string prmId)
        {
            User.virtualMourningHallId = Convert.ToInt32(prmId);
            DebugEx.Log("Recieve room id " + prmId);
        }
    }
}