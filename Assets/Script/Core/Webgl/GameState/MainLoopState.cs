﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class MainLoopState : GameState
    {
        private bool _userOperationDectectSwitch = true;
        private bool _userCreateSwitch = true;
        public override void OnEnter()
        {
            //  Play 
            if (User.IsUserOrGuest())
            {
                Assist.StartCoroutine(CameraAnimation);
            }
        }

        //-----------------------------------------------------------------------
        public override GameState OnHandleInput()
        {
            if (ComponentController.IsNeedLockAndHideCursor())
                ComponentController.LockAndHideCursor();

            //  按F10打开编辑模式
            if (Input.GetKeyDown(KeyCode.F10))
            {
                GameManager.Instance.GetUserOperationDectector().SetEnable(_userOperationDectectSwitch);
                _userOperationDectectSwitch = !_userOperationDectectSwitch;
            }

            //  按F9打开创造模式
            if (Input.GetKeyDown(KeyCode.F9))
            {
                GameManager.Instance.GetOperationSensor().SetEnable(_userCreateSwitch);
                _userCreateSwitch = !_userCreateSwitch;
            }
               
            return this;
        }

        //-----------------------------------------------------------------------
        public override GameState Update()
        {
            GameManager.Instance.GetOperationSensor().Update();
            GameManager.Instance.GetUserOperationDectector().Update();
            return this;
        }

        //-----------------------------------------------------------------------
        public override void LateUpdate()
        {
            GameManager.Instance.GetOperationSensor().LateUpdate();
            GameManager.Instance.GetUserOperationDectector().LateUpdate();
        }

        //-----------------------------------------------------------------------
        /// A temp animation method - should be refactor to scrpit and attach to camera
        //-----------------------------------------------------------------------
        public IEnumerator CameraAnimation()
        {
            ComponentController.SetCameraInputEnable(false);

            int elapsed = 0;
            while (elapsed < 60)
            {
                GameObject mainCamCtrl = GameManager.Instance.GetMainCameraController();
                if (mainCamCtrl)
                    mainCamCtrl.transform.Translate(new Vector3(0f, 0f, 0.2f));

                ++elapsed;
                yield return 0;
            }

            ComponentController.SetCameraInputEnable(true);
        }
    }
}