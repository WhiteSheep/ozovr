﻿using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class GameState
    {
        public GameState()
        {
        }

        //-----------------------------------------------------------------------
        public virtual void OnEnter()
        {
        }

        //-----------------------------------------------------------------------
        public virtual void OnExit()
        {
        }

        //-----------------------------------------------------------------------
        public virtual GameState Update()
        {
            return this;
        }

        //-----------------------------------------------------------------------
        public virtual void LateUpdate()
        {
        }

        //-----------------------------------------------------------------------
        public virtual GameState OnHandleInput()
        {
            return this;
        }
    }
}