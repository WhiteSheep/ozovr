﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    public class MenuState : GameState
    {
        private int _oepnWindowCall;

        public override void OnEnter()
        {
            _oepnWindowCall = 1;
            ComponentController.UnLockAndShowCursor();
        }

        //-----------------------------------------------------------------------
        public override GameState OnHandleInput()
        {
            return this;
        }

        //-----------------------------------------------------------------------
        public override void OnExit()
        {
            ComponentController.LockAndHideCursor();
        }
    }
}