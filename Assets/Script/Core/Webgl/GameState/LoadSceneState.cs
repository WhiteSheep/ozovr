﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VBGames.PlayMaker.UI;

namespace VBGames.PlayMaker.Actions
{
    internal class LoadSceneState : GameState
    {
        private SceneCreator _sceneCreator;

        public override void OnEnter()
        {
            UIManager.Instance.SetBigLoadingWindowState(true);
            ComponentController.SetCameraInputEnable(false);

            _sceneCreator = GameManager.Instance.GetSceneCreator();
            _sceneCreator.LoadScene(User.SceneFilePath, _CallbackLoaded);
        }

        //-----------------------------------------------------------------------
        public override void OnExit()
        {
            UIManager.Instance.SetBigLoadingWindowState(false);
            ComponentController.SetCameraInputEnable(true);
        }

        //-----------------------------------------------------------------------
        public override GameState Update()
        {
            if (!_sceneCreator.IsDone)
            {
                return this;
            }
            else
            {
                _CreateMainCamera();
                return new MainLoopState();
            }
        }

        //-----------------------------------------------------------------------
        private void _CreateMainCamera()
        {
            //  Create and regist main camera controller
            GameObject mainCam = Resource.LoadAsset<GameObject>(AssetBundleName.RoomObject, "FPSController.prefab");
            GameObject instCam = Assist.Instantiate(mainCam);
            instCam.SetActive(true);
            instCam.transform.position.Set(40f, 2.18f, 15f);
            GameManager.Instance.RegistMainCameraController(instCam);

            if (true || User.IsUserOrGuest())
                ComponentController.mouseLookMode = MouseLookMode.DragMode;
        }

        //-----------------------------------------------------------------------
        private void _CallbackLoaded(List<GameObject> prmInitedGameObject)
        {
            foreach (GameObject go in prmInitedGameObject)
                World.Add(go);
        }
    }
}