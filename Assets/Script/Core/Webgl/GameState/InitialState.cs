﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VBGames.PlayMaker.UI;

namespace VBGames.PlayMaker.Actions
{
    internal class InitialState : GameState
    {
        private DownloadingRequest _getUserInfoRequest;
        private DownloadingRequest _getUserSceneRequest;
        private bool _initialFinished = false;

        public override void OnEnter()
        {
            UIManager.Instance.SetBigLoadingWindowState(true);
            Assist.StartCoroutine(_InitialUser);
        }

        //-----------------------------------------------------------------------
        public override void OnExit()
        {

        }

        //-----------------------------------------------------------------------
        public override GameState OnHandleInput()
        {
            if (_initialFinished)
                return new LoadSceneState();

            return this;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _InitialUser()
        {
            //  Get UserInfo
#if UNITY_EDITOR
            User.Id = 6;
            User.Role = RoleType.NormalUser;
#else
            _getUserInfoRequest = new Request(Config.GetUserInfoPort).Start();
            while (!_getUserInfoRequest.IsDone())
                yield return null;
            GetUserInfo(_getUserInfoRequest.www);

#endif
            //  Deal with user by RoleType
            if (true || User.IsUserOrGuest())
            {
#if UNITY_EDITOR
                User.virtualMourningHallId = 6;
#else
                UIController.SetLoadingMsg("Getting user scene config file ... ...");
                Application.ExternalCall("GetRoomId");
                while (User.virtualMourningHallId < 0)
                    yield return null;
#endif
            }
            else if (User.IsAdmin())
                User.virtualMourningHallId = 123456;

            //  Get scene file path
            WWWForm form = new WWWForm();
            form.AddField("userId", User.Id);
            form.AddField("virtualMourningHall_id", User.virtualMourningHallId);

            //  Get scene information
            _getUserSceneRequest = new RequestWWW(Config.GetUserSceneInfoPort, null, form).Start();
            while (!_getUserSceneRequest.IsDone())
                yield return null;
            GetUserScenePath(_getUserSceneRequest.www);

            _initialFinished = true;
        }

        //-----------------------------------------------------------------------
        public static void GetUserInfo(WWW prmWWW)
        {
            XMLDom xmlDoc = new XMLDom();
            xmlDoc.LoadWithData(prmWWW.text);

            int errorCode = Convert.ToInt32(xmlDoc.GetElementValue("./errorCode"));
            string errorInfo = xmlDoc.GetElementValue("./errorInfo");

            if (errorCode == Config.InterfaceNoError)
            {
                User.Id = Convert.ToInt32(xmlDoc.GetElementValue("./userId"));
                User.Role = (RoleType)Enum.Parse(typeof(RoleType), xmlDoc.GetElementValue("./role"));
            }
            else
                DebugEx.LogError(string.Format("error code {0} : {1}", errorCode, errorInfo));
        }

        //-----------------------------------------------------------------------
        public void GetUserScenePath(WWW prmWWW)
        {
            XMLDom xmlDoc = new XMLDom();
            xmlDoc.LoadWithData(prmWWW.text);

            int errorCode = Convert.ToInt32(xmlDoc.GetElementValue("./errorCode"));
            string errorInfo = xmlDoc.GetElementValue("./errorInfo");
            if (errorCode == Config.InterfaceNoError)
            {
                User.SceneFilePath = Config.FileServerBasePath + xmlDoc.GetElementValue("./contentPath");
            }
            else
                DebugEx.LogError(string.Format("error code {0} : {1}", errorCode, errorInfo));
        }
    }
}