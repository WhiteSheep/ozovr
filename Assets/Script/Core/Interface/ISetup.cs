﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    public interface IEnumerableSetup
    {
        IEnumerator Setup();
        bool FinishedSetup();
    }

    public interface ISetup
    {
        void Setup();
        bool FinishedSetup();
    }
}
