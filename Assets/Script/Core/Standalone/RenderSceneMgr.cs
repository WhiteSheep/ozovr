﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using VBGames.Utils.Logger;

namespace VBGames.PlayMaker.Actions
{
    class RenderSceneMgr : ISetup
    {
        private List<RenderPlace> _usedPlace;
        private Queue<RenderPlace> _emptyPlace; 

        private SceneCreator _sceneCreator;
        private Vector2 _roomCounts;
        private Vector2 _roomSize;

        private bool _isSetuped;

        public void Setup()
        {
            _usedPlace = new List<RenderPlace>();
            _emptyPlace = new Queue<RenderPlace>();
            _sceneCreator = new SceneCreator();
            
            _roomCounts = new Vector2(Config.RPlaceInfo.xCount, Config.RPlaceInfo.yCount);
            _roomSize = new Vector2(Config.RPlaceInfo.xSize, Config.RPlaceInfo.ySize);

            for (int i = 1; i <= _roomCounts.x; ++i)
                for (int j = 1; j <= _roomCounts.y; ++j)
                {
                    GameObject obj = new GameObject();
                    RenderPlace rPlace = new RenderPlace(AppSetting.DEFAULT_ROOM_NAME, obj);

                    // x-z field
                    obj.transform.position = new Vector3(i * _roomSize.x, 0.0f, j * _roomSize.y);

                    _emptyPlace.Enqueue(rPlace);
                }

            _isSetuped = true;
        }

        public bool FinishedSetup()
        {
            return _isSetuped;
        }

        public void FreeRenderPlace(string prmRenderPlaceName)
        {
            foreach (RenderPlace item in _usedPlace)
            {
                if (item.PlaceName == prmRenderPlaceName)
                {
                    FreeRenderPlace(item);
                    break;
                }
            }
        }

        public void FreeRenderPlace(RenderPlace prmRenderPlace)
        {
            //  Remove all gameobjects in renderplace
            prmRenderPlace.RemoveAll();

            //  Remove place from used list and enqueue empty
            _usedPlace.Remove(prmRenderPlace);
            _emptyPlace.Enqueue(prmRenderPlace);
        }

        public float GetRenderPlaceUsage()
        {
            return _usedPlace.Count /(float) (_usedPlace.Count + _emptyPlace.Count);
        }

        public bool EmptyRenderPlaceExist()
        {
            return _emptyPlace.Count != 0;
        }

        private bool _GetEmptyRenderPlace(out RenderPlace outPlace)
        {
            outPlace = null;

            if (_emptyPlace.Count != 0)
            {
                outPlace = _emptyPlace.Dequeue();
                _usedPlace.Add(outPlace);

                return true;
            }
            else
            {
                VBLogger.Debug("没有足够场地分配");
            }
           
            return false;
        }

        private bool _IsCommandDuplicate(RenderCommand prmCmd)
        {
            foreach (RenderPlace item in _usedPlace)
            {
                if (item.occurCmd.CompareTo(prmCmd) == 0)
                {
                    return true;
                }
            }

            return false;
        }

        private string _GetUserScenePath(WWW prmWWW)
        {
            XMLDom xmlDoc = new XMLDom();
            xmlDoc.LoadWithData(prmWWW.text);

            int errorCode = Convert.ToInt32(xmlDoc.GetElementValue("./errorCode"));
            string errorInfo = xmlDoc.GetElementValue("./errorInfo");

            if (errorCode == Config.InterfaceNoError)
                return Config.FileServerBasePath + xmlDoc.GetElementValue("./contentPath");
            else
                DebugEx.LogError(string.Format("error code {0} : {1}", errorCode, errorInfo));

            return null;
        }

        private void _InitialPlace(RenderCommand prmCmd)
        {
            RenderPlace renderPlace;
            if (_GetEmptyRenderPlace(out renderPlace))
            {
                renderPlace.PlaceName = prmCmd.roomID;
                renderPlace.occurCmd = prmCmd;

                //  1. 获取场景XML配置文件路径
                WWWForm form = new WWWForm();
                form.AddField("userId", "6");// 临时测试接口用
                form.AddField("virtualMourningHall_id", prmCmd.roomID);

                DownloadManager.DownloadAsync(new RequestWWW(Config.GetUserSceneInfoPort, (prmWWW) =>
                {
                    string xmlFilePath = _GetUserScenePath(prmWWW);

                    // 2. 读取并构建场景
                    DownloadManager.DownloadAsync(new RequestWWW(xmlFilePath, (sceneWWW) =>
                    {
                        XMLDom _xmlDoc = new XMLDom();
                        _xmlDoc.LoadWithData(sceneWWW.text);

                        Assist.CentralMono().StartCoroutine(_sceneCreator.InstantiateScene(_xmlDoc.Root, (prmGoList) =>
                        {
                            //  3. 构建完毕后，将所有场景内对象平移到指定场地中去
                            foreach (GameObject go in prmGoList)
                            {
                                renderPlace.AddGameObject(go);
                            }

                            //  4. 创建摄像机并拍照
                            renderPlace.CreateCamera();

                            GameManagerStd.Instance.GetRenderCapture().AddCommand(prmCmd);
                        }, prmCmd.roomID));
                    }));
                }, form));
            }
        }

        public void TransferCommand(RenderCommand prmCmd)
        {
            // 1. 检查当前是否有同样的指令正在执行，如果没有进行下一步
            // 2. 异步进行下载操作
            // 3. 同步进行场景构建和拍照操作
            // 4. 异步进行上传操作
            if (!_IsCommandDuplicate(prmCmd))
            {
                _InitialPlace(prmCmd);
            }
            else
            {
                DebugEx.Log(string.Format("指令[{0}]已在执行中，因此不再执行该指令", prmCmd));
            }
        }
    }
}
