﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class RenderPlace
    {
        public GameObject placeRoot;
        public RenderCommand occurCmd;

        public string PlaceName
        {
            get { return placeRoot.name; }
            set { placeRoot.name = value; }
        }

        public RenderPlace(string prmPlaceName, GameObject prmGameObj)
        {
            placeRoot = prmGameObj;
            placeRoot.name = prmPlaceName;
        }

        public void AddGameObject(GameObject prmGameObj)
        {
            prmGameObj.transform.parent = placeRoot.transform;
            prmGameObj.transform.position += placeRoot.transform.position;
        }

        public void AddGameObject(GameObject prmGameObj, Vector3 prmTrans)
        {
            AddGameObject(prmGameObj);
            prmGameObj.transform.position += prmTrans;
        }

        public void AddGameObject(List<GameObject> prmGameObjs)
        {
            foreach (GameObject go in prmGameObjs)
            {
                AddGameObject(go);
            }
        }

        public GameObject CreateCamera()
        {
            //  Create Camera
            string cameraName = placeRoot.name + "_camera";
            GameObject cameraGameObj = GameObjectCreator.CreateCamera(cameraName);

            //  Move Camera to Place
            AddGameObject(cameraGameObj, new Vector3(40f, 7f, 30f));

            return cameraGameObj;
        }

        public void RemoveAll()
        {
            placeRoot.name = AppSetting.DEFAULT_ROOM_NAME;
            occurCmd = null;

            Assist.DestroyAllChildren(placeRoot);
        }
    }
}
