﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class GameObjectCreator
    {
        public static GameObject CreateCamera()
        {
            return CreateCamera(AppSetting.DEFAULT_CAMERA_NAME);
        }

        //-----------------------------------------------------------------------
        public static GameObject CreateCamera(string prmName)
        {
            return CreateCamera(prmName, 10.0f, 1000.0f, Screen.width/Screen.height, 60.0f);
        }

        //-----------------------------------------------------------------------
        public static GameObject CreateCamera(string prmName, float prmNear, float prmFar, float prmAspect, float prmFov)
        {
            GameObject retCameraGameObj = new GameObject(prmName);
            Camera camera = retCameraGameObj.AddComponent<Camera>();

            camera.nearClipPlane = prmNear;
            camera.farClipPlane = prmFar;
            //camera.aspect = prmAspect;
            camera.fieldOfView = prmFov;
            camera.depth = -1;

            return retCameraGameObj;
        }
    }
}