﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking.Match;
using VBGames.Utils.Logger;

namespace VBGames.PlayMaker.Actions
{
    public class RenderCapture
    {
        private readonly MapListFilter<string, RenderCommand> _mlCmdFilter = new MapListFilter<string, RenderCommand>();

        public RenderCapture()
        {
            //  摄像机的Id是RoomID_camera
            _mlCmdFilter.FuncPermitDelegate =
                (_this => _this.NextRoundCmds.Count < AppSetting.MAX_SHADER_TIMES_PER_FRAME);
            _mlCmdFilter.FuncCreateKeyDelegate = (command => command.roomID + "_camera");
        }

         //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            if (_mlCmdFilter.WaitCount() > 0)
                Assist.StartCoroutine(_MoveAndCapture);
        }

        //-----------------------------------------------------------------------
        public void AddCommand(RenderCommand cmd)
        {
            _mlCmdFilter.Add(cmd);
        }

        //-----------------------------------------------------------------------
        private Texture2D _CaptureScreenShot(Camera selectedCam, Rect rect)
        {
            var renderTexture = new RenderTexture((int)rect.width, (int)rect.height, 24);

            if (selectedCam != null)
            {
                selectedCam.targetTexture = renderTexture;
                selectedCam.Render();

                // active the renderTexture and load pixels
                RenderTexture.active = renderTexture;

                var screenShot = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.RGB24, false);
                screenShot.ReadPixels(rect, 0, 0);
                screenShot.Apply();

                //  resume the camera
                selectedCam.targetTexture = null;
                RenderTexture.active = null;
                Assist.DestroyObject(renderTexture);

                return screenShot;
            }
            return null;
        }

        //-----------------------------------------------------------------------
        private byte[] _ConvertTexToPic(Texture2D prmTex, string prmExtension)
        {
            byte[] outBytes = { };

            //  check which extension should be selected
            if (prmExtension == ".png")
                outBytes = prmTex.EncodeToPNG();
            else if (prmExtension == ".jpg")
                outBytes = prmTex.EncodeToJPG();
            else
                DebugEx.Log("Unsupported ScreenShot File Extension!");

            return outBytes;
        }

        //-----------------------------------------------------------------------
        private List<Texture2D> _CameraWork(String key, RenderCommand cmd)
        {
            List<Texture2D> cameraResult = new List<Texture2D>();

            Camera camera = GameObject.Find(key).GetComponent<Camera>();
            float yRotDelta = Config.CameraCaptureDegree;
             
            for (float yRot = 0f; yRot < 360f; yRot += yRotDelta)
            {
                //  拍摄照片并将其转化成byte[]
                Texture2D tex = _CaptureScreenShot(camera,
                    new Rect(Screen.width * 0f, Screen.height * 0f, Screen.width * 1f, Screen.height * 1f));
                //var bytes = _ConvertTexToPic(tex, Config.ScreenShotExtension);

                Assist.CentralMono().DestroyObject(tex);
                //  添加结果并旋转摄像机
                cameraResult.Add(tex);
                camera.transform.Rotate(.0f, yRotDelta, .0f);
            }

            return cameraResult;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _MoveAndCapture()
        {
            //  释放一批待拍照申请进入
            _mlCmdFilter.PermitIn();
            VBLogger.Debug("A shipment of render command has passed");

            //  key -> cameraId, value -> command
            _mlCmdFilter.Foreach((key, value) =>
            {
                //  摄像机完成拍摄任务
                List<Texture2D> cameraResult = _CameraWork(key, value);
                VBLogger.Debug(string.Format("Command [{0}] finished", value.ToString()));

                //  删除场景
                GameManagerStd.Instance.GetRenderSceneManager().FreeRenderPlace(value.roomID);
                VBLogger.Debug(string.Format("Command [{0}] scene delete", value.ToString()));

                //  上传拍摄结果
                VBLogger.Debug(string.Format("Command [{0}] render pictures are ready to upload", value.ToString()));
                Assist.CentralMono().StartCoroutine(_SaveScreenshot(cameraResult, value));
            });

            yield break;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _SaveScreenshot(List<Texture2D> prmBytesList, RenderCommand prmCmd)
        {
            //  构造Form上传拍摄结果并准备上传
            var form = new WWWForm();
            form.AddField("type", (int)UploadType.RenderResults);

            for (int idx = 0; idx < prmBytesList.Count; ++idx)
            {
                byte[] cvtPic = _ConvertTexToPic(prmBytesList[idx], Config.ScreenShotExtension);
                form.AddBinaryData(
                    string.Format("filestream_{0}",idx + 1), 
                    cvtPic, 
                    string.Format("{0}_{1}{2}",idx + 1, prmCmd.roomID, Config.ScreenShotExtension));

                Assist.CentralMono().DestroyObject(prmBytesList[idx]);

                //File.WriteAllBytes(string.Format("D:\\photos\\{0}_{1}{2}", idx + 1, prmCmd.roomID, Config.ScreenShotExtension), cvtPic);
            }

            //  上传文件
            Request uploadFile = new RequestWWW(Config.UploadFilePort, www =>
            {
                VBLogger.Log(string.Format("Scene [{0}] render pictures has uploaded to file server", prmCmd.roomID));

                var xmlDom = new XMLDom();
                xmlDom.LoadWithData(www.text);

                var errorCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                var errorInfo = xmlDom.GetElementValue("./errorInfo");

                //  将所有文件路径通过#符号串联起来
                List<String> filePaths = xmlDom.GetAllElementValue("./path/e");
                string joinedPath = string.Join("#", filePaths.ToArray());

                VBLogger.Debug(string.Format("Scene [{0}] filepath combined to [{1}]", prmCmd.roomID, joinedPath));

                if (errorCode == Config.InterfaceNoError)
                {
                    form = new WWWForm();
                    form.AddField("roomId", prmCmd.roomID);
                    form.AddField("postId", 0);
                    form.AddField("name", joinedPath);

                    //  Upload sreenshot info to server
                    Request uploadSrcInfo = new RequestWWW(Config.UploadScreenshotInfoPort, null, form);
                    DownloadManager.DownloadAsync(uploadSrcInfo);
                }
                else
                    DebugEx.Log(string.Format("errorCode {0} : {1}", errorCode, errorInfo));
            }, form);

            DownloadManager.DownloadAsync(uploadFile);
            yield break;
        }
    }
}
