﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    class StandaloneApp : SimpleApp
    {
        public override IEnumerator LateSetup()
        {
            Application.targetFrameRate = 60;
            Application.runInBackground = Config.RunInBackground;
            QualitySettings.vSyncCount = 0;

            //  Setup SceneManager
            GameManagerStd.Instance.Setup();

            yield break;
        }
    }
}
