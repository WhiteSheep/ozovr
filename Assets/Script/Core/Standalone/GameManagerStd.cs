﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ozovr.SceneObject;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    internal class GameManagerStd : MonoBehaviour, ISetup
    {
        private static GameManagerStd _instance;
        private RenderMachineSocketServer _rmSocketServer;
        private RenderSceneMgr _renderSceneMgr;
        private RenderCapture _renderCapture;

        private bool _isSetuped;

        protected GameManagerStd()
        {
            _rmSocketServer = new RenderMachineSocketServer();
            _renderSceneMgr = new RenderSceneMgr();
            _renderCapture = new RenderCapture();
        }

        //-----------------------------------------------------------------------
        public static GameManagerStd Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameManagerStd();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            if (_isSetuped)
            {
                _rmSocketServer.Update();
            }
            
        }

        //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            if (_isSetuped)
            {
                _renderCapture.LateUpdate();
            }

            // Refresh debug info  
            DebugEx.Update();
        }

        //-----------------------------------------------------------------------
        public void OnDestroy()
        {
            _rmSocketServer.OnDestroy();
        }

        //-----------------------------------------------------------------------
        public void Setup()
        {
            _rmSocketServer.Setup();
            _renderSceneMgr.Setup();
            
            //  Initialize SceneObjectTemplateMgr
            SceneObjectTemplateMgr.Instance.Init();

            _isSetuped = true;
        }

        //-----------------------------------------------------------------------
        public bool FinishedSetup()
        {
            return _isSetuped;
        }

        //-----------------------------------------------------------------------
        public RenderSceneMgr GetRenderSceneManager()
        {
            return _renderSceneMgr;
        }
        
        //-----------------------------------------------------------------------
        public RenderMachineSocketServer GetRMSocketServer()
        {
            return _rmSocketServer;
        }

        //-----------------------------------------------------------------------
        public RenderCapture GetRenderCapture()
        {
            return _renderCapture;
        }
    }
}