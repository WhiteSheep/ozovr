﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Ozovr.SceneObject;
using VBGames.PlayMaker.UI;

namespace VBGames.PlayMaker.Actions
{
    public class SceneCreator
    {
        private Action<List<GameObject>> _callbackFunc;
        private LoadingState _loadingState;
        private bool _isLoading;
        
        //-----------------------------------------------------------------------
        public bool IsDone
        {
            get { return !_isLoading; }
        }

        //-----------------------------------------------------------------------
        public LoadingState NowState
        {
            get { return _loadingState; }
        }

        //-----------------------------------------------------------------------
        /// 读取场景方法会将通过WWW下载并解析场景配置信息，然后生成对应的场景单位，同时该方法
        /// 接受一个回调函数，并将生成的单位列表作为参数调用回调函数
        //-----------------------------------------------------------------------
        public LoadingState LoadScene(string prmSceneRoute, Action<List<GameObject>> prmCallback = null)
        {
            if (!_isLoading)
            {
                _isLoading = true;
                _loadingState = LoadingState.CreateSingle("Loading scene ... Please wait for a moment ...");
                _callbackFunc = prmCallback;

                DownloadManager.DownloadAsync(new RequestWWW(prmSceneRoute, _AfterLoaded));
            }
            return _loadingState;
        }

        //-----------------------------------------------------------------------
        private void _AfterLoaded(WWW prmWWW)
        {
            //  Load scene configuration
            XMLDom _xmlDoc = new XMLDom();
            _xmlDoc.LoadWithData(prmWWW.text);

            //  Clear world for new creation
            World.Clear();
            Assist.CentralMono().StartCoroutine(InstantiateScene(_xmlDoc.Root, _callbackFunc, User.virtualMourningHallId.ToString()));
        }

        //-----------------------------------------------------------------------
        /// 将场景文件实例化为3D场景，需要场景ID用以索引牌位文字和照片
        //-----------------------------------------------------------------------
        public IEnumerator InstantiateScene(XElement prmSceneRoot, Action<List<GameObject>> prmCallback, string prmRoomId = "")
        {
            List<GameObject> goList = new List<GameObject>();

            foreach (XElement obj in prmSceneRoot.Elements())
            {
                string objName = obj.Attribute("UnitTemplateName").Value;

                SceneObjectTemplate unitTemplate;
                unitTemplate = SceneObjectTemplateMgr.Instance.GetTemplateById(objName);
                GameObject copy = unitTemplate.CreateSceneObject();

                /// Instantiate basic info
                XElement transform = obj.Element("Transform");

                //  get position info
                XElement position = transform.Element("Position");
                Vector3 pos = Vector3.zero;
                pos.x = float.Parse(position.Element("x").Value);
                pos.y = float.Parse(position.Element("y").Value);
                pos.z = float.Parse(position.Element("z").Value);

                //  get rotation info
                XElement rotation = transform.Element("Rotation");
                Vector3 rot = Vector3.zero;
                rot.x = float.Parse(rotation.Element("x").Value);
                rot.y = float.Parse(rotation.Element("y").Value);
                rot.z = float.Parse(rotation.Element("z").Value);

                copy.transform.position = pos;
                copy.transform.rotation = Quaternion.Euler(rot);

                //  Init PictureFrame
                Assist.SetActive(copy, true);
                if (copy.GetComponent<SceneObjectTexCtrl>() != null)
                {
                    var texCtrlScript = copy.GetComponent<SceneObjectTexCtrl>();
                    XElement pictureFrame = obj.Element("PictureFrame");

                    texCtrlScript.locked = Convert.ToBoolean(pictureFrame.Element("Locked").Value);
                    texCtrlScript.pathType = (TexturePathType) Enum.Parse(typeof (TexturePathType),
                        pictureFrame.Element("FrameTag").Value);
                    texCtrlScript.texturePath = pictureFrame.Element("TexturePath").Value;
                    texCtrlScript.roomId = prmRoomId;

                    yield return Assist.CentralMono().StartCoroutine(texCtrlScript.UpdateTexture());
                }

                goList.Add(copy);   
            }

            if (prmCallback != null)
                prmCallback(goList);

            _isLoading = false;

            yield break;
        }


        //-----------------------------------------------------------------------
        /// 异步方法，将场景保存并上传到文件服务器
        //-----------------------------------------------------------------------
        public IEnumerator SaveSceneToXml(string prmFileName)
        {
            if (_isLoading)
                yield break;

            _isLoading = true;

            //  Get all export gameobjects
            GameObject[] allExportObjects = GameObject.FindGameObjectsWithTag(Tag.ExportGameObject);
            DebugEx.Log(string.Format("{0} GameObjects will be exported from scene", allExportObjects.Length));

            //  Create XDocument
            XDocument xmlDoc = new XDocument();
            XElement root = new XElement("Scene");
            xmlDoc.Add(root);

            foreach (GameObject obj in allExportObjects)
            {
                //  Create gameObject
                XElement gameObj = new XElement("GameObject");
                Unit unit = obj.GetComponent<Unit>();
                root.Add(gameObj);

                //  Create basic informations
                gameObj.SetAttributeValue("UnitTemplateName", obj.name);

                //  Create gameObject transform
                XElement transform = new XElement("Transform");
                gameObj.Add(transform);

                //  create transform - position
                XElement position = new XElement("Position");
                XElement positionX = new XElement("x", obj.transform.position.x.ToString());
                XElement positionY = new XElement("y", obj.transform.position.y.ToString());
                XElement positionZ = new XElement("z", obj.transform.position.z.ToString());
                position.Add(positionX, positionY, positionZ);
                transform.Add(position);

                //  create transform - rotation
                XElement rotation = new XElement("Rotation");
                XElement rotationX = new XElement("x", obj.transform.rotation.eulerAngles.x.ToString());
                XElement rotationY = new XElement("y", obj.transform.rotation.eulerAngles.y.ToString());
                XElement rotationZ = new XElement("z", obj.transform.rotation.eulerAngles.z.ToString());
                rotation.Add(rotationX, rotationY, rotationZ);
                transform.Add(rotation);

                //  Create transform - scale
                XElement scale = new XElement("Scale");
                XElement scaleX = new XElement("x", obj.transform.localScale.x.ToString());
                XElement scaleY = new XElement("y", obj.transform.localScale.y.ToString());
                XElement scaleZ = new XElement("z", obj.transform.localScale.z.ToString());
                scale.Add(scaleX, scaleY, scaleZ);
                transform.Add(scale);

                //  Create PictureFrame
                if (obj.GetComponent<SceneObjectDataCtrl>().srcTemplate.pictureFrameMode)
                {
                    var pf = obj.GetComponent<SceneObjectTexCtrl>();
                    XElement pictureFrame = new XElement("PictureFrame");
                    XElement locked = new XElement("Locked", pf.locked.ToString());
                    XElement frameTag = new XElement("FrameTag", pf.pathType.ToString());
                    XElement texturePath = new XElement("TexturePath", pf.texturePath);
                    pictureFrame.Add(locked, frameTag, texturePath);

                    gameObj.Add(pictureFrame);
                }
            }

            //  Create form
            WWWForm form = new WWWForm();
            UTF8Encoding encoding = new UTF8Encoding();
            form.AddBinaryData("file", encoding.GetBytes(xmlDoc.ToString()), prmFileName + ".xml");
            form.AddField("type", (int) UploadType.Picture);

            DownloadManager.DownloadAsync(new RequestWWW(Config.UploadFilePort, _UploadFileCallback, form));
        }

        //-----------------------------------------------------------------------
        private void _UploadFileCallback(WWW prmWWW)
        {
            XMLDom xmlDom = new XMLDom();
            xmlDom.LoadWithData(prmWWW.text);

            int errorCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
            string errorInfo = xmlDom.GetElementValue("./errorInfo");
            if (errorCode == Config.InterfaceNoError)
            {
                WWWForm form = new WWWForm();
                form.AddField("userId", User.Id);
                form.AddField("virtualMourningHall_id", User.virtualMourningHallId);
                form.AddField("contentPath", xmlDom.GetElementValue("./path"));

                DownloadManager.DownloadAsync(new RequestWWW(Config.UploadUserSceneInfoPort, (www) =>
                {
                    errorCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                    errorInfo = xmlDom.GetElementValue("./errorInfo");

                    //  弹出通知并关闭读取Mask
                    if (errorCode != Config.InterfaceNoError)
                        UIManager.Instance.OpenConfirmWindow("通知", string.Format("场景文件存储失败！(无法向数据库上交场景信息资料,{0} : {1})", errorCode, errorInfo));
                    else
                        UIManager.Instance.OpenConfirmWindow("通知", string.Format("场景保存成功！"));
                    UIManager.Instance.SetDataLoadingWindowState(false);
                    
                    _isLoading = false;
                    _loadingState = null;
                }, form));
            }
            else
            {
                UIManager.Instance.OpenConfirmWindow("通知",
                    string.Format("场景文件存储失败！(无法上传场景文件至文件服务器,{0} : {1})", errorCode, errorInfo));

                UIManager.Instance.SetDataLoadingWindowState(false);
            }
        }
    }
}