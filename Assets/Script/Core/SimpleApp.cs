﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class SimpleApp : MonoBehaviour
    {
        public IEnumerator Start()
        {
            //  Setup basic module
            yield return StartCoroutine(Setup());

            //  Setup concrete module
            StartCoroutine(LateSetup());
        }

        public IEnumerator Setup()
        {
            //  Setup application basic setting
            AppSetting.Setup();

            //  Setup dynamic config setting
            Config.Instance.Setup();
            while (!Config.Instance.FinishedSetup())
                yield return null;

            //  Setup resource
            StartCoroutine(Resource.Instance.Setup());
            while (!Resource.Instance.FinishedSetup())
                yield return null;

            DebugEx.Log("Framework setup successful");
        }

        public virtual IEnumerator LateSetup()
        {
            yield break;
        }
    }
}
