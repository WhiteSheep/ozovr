﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;

namespace VBGames.PlayMaker.Actions
{
    public class RenderMachine
    {
        private RenderMachineSocketServer _rms;
        public RenderMachineStat status;

        public RenderMachine(RenderMachineSocketServer rms)
        {
            _rms = rms;
            status = RenderMachineStat.OK;
        }

        //-----------------------------------------------------------------------
        public void handleMessage(Socket myClientSocket, string message)
        {
            try
            {
                // 分解消息
                string[] msgs = message.Split(RequestHeader.SPLIT);

                // 判断消息长度是否合法
                if (msgs.Length < 2)
                    throw new ArgumentException(string.Format("message '{0}' length length too short", message));

                if (message.Contains(RequestHeader.KEEP_ALIVE))
                {
                    keepAlive(myClientSocket, msgs[0]);
                } 
                else if (message.Contains(RequestHeader.CHECK_STATUS))
                {
                    checkStatus(myClientSocket, msgs[0]);
                }
                else if (message.Contains(RequestHeader.RENDER_MACHINE))
                {
                    renderRoom(myClientSocket, msgs[1]);
                }
            }
            catch (Exception e)
            {
                Debug.Log(string.Format("Error while handling message '{0}', e = {1}", message, e.Message));
            }
        }

        //-----------------------------------------------------------------------

        public void renderRoom(Socket myClientSocket, string roomid)
        {
            // 完全没有实现的操作，需要补充
            //_rms.socketStr2CmdStr("0;0");
        }

        //-----------------------------------------------------------------------

        public void keepAlive(Socket myClientSocket, string messageId)
        {
            int result = SendData(myClientSocket, messageId + "|KeepAlive", 10);   
            Debug.Log("发送客户端消息" + myClientSocket.RemoteEndPoint.ToString() + messageId);
        }

        //-----------------------------------------------------------------------

        public void checkStatus(Socket myClientSocket, string messageId)
        {
            //check status in 
            int result = SendData(myClientSocket, messageId + '|' + (int)status, 10);
            Debug.Log("发送客户端消息" + myClientSocket.RemoteEndPoint.ToString() + messageId + '|' + (int)status);
        }


        //-----------------------------------------------------------------------
        /// <summary>  
        /// 向远程主机发送数据  
        /// </summary>  
        /// <param name="socket">连接到远程主机的socket</param>  
        /// <param name="buffer">待发送数据</param>  
        /// <param name="outTime">发送超时时长，单位是秒(为-1时，将一直等待直到有数据需要发送)</param>  
        /// <returns>0:发送成功；-1:超时；-2:出现错误；-3:出现异常</returns>  
        public int SendData(Socket socket, byte[] buffer, int outTime)
        {
            if (buffer == null || buffer.Length == 0)
            {
                throw new ArgumentException("参数buffer为null ,或者长度为 0");
            }

            int flag = 0;
            try
            {
                int left = buffer.Length;
                int sndLen = 0;
                int hasSend = 0;

                while (true)
                {
                    if ((socket.Poll(outTime * 1000, SelectMode.SelectWrite) == true))
                    {
                        // 收集了足够多的传出数据后开始发送  
                        sndLen = socket.Send(buffer, hasSend, left, SocketFlags.None);
                        left -= sndLen;
                        hasSend += sndLen;

                        // 数据已经全部发送  
                        if (left == 0)
                        {
                            flag = 0;
                            break;
                        }
                        else
                        {
                            // 数据部分已经被发送  
                            if (sndLen > 0)
                            {
                                continue;
                            }
                            else // 发送数据发生错误  
                            {
                                flag = -2;
                                break;
                            }
                        }
                    }
                    else // 超时退出  
                    {
                        flag = -1;
                        break;
                    }
                }
            }
            catch (SocketException)
            {
                //Log  
                flag = -3;
            }
            return flag;
        }

        //-----------------------------------------------------------------------

        /// <summary>  
        /// 向远程主机发送数据  
        /// </summary>  
        /// <param name="socket">连接到远程主机的socket</param>  
        /// <param name="buffer">待发送的字符串</param>  
        /// <param name="outTime">发送数据的超时时间，单位是秒(为-1时，将一直等待直到有数据需要发送)</param>  
        /// <returns>0:发送数据成功；-1:超时；-2:错误；-3:异常</returns>  
        public int SendData(Socket socket, string buffer, int outTime)
        {
            // SendData中有检查
            /*
            if (buffer == null || buffer.Length == 0)
            {
                throw new System.ArgumentException("buffer为null或则长度为0.");
            }*/

            return SendData(socket, System.Text.Encoding.Default.GetBytes(buffer), outTime);
        }
    }
}
