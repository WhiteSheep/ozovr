﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace VBGames.PlayMaker.Actions
{
    public class SocketClient : MonoBehaviour
    {

        private float elapsedTime = 0.0f;
        private IPAddress ipAddr;
        private IPEndPoint ipEP;
        private Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private bool once = true;
        // Use this for initialization
        private void Start()
        {

        }

        private void ConnectServer()
        {

            clientSocket.Connect(ipEP);
            string output = "capture";
            byte[] concent = Encoding.UTF8.GetBytes(output);
            clientSocket.Send(concent);

            byte[] response = new byte[1024];
            int bytesRead = clientSocket.Receive(response);
            string responseStr = Encoding.UTF8.GetString(response, 0, bytesRead);

            DebugEx.Log("Client request : " + responseStr);

            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Disconnect(true);
            clientSocket.Close();
        }

        // Update is called once per frame
        private void Update()
        {

        }

        private void LateUpdate()
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > 3.0f && once)
            {
                once = false;
                ipAddr = IPAddress.Parse("127.0.0.1");
                ipEP = new IPEndPoint(ipAddr, 8885);
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                ConnectServer();
            }

        }
    }
}