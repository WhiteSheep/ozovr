﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

namespace VBGames.PlayMaker.Actions
{
    public enum MenuType
    {
        System_Menu
    }

    public class UIController : MonoBehaviour
    {
        private static UIController _instance = null;
        private bool _ifOpenMenu = false;

        [SerializeField] private GameObject _systemMenu;
        [SerializeField] private GameObject _loadingMenu;
        [SerializeField] private GameObject _screenMenu;
        [SerializeField] private GameObject _consoleMenu;
        [SerializeField] private GameObject _startMenu;
        [SerializeField] private UITextList _consoleMessageBox;
        [SerializeField] private UILabel _loadingMsg;

        protected UIController()
        {
        }

        public static UIController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UIController();
                return _instance;
            }
        }

        public UILabel LoadingMsg
        {
            get { return _loadingMsg; }
        }

        public bool IfOpenMenu()
        {
            return _ifOpenMenu;
        }

        private void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }

        public void SwitchMenu(MenuType prmMenuType, bool prmSwitch = true)
        {
            switch (prmMenuType)
            {
                case MenuType.System_Menu:
                    _systemMenu.SetActive(!_systemMenu.activeSelf);
                    break;
                default:
                    break;
            }
        }

        public void OpenSystemMenu()
        {
            _systemMenu.SetActive(true);
            NGUITools.SetActive(_screenMenu, false);

            _ifOpenMenu = true;
        }

        public void CloseSystemMenu()
        {
            _systemMenu.SetActive(false);
            NGUITools.SetActive(_screenMenu, true);

            _ifOpenMenu = false;
        }

        public void SetLoadingActive(bool prmActive)
        {
            NGUITools.SetActive(_loadingMenu, prmActive);
        }

        public void SetConsoleActive(bool prmActive)
        {
            NGUITools.SetActive(_consoleMenu, prmActive);
        }

        public void SetStartMenuActive(bool prmActive)
        {
            NGUITools.SetActive(_startMenu, prmActive);
        }

        public void PrintMsgToConsole(string prmMsg)
        {
            if (_consoleMessageBox)
                _consoleMessageBox.Add(prmMsg);
        }

        public static void SetLoadingMsg(string prmMsg = "")
        {
            if (string.IsNullOrEmpty(prmMsg))
                prmMsg = "Please Waiting ... ...";

            //Instance.LoadingMsg.text = prmMsg;
        }

        public void ShowComments(GameObject prmPanel)
        {
            GameObject titleBar = Assist.GetChildGameObjectByName(prmPanel, "Title");
            GameObject subPanel = Assist.GetChildGameObjectByName(prmPanel, "SubPanel");

            titleBar.GetComponent<UILabel>().text = "亲友留言";
            Assist.DestroyAllChildren(subPanel);

            WWWForm form = new WWWForm();
            form.AddField("page", 1);
            form.AddField("limit", 100);
            form.AddField("id", User.virtualMourningHallId);

            DownloadManager.DownloadAsync(new RequestWWW(Config.GetCommentsPort, (www) =>
            {
                XMLDom xmlDoc = new XMLDom();
                xmlDoc.LoadWithData(www.text);

                int errorCode = Convert.ToInt32(xmlDoc.GetElement("./errorCode").Value);
                string errorInfo = xmlDoc.GetElement("./errorInfo").Value;

                if (errorCode != Config.InterfaceNoError)
                {
                    DebugEx.Log(DebugEx.CreateErrMsg(errorCode, errorInfo));
                    return;
                }

                //  Create table 
                GameObject tableGo = NGUITools.AddChild(subPanel);
                tableGo.name = "Table - Contents";

                UIAnchor anchor = tableGo.AddComponent<UIAnchor>();
                anchor.container = tableGo.transform.parent.gameObject;
                anchor.side = UIAnchor.Side.TopLeft;

                UITable table = tableGo.AddComponent<UITable>();
                table.columns = 1;
                table.direction = UITable.Direction.Down;
                table.padding = new Vector2(0f, 10f);

                //  Create content
                IEnumerable<XElement> allComments = xmlDoc.GetElements("./message/e");
                foreach (XElement e in allComments)
                {
                    GameObject content = NGUITools.AddChild(tableGo);
                    content.name = "Content - Item";

                    GameObject text = NGUITools.AddChild(content);
                    text.name = "Label";

                    //  Set comment text style
                    UILabel label = text.AddComponent<UILabel>();
                    label.trueTypeFont = (Font) Resources.Load("msyh");
                    label.width = 700;
                    label.fontSize = 25;
                    label.alignment = NGUIText.Alignment.Left;
                    label.overflowMethod = UILabel.Overflow.ResizeHeight;
                    label.effectStyle = UILabel.Effect.Shadow;
                    label.effectDistance = new Vector2(3f, 3f);

                    //  Get sendTime
                    DateTime sendTime =
                        MathfEx.ConvertIntDataTime(Convert.ToDouble(e.Element("sendTime").Element("time").Value));

                    label.text = string.Format("[ffff00]{0}({1})[-]:\n    {2}", e.Element("trueName").Value,
                        sendTime.ToString(AppSetting.DEFAULT_DATE_FORMAT)
                        , e.Element("content").Value);
                }

                //  Refresh table position
                table.Reposition();

            }, form));
        }

        public void ShowCultureIntroduction(GameObject prmPanel)
        {
            GameObject titleBar = Assist.GetChildGameObjectByName(prmPanel, "Title");
            GameObject subPanel = Assist.GetChildGameObjectByName(prmPanel, "SubPanel");

            titleBar.GetComponent<UILabel>().text = "文化介绍";
            Assist.DestroyAllChildren(subPanel);

            //  Create table 
            GameObject tableGo = NGUITools.AddChild(subPanel);
            tableGo.name = "Table - Contents";

            UIAnchor anchor = tableGo.AddComponent<UIAnchor>();
            anchor.container = tableGo.transform.parent.gameObject;
            anchor.side = UIAnchor.Side.TopLeft;

            UITable table = tableGo.AddComponent<UITable>();
            table.columns = 1;
            table.direction = UITable.Direction.Down;
            table.cellAlignment = UIWidget.Pivot.Center;
            table.padding = new Vector2(0f, 5f);

            //  Create content
            string[] infos =
            {
                "[FFFF00]中国殡葬文化介绍[-]",
                "[FFFF00]1、土葬[-]",
                "土葬是我国产生最早、流传时间最长、使用范围最广、涉及民族最多、最为普遍的一种丧葬民俗",
                "[FFFF00]2、树葬[-]",
                "树葬是一种非常古老的葬法，它的主要形式是把死者置于深山或野外的大树上，任其风化；后来，有的稍作改进的方式是将死者陈放于专门制做的棚架上",
                "[FFFF00]3、天葬[-]",
                "天葬是蒙古、藏族等少数民族的一种传统丧葬方式，人死后把尸体拿到指定的地点让鹰（或者其他的鸟类、兽类等）吞食，认为可以带到天堂" /*,
            "[FFFF00]4、崖葬[-]", 
            "崖葬又名悬棺葬，是我国古代广居南方的濮越民族的一种特殊葬俗，被认为是世界大化史上的一大奇迹。其葬法是利用天然岩缝或人工木桩把棺木悬置在嘴壁之上，或者将棺木放在天然或人工凿成的岩洞之中，悬棺葬的葬地都是选在面临江河的绝壁高岩上，其葬具多为船棺，长2-3米，宽约半米多，形体似一只船，分为头、尾和仑三部分，头尾翘起，仓为棺枢，安放尸体",
            "[FFFF00]5、现代葬俗[-]", 
            "解放以后，国家为了节约土地，杜绝疾病蔓延，规定除部分少数民族外，其余一律采用火葬。人死后，被送往殡仪馆停放，尸体赤裸冰冻存放。为了延长保存时间，殡葬人员会在死者脚板底扎一个洞使死者的血全部放走。",
            "尸体送到殡仪馆后，一般三天内举殡，举殡分遗体告别仪式和追悼会两部分。遗体告别仪式时设置“灵堂”，灵堂四周排满花圈，花圈上挂满亲朋戚友的挽联。告别仪式开始先播放一段哀乐，悼念者默哀三分钟，之后向死者三鞠躬，最后绕遗体一周，瞻仰死者遗容。长子手捧死者遗照领悼念者去小礼堂开追悼会。追悼会主要是表彰死者生前功迹，因过于形式及劳民伤财，现已基本取消。遗照一般不用彩色，且用“炭烧瓷相”，以示永久存。"*/
            };

            for (int idx = 0; idx < infos.Length; ++ idx)
            {
                GameObject content = NGUITools.AddChild(tableGo);
                content.name = "Content - Item";

                GameObject labelGo = NGUITools.AddChild(content);
                UILabel label = labelGo.AddComponent<UILabel>();

                label.name = "Label";
                label.trueTypeFont = (Font) Resources.Load("msyh");
                label.width = 930;
                label.fontSize = 20;
                label.overflowMethod = UILabel.Overflow.ResizeHeight;
                label.effectStyle = UILabel.Effect.Shadow;
                label.effectDistance = new Vector2(3f, 3f);

                if (idx == 0)
                    label.alignment = NGUIText.Alignment.Center;
                else
                    label.alignment = NGUIText.Alignment.Left;
                label.text = infos[idx];
            }
            table.Reposition();
        }

        public void ShowOrganizationServerWindow(GameObject prmPanel)
        {
            GameObject titleBar = Assist.GetChildGameObjectByName(prmPanel, "Title");
            GameObject subPanel = Assist.GetChildGameObjectByName(prmPanel, "SubPanel");

            titleBar.GetComponent<UILabel>().text = "政策介绍";
            Assist.DestroyAllChildren(subPanel);
        }

        public void ShowUnitModifyWindow(GameObject prmPanel)
        {
            GameObject titleBar = Assist.GetChildGameObjectByName(prmPanel, "Title");
            GameObject subPanel = Assist.GetChildGameObjectByName(prmPanel, "SubPanel");

            titleBar.GetComponent<UILabel>().text = "场景布置";
            Assist.DestroyAllChildren(subPanel);
        }
    }
}