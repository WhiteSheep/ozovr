﻿using UnityEngine;
using System.Collections;

namespace OzovrAssets.MiddleWares.Characters.FirstPerson
{
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private float _walkSpeed;
        [SerializeField] private MouseLook _mouseLook;
        [SerializeField] private MouseLookMode _mouseLookMode = MouseLookMode.NormalMode;

        private Camera _camera;

        // Use this for initialization
        void Start()
        {
            _camera = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}