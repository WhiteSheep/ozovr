﻿using System;
using System.Collections;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class ScreenCapture
    {
        private readonly MapListFilter<string, Command> _mlCmdFilter = new MapListFilter<string, Command>();

        public ScreenCapture()
        {
            _mlCmdFilter.FuncPermitDelegate =
                (_this => _this.NextRoundCmds.Count < AppSetting.MAX_SHADER_TIMES_PER_FRAME);
            _mlCmdFilter.FuncCreateKeyDelegate = (command => command.sUserID + "_" + command.sRoomID);
        }

        //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            if (_mlCmdFilter.WaitCount() > 0)
                Assist.StartCoroutine(_MoveAndCapture);
        }

        //-----------------------------------------------------------------------
        public void AddCommand(Command cmd)
        {
            _mlCmdFilter.Add(cmd);
        }

        //-----------------------------------------------------------------------
        private void _MoveCamera(String key, Command cmd)
        {
            var camera = GameObject.Find(key);
            try
            {
                if (AppSetting.Command.Rotate == cmd.cType)
                {
                    var xAngle = float.Parse(cmd.alParams[0]);
                    var yAngle = float.Parse(cmd.alParams[1]);
                    camera.transform.Rotate(xAngle, yAngle, .0f);
                }
                else if (AppSetting.Command.Move == cmd.cType)
                {
                }
            }
            catch (Exception e)
            {
                DebugEx.Log(e.ToString());
            }
        }

        //-----------------------------------------------------------------------
        private IEnumerator _MoveAndCapture()
        {
            _mlCmdFilter.PermitIn();
            _mlCmdFilter.Foreach((key, value) =>
            {
                _MoveCamera(key, value);
                var tex = _CaptureScreenShot(key,
                    new Rect(Screen.width*0f, Screen.height*0f, Screen.width*1f, Screen.height*1f));

                var bytes = _ConvertTexToPic(tex, Config.ScreenShotExtension);
                Assist.CentralMono().StartCoroutine(_SaveScreenshot(bytes, key, value));
            });

            yield break;
        }

        //-----------------------------------------------------------------------
        private Texture2D _CaptureScreenShot(string cameraName, Rect rect)
        {
            Camera selectedCam = null;

            var renderTexture = new RenderTexture((int) rect.width, (int) rect.height, 24);
            foreach (var camera in Camera.allCameras)
            {
                if (camera.name.Equals(cameraName))
                {
                    selectedCam = camera;
                    DebugEx.Log("camera found!");
                    break;
                }
            }

            if (selectedCam != null)
            {
                selectedCam.targetTexture = renderTexture;
                selectedCam.Render();

                // active the renderTexture and load pixels
                RenderTexture.active = renderTexture;

                var screenShot = new Texture2D((int) rect.width, (int) rect.height, TextureFormat.RGB24, false);
                screenShot.ReadPixels(rect, 0, 0);
                screenShot.Apply();

                //  resume the camera
                selectedCam.targetTexture = null;
                RenderTexture.active = null;
                Assist.DestroyObject(renderTexture);

                return screenShot;
            }
            return null;
        }

        //-----------------------------------------------------------------------
        private byte[] _ConvertTexToPic(Texture2D prmTex, string prmExtension)
        {
            byte[] outBytes = {};

            //  check which extension should be selected
            if (prmExtension == ".png")
                outBytes = prmTex.EncodeToPNG();
            else if (prmExtension == ".jpg")
                outBytes = prmTex.EncodeToJPG();
            else
                DebugEx.Log("Unsupported ScreenShot File Extension!");

            return outBytes;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _SaveScreenshot(byte[] prmBytes, string prmFilename, Command prmCmd)
        {
            var form = new WWWForm();
            form.AddBinaryData("file", prmBytes, prmFilename + Config.ScreenShotExtension);
            form.AddField("type", (int) UploadType.Picture);

            //  Upload screenshot file
            Request uploadFile = new RequestWWW(Config.UploadFilePort, www =>
            {
                var xmlDom = new XMLDom();
                xmlDom.LoadWithData(www.text);

                var errorCode = Convert.ToInt32(xmlDom.GetElementValue("./errorCode"));
                var errorInfo = xmlDom.GetElementValue("./errorInfo");

                if (errorCode == Config.InterfaceNoError)
                {
                    form = new WWWForm();
                    form.AddField("userId", prmCmd.sUserID);
                    form.AddField("virtualMourningHall_id", prmCmd.sRoomID);
                    form.AddField("imagePath", xmlDom.GetElementValue("./path"));

                    //  Upload sreenshot info to server
                    Request uploadSrcInfo = new RequestWWW(Config.UploadScreenshotInfoPort, null, form);
                    DownloadManager.DownloadAsync(uploadSrcInfo);
                }
                else
                    DebugEx.Log(string.Format("errorCode {0} : {1}", errorCode, errorInfo));
            }, form);
            DownloadManager.DownloadAsync(uploadFile);

            yield break;
        }
    }
}