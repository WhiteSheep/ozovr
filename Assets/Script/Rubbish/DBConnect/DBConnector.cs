﻿using System;
using System.Collections.Generic;
//using MySql.Data.MySqlClient;
using System.Threading;

namespace VBGames.PlayMaker.Actions
{
    public class DBConfig
    {
        //  save connection between sql and database
        private string _sUsername;

        public string Username
        {
            get { return _sUsername; }
        }

        private string _sPassword;

        public string Password
        {
            get { return _sPassword; }
        }

        private string _sAddress;

        public string Address
        {
            get { return _sAddress; }
        }

        private struct SqlCmd
        {
            public string sSql;
            public string sDatabase;

            public SqlCmd(string prmSql, string prmDatabase)
            {
                sSql = prmSql;
                sDatabase = prmDatabase;
            }
        }

        private Dictionary<string, SqlCmd> _dicSqlCmds = new Dictionary<string, SqlCmd>();
        private List<string> _dicDatabase = new List<string>();

        public List<string> Databases
        {
            get { return _dicDatabase; }
        }

        public DBConfig(string prmUsername, string prmPassword, string prmAddress)
        {
            _sUsername = prmUsername;
            _sPassword = prmPassword;
            _sAddress = prmAddress;
        }

        public void AddDatabase(string prmDatabaseName)
        {
            _dicDatabase.Add(prmDatabaseName);
        }

        public void AddSqlCmd(string prmSqlName, string prmSqlCmd, string prmDatabaseName)
        {
            _dicSqlCmds.Add(prmSqlName, new SqlCmd(prmSqlCmd, prmDatabaseName));
        }

        public bool TryGetSql(string prmSqlName, out string prmSqlCmd, out string prmDatabaseName)
        {
            SqlCmd cmd;

            //  check if sql exist
            bool ret = _dicSqlCmds.TryGetValue(prmSqlName, out cmd);

            if (ret)
            {
                prmSqlCmd = cmd.sSql;
                prmDatabaseName = cmd.sDatabase;
            }
            else
            {
                prmDatabaseName = prmSqlCmd = "";
            }

            return ret;
        }
    }
}

/*
public class DBConnector {

    static DBConnector _instance = null;
    
    public static DBConnector Instance
    {
        get 
        {
            if (_instance == null)
                _instance = new DBConnector();

            return _instance;
        }
    }

    protected DBConnector() 
    {
        //  initial
        dMysqlConnections = new Dictionary<string, MySqlConn>();

        //  create mysql connections
        foreach(string dbName in Config.MysqlConfig.Databases)
        {
            MySqlConn connection = new MySqlConn(
                Config.MonitorIP, 
                Config.MysqlUsername, 
                Config.MysqlPassword, 
                dbName
            );

            DebugEx.Log(string.Format("IP:{0} username:{1} password:{2} dbName:{3}"
                , Config.MonitorIP
                , Config.MysqlUsername,
                Config.MysqlPassword,
                dbName));

            connection.OpenConnection();
            DebugEx.Log("open database - " + dbName + " - " + connection.IsConnecting());

            dMysqlConnections.Add(dbName, connection);
        }
    }

    Dictionary<string, MySqlConn> dMysqlConnections;
	
    public MySqlConn GetMysqlConn(string dbName)
    {
        MySqlConn connection;
        if (dMysqlConnections.TryGetValue(dbName, out connection))
            return connection;
        
        return null;
    }
}


public class MySqlConn
{
    public MySqlConn(string ip, string username, string password, string database)
    {
        sIP = ip;
        sUsername = username;
        sPassword = password;
        sDatabase = database;
    }

    
    public void OpenConnection()
    {
        MySqlConnectionStringBuilder connBuilder = new MySqlConnectionStringBuilder();
        connBuilder.Server = sIP;
        connBuilder.UserID = sUsername;
        connBuilder.Password = sPassword;
        connBuilder.Database = sDatabase;

        DebugEx.Log(connBuilder.GetConnectionString(true));
        _mysqlConnection = new MySqlConnection(connBuilder.GetConnectionString(true));
        try
        {
            _mysqlConnection.Open();
        }
        catch(Exception e)
        {
            DebugEx.Log(e.Message);
            _mysqlConnection = null;
        }
    }

    public void CloseConnection()
    {
        try
        {
            _mysqlConnection.Close();
            _mysqlConnection = null;
        }
        catch(Exception e)
        {
            DebugEx.Log(e.Message);
        }
    }

    public bool IsConnecting()
    {
        return (_mysqlConnection != null);
    }

    private MySqlCommand _CreateCommand(string command, Dictionary<string, string> cmdParams)
    {
        MySqlCommand retCmd = new MySqlCommand(command, _mysqlConnection);
        retCmd.Prepare();

        foreach (var item in cmdParams)
            retCmd.Parameters.AddWithValue(item.Key, item.Value);

        return retCmd;
    }

    public MySqlDataReader Query(string queryCommand, Dictionary<string, string> cmdParams)
    {
        try
        {
            MySqlCommand cmd = _CreateCommand(queryCommand, cmdParams);
            MySqlDataReader reader = cmd.ExecuteReader();

            return reader;
        }
        catch (Exception e)
        {
            DebugEx.Log(e);
        }

        return null;
    }

    public int Update(string queryCommand, Dictionary<string, string> cmdParams)
    {
        try
        {
            MySqlCommand cmd = _CreateCommand(queryCommand, cmdParams);
            ThreadPool.QueueUserWorkItem(_UpdateThread, cmd);
        } 
        catch (Exception e)
        {
            DebugEx.Log(e.Message);
            return 1;
        }

        return 0;
    }

    private static void _UpdateThread(object mysqlCmd)
    {
        MySqlCommand cmd = mysqlCmd as MySqlCommand;
        cmd.ExecuteNonQuery();
    }

    private MySqlConnection _mysqlConnection;
    public string sIP;
    public string sUsername;
    public string sPassword;
    public string sDatabase; 
}
*/