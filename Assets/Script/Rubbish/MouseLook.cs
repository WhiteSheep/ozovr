﻿using UnityEngine;
using System.Collections;

namespace OzovrAssets.MiddleWares.Characters.FirstPerson
{
    public enum MouseLookMode
    {
        DragMode, NormalMode
    }

    public class MouseLook
    {
        public float deltaX = 2f;
        public float deltaY = 2f;

        private MouseLookMode _lookMode;
        private Quaternion _characterTargetRot;
        private Quaternion _cameraTargetRot;
        private Quaternion _originCameraTargetRot;
        private Quaternion _origineCharaterTargetRot;

        public void Init(Transform prmCharacter, Transform prmCamera, MouseLookMode prmLookMode)
        {
            _characterTargetRot = prmCharacter.localRotation;
            _cameraTargetRot = prmCamera.localRotation;
            _lookMode = prmLookMode;
        }

        public void LookRotation(Transform prmCharacter, Transform prmCamera)
        {
            float yRot = Input.GetAxis("Mouse X") * deltaX;
            float xRot = Input.GetAxis("Mouse Y") * deltaY;

            if (_lookMode == MouseLookMode.DragMode)
            {
                if (!Input.GetMouseButton(0))
                    return;

                _characterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
                _cameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

                prmCharacter.localRotation = _characterTargetRot;
                prmCamera.localRotation = _cameraTargetRot;

            }
            else if (_lookMode == MouseLookMode.NormalMode)
            {
                
            }
        }
    }    
}

