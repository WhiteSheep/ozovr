﻿using System;
using System.Collections;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class WWWMgr : MonoBehaviour
    {

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {

        }

        public static IEnumerator Send(string prmPath, Action<WWW> prmAction)
        {
            WWW www = new WWW(prmPath);
            yield return www;

            if (www.error != null)
            {
                DebugEx.LogError(www.error);
            }

            prmAction(www);
        }

        public static IEnumerator Send(string prmPath, WWWForm prmForm, Action<WWW> prmAction)
        {
            WWW www = new WWW(prmPath, prmForm);
            yield return www;

            if (www.error != null)
                DebugEx.LogError(www.error);

            prmAction(www);
        }
    }
}