﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

namespace VBGames.PlayMaker.Actions
{
    public class SceneManager : ISetup
    {
        private List<Place> _lUsedPlace = new List<Place>();
        private List<Place> _lEmptyPlace = new List<Place>();
        private Vector2 _v2RoomCounts;
        private Vector2 _v2RoomSize;

        //private static SceneManager _instance;
        private SceneCreator _sceneCreator;
        private bool _isIdle = true;
        private bool _isSetuped;

        public void Setup()
        {
            _sceneCreator = new SceneCreator();

            //  Init all EmptyRoom
            _v2RoomCounts = new Vector2(11, 11);
            _v2RoomSize = new Vector2(80, 60);

            for (int i = 1; i < _v2RoomCounts.x; ++i)
                for (int j = 1; j < _v2RoomCounts.y; ++j)
                {
                    GameObject obj = new GameObject();
                    Place place = new Place(AppSetting.DEFAULT_ROOM_NAME, obj);

                    // x-z field
                    place.gameObj.transform.position = new Vector3(i * _v2RoomSize.x, 0.0f, j * _v2RoomSize.y);

                    _lEmptyPlace.Add(place);
                }

            _isSetuped = true;
        }

        //-----------------------------------------------------------------------
        public bool FinishedSetup()
        {
            return _isSetuped;
        }

        //-----------------------------------------------------------------------
        public void LateUpdate()
        {
            List<Place> removedPlace = new List<Place>();

            //  update need removed lUsedPlace 
            foreach (Place place in _lUsedPlace)
            {
                place.Update();

                if (!place.IsAlive())
                    removedPlace.Add(place);
            }

            //  return place to EmptyList
            foreach (Place place in removedPlace)
            {
                _ReturnPlace(place);
            }
        }

        //-----------------------------------------------------------------------
        private void _ReturnPlace(Place prmPlace)
        {
            //  remove place child
            prmPlace.Unload();

            //  move place to empty list
            _lUsedPlace.Remove(prmPlace);

            prmPlace.PlaceName = AppSetting.DEFAULT_ROOM_NAME;
            _lEmptyPlace.Add(prmPlace);
        }

        //-----------------------------------------------------------------------
        private Place _GetEmptyPlace()
        {
            Place retObj = null;

            if (_lEmptyPlace.Count != 0)
            {
                retObj = _lEmptyPlace[0];
                _lEmptyPlace.RemoveAt(0);
                _lUsedPlace.Add(retObj);
            }
            else
            {
                DebugEx.Log("There is no empty room to allocation");
            }

            return retObj;
        }

        //-----------------------------------------------------------------------
        private bool _TryGetUsedPlace(string prmName, out Place prmObj)
        {
            foreach (Place obj in _lUsedPlace)
            {
                if (obj.PlaceName == prmName)
                {
                    prmObj = obj;
                    return true;
                }
            }

            prmObj = null;
            return false;
        }

        //-----------------------------------------------------------------------
        private IEnumerator _InitialPlace(string prmUserId, string prmRoomId, Command prmCmd)
        {
            Place place = null;
            bool runCmdImmediately = true;
            bool findPlace = _TryGetUsedPlace(prmRoomId, out place);
            
            if (!findPlace)
            {
                //  Get EmptyPlace
                place = _GetEmptyPlace();
                if (place != null)
                {
                    place.PlaceName = prmRoomId;

                    //  Get user scene file path
                    WWWForm form = new WWWForm();
                    form.AddField("userId", prmUserId);
                    form.AddField("virtualMourningHall_id", prmRoomId);

                    DownloadingRequest getUserSceneInfo = new RequestWWW(Config.GetUserSceneInfoPort, null, form).Start();
                    while (!getUserSceneInfo.IsDone())
                        yield return null;
                   
                    //  Load Scene 
                    _sceneCreator.LoadScene(_GetUserScenePath(getUserSceneInfo.www), (prmGoList) =>
                    {
                        foreach (GameObject go in prmGoList)
                        {
                            go.transform.parent = place.gameObj.transform;
                            go.transform.position += place.gameObj.transform.position;
                        }

                        place.AddPlaceHolder(prmUserId, place.CreatePlaceHolder(prmUserId));
                        //GameManagerStd.Instance.GetScreenCapture().AddCommand(prmCmd);
                    });
                }
            }
            else
            {
                if (!place.ContainsHolder(prmUserId))
                    place.AddPlaceHolder(prmUserId, place.CreatePlaceHolder(prmUserId));
                else
                    place.RefreshHolderTrigger(prmUserId);

                //GameManagerStd.Instance.GetScreenCapture().AddCommand(prmCmd);
            }

            _isIdle = true;
        }

        //-----------------------------------------------------------------------
        private string _GetUserScenePath(WWW prmWWW)
        {
            XMLDom xmlDoc = new XMLDom();
            xmlDoc.LoadWithData(prmWWW.text);

            int errorCode = Convert.ToInt32(xmlDoc.GetElementValue("./errorCode"));
            string errorInfo = xmlDoc.GetElementValue("./errorInfo");

            if (errorCode == Config.InterfaceNoError)
                return Config.FileServerBasePath + xmlDoc.GetElementValue("./contentPath");
            else
                DebugEx.LogError(string.Format("error code {0} : {1}", errorCode, errorInfo));

            return null;
        }

        //-----------------------------------------------------------------------
        public bool IsIdle()
        {
            return _isIdle && _sceneCreator.IsDone;
        }

        //-----------------------------------------------------------------------
        public void TransferCommand(Command cmd)
        {
            _isIdle = false;

            try
            {
                switch (cmd.cType)
                {
                    case AppSetting.Command.Move:
                    case AppSetting.Command.Rotate:
                        Assist.CentralMono().StartCoroutine(_InitialPlace(cmd.sUserID, cmd.sRoomID, cmd));
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                DebugEx.Log(e.ToString());
            }
        }


    }
}