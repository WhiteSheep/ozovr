﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    public class Place
    {
        public GameObject gameObj;
        private Dictionary<string, TriggerEvent> _dicPlaceHolder;
        private bool _bFirstUse;

        public string PlaceName
        {
            get { return gameObj.name; }
            set { gameObj.name = value; }
        }

        //-----------------------------------------------------------------------
        public Place(string prmPlaceName, GameObject prmGameObj)
        {
            gameObj = prmGameObj;
            gameObj.name = prmPlaceName;
            _bFirstUse = true;

            _dicPlaceHolder = new Dictionary<string, TriggerEvent>();
        }

        //-----------------------------------------------------------------------
        public GameObject CreatePlaceHolder(string prmUserId)
        {
            //  Create Camera
            string cameraName = prmUserId + "_" + gameObj.name;
            GameObject cameraGameObj = GameObjectCreator.CreateCamera(cameraName);

            //  Move Camera to Place
            cameraGameObj.transform.parent = gameObj.transform;
            cameraGameObj.transform.position += gameObj.transform.position;
            cameraGameObj.transform.position += new Vector3(40f, 7f, 30f);

            return cameraGameObj;
        }

        //-----------------------------------------------------------------------
        public bool AddPlaceHolder(string prmUserName, GameObject prmCameraGameObj)
        {
            //  Create trigger and add trigger to manager
            TriggerEvent trigger = new TriggerEvent(20, 1);
            //GameManagerStd.Instance.GetEventManager().AddTrigger(trigger);

            _dicPlaceHolder.Add(prmUserName, trigger);
            _bFirstUse = false;

            return true;
        }

        //-----------------------------------------------------------------------
        public bool RemovePlaceHolder(string prmUserId)
        {
            _dicPlaceHolder.Remove(prmUserId);
            return true;
        }

        //-----------------------------------------------------------------------
        public bool ContainsHolder(string prmUserId)
        {
            return _dicPlaceHolder.ContainsKey(prmUserId);
        }

        //-----------------------------------------------------------------------
        public void RefreshHolderTrigger(string prmUserId)
        {
            TriggerEvent trigger = null;
            _dicPlaceHolder.TryGetValue(prmUserId, out trigger);

            if (trigger != null)
                trigger.Reset();
        }

        //-----------------------------------------------------------------------
        public bool IsAlive()
        {
            return _dicPlaceHolder.Count != 0 || _bFirstUse;
        }

        //-----------------------------------------------------------------------
        public void Update()
        {
            List<string> removeList = new List<string>();

            //  check holder state
            foreach (var pair in _dicPlaceHolder)
            {
                TriggerEvent trigger = pair.Value;

                if (!trigger.IsAlive())
                    removeList.Add(pair.Key);
            }

            foreach (string name in removeList)
            {
                _dicPlaceHolder.Remove(name);
            }
        }

        //-----------------------------------------------------------------------
        public void Unload()
        {
            //  Remove all children
            /*
            List<GameObject> children = new List<GameObject>();
            for (int i = 0; i < gameObj.transform.childCount; ++i)
                children.Add(gameObj.transform.GetChild(i).gameObject);
            children.ForEach(child => Assist.DestroyUo(child));
            children.Clear();
            */

            Assist.DestroyAllChildren(gameObj);
            Assist.DestroyObject(gameObj);

            _bFirstUse = true;
            _dicPlaceHolder.Clear();
        }
    }
}