﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class UITributeItem : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Text _name;
        [SerializeField] private Text _description;

        private TributeInfo _tributeInfo = null;


        public void SetTributeInfo(TributeInfo tributeInfo)
        {
            _tributeInfo = tributeInfo;

            _name.text = tributeInfo.name;
            _description.text = tributeInfo.description;

            if (tributeInfo.sprite != null)
                _image.sprite = tributeInfo.sprite;
            else
            {
                new RequestWWW(tributeInfo.spritePath, (www) =>
                {
                    Texture2D tex2D = www.texture;
                    if (tex2D != null)
                    {
                        if (_image != null)
                        {
                            tributeInfo.sprite = Sprite.Create(tex2D, new Rect(0, 0, tex2D.width, tex2D.height),
                                new Vector2(0.5f, 0.5f));
                            _image.sprite = tributeInfo.sprite;
                        }
                    }
                }).Start();
            }
            
        }

        public void SubmitTributeItem()
        {
            //  Upload selected tribute
            WWWForm uploadForm = new WWWForm();

            uploadForm.AddField("roomId", User.virtualMourningHallId);
            uploadForm.AddField("userId", User.Id);
            uploadForm.AddField("tributeId", _tributeInfo.id);

            UIManager.Instance.SetDataLoadingWindowState(true);
            new RequestWWW(Config.AddTributeHallPort, (www) =>
            {
                UIManager.Instance.SetDataLoadingWindowState(false);

                //  check return info
                XMLDom xmlDom = new XMLDom();
                xmlDom.LoadWithData(www.data);

                int errorCode = Convert.ToInt32(xmlDom.GetElement("./errorCode").Value);
                string errorInfo = xmlDom.GetElement("./errorInfo").Value;

                if (errorCode == Config.InterfaceNoError)
                {
                    UIManager.Instance.OpenConfirmWindow("通知", string.Format("您选择的贡品“{0}”已经成功上贡!", _tributeInfo.name));    
                }
                else
                {
                    UIManager.Instance.OpenConfirmWindow("通知", string.Format("操作失败，错误代码:{0},{1}", errorCode, errorInfo));
                }
            }, uploadForm).Start();

            
        }
    }
}
