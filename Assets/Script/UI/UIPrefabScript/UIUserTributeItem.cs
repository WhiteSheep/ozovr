﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class UIUserTributeItem : MonoBehaviour {

    [SerializeField] private GameObject _username;
    [SerializeField] private GameObject _tributeDate;
    [SerializeField] private GameObject _tributePic;

    private UserTributeInfo _userTributeInfo;

    public void Init(UserTributeInfo item)
    {
        _userTributeInfo = item;

        _username.GetComponent<Text>().text = item.trueName;
        _tributeDate.GetComponent<Text>().text = item.userTributeDate.ToString(AppSetting.DEFAULT_DATE_FORMAT);

        if (item.tributeInfo.sprite != null)
        {
            _tributePic.GetComponent<Image>().sprite = item.tributeInfo.sprite;
            _ChechTimeValid();
        }
        else
        {
            new RequestWWW(item.tributeInfo.spritePath, (www) =>
            {
                Texture2D tex2D = www.texture;
                if (tex2D != null)
                {
                    if (_tributePic != null)
                    {
                        item.tributeInfo.sprite = Sprite.Create(tex2D,
                            new Rect(0, 0, tex2D.width, tex2D.height), new Vector2(0.5f, 0.5f));
                        _tributePic.GetComponent<Image>().sprite = item.tributeInfo.sprite;

                        _ChechTimeValid();
                    }
                }

            }).Start();
        }
    }

    //  计算贡品上贡时间差
    private void _ChechTimeValid()
    {
        TimeSpan ts = DateTime.Now - _userTributeInfo.userTributeDate;
        if (ts.TotalMinutes > 15)
            _tributePic.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 0.7f);

    }
}

}
