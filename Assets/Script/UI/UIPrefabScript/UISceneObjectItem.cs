﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ozovr.SceneObject;
using UnityEngine;
using UnityEngine.UI;
using VBGames.PlayMaker.Actions;
using UnitTemplate = Ozovr.SceneObject.UnitTemplate;

namespace VBGames.PlayMaker.UI
{
    public class UISceneObjectItem : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Text _name;
        [SerializeField] private Text _description;
        [SerializeField] private GameObject _userObjectReplaceMenu;
        
        private SceneObjectInfo _sceneObjectInfo;

        public void Init(SceneObjectInfo prmObj, GameObject _parent)
        {
            _sceneObjectInfo = prmObj;
            _userObjectReplaceMenu = _parent;

            _image.sprite = _sceneObjectInfo.sprite;
            _name.text = _sceneObjectInfo.name;
            _description.text = _sceneObjectInfo.description;
        }

        public void ClickToView3DModel()
        {
            var unit = SceneObjectTemplateMgr.Instance.GetTemplateById<UnitTemplate>(_sceneObjectInfo.binderId);

            //  调整展示模型相关参数
            GameObject showObj = unit.CreateSceneObject();
            showObj.tag = Tag.Untagged;
            Assist.SetLayer(showObj, LayerMask.NameToLayer("UI"));
            showObj.transform.localPosition = new Vector3(0f, 0f, -150f);

            //  调整模型大小
            Vector3 min, max, size;
            Assist.GetAABBSize(showObj, out min, out max);
            size = max - min;
            float deltaScale = Mathf.Min(200 / size.x, 200 / size.y);
            showObj.transform.localScale = showObj.transform.localScale*deltaScale;

            //  添加模型
            showObj.AddComponent<UIModelShow>();

            var ctrlScript = _userObjectReplaceMenu.GetComponent<UserObjectReplaceMenuService>();
            ctrlScript.Show3DModel(showObj);
            ctrlScript.replaceTemplateId = _sceneObjectInfo.binderId;
        }
    }
}