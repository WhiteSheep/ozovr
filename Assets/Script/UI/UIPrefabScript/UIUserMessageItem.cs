﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace VBGames.PlayMaker.UI
{
    public class UIUserMessageItem : MonoBehaviour
    {
        [SerializeField] private GameObject _msgTitle;
        [SerializeField] private GameObject _msgContent;

        public void Init(UserMessage prmUserMessage)
        {
            if (_msgTitle != null)
                _msgTitle.GetComponent<Text>().text = prmUserMessage.GetMessageTitle();

            if (_msgContent != null)
                _msgContent.GetComponent<Text>().text = prmUserMessage.GetContent();
        }
    }
}
