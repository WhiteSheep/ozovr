﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace VBGames.PlayMaker.UI
{
    public class ConfirmWindowService : MonoBehaviour
    {
        [SerializeField] private GameObject _contentGameObject;
        [SerializeField] private GameObject _titleGameObject;

        public void SetContent(string text)
        {
            _contentGameObject.GetComponent<Text>().text = text;
        }

        public void SetTitle(string text)
        {
            _titleGameObject.GetComponent<Text>().text = text;
        }
    }
}