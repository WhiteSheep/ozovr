﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Xml.Linq;
using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class MessageMenuService : MonoBehaviour
    {
        [SerializeField] private GameObject _contentGameObject;

        public void LoadUserMessage()
        {
            WWWForm uploadForm = new WWWForm();
            uploadForm.AddField("limit", 100);
            uploadForm.AddField("page", 1);
            uploadForm.AddField("id", User.virtualMourningHallId);

            new RequestWWW(Config.GetCommentsPort, (www) =>
            {
                XMLDom xmlDom = new XMLDom();
                xmlDom.LoadWithData(www.data);

                //  检查数据合法性
                int errorCode = Convert.ToInt32(xmlDom.GetElement("./errorCode").Value);
                string errorInfo = xmlDom.GetElement("./errorInfo").Value;

                if (errorCode == Config.InterfaceNoError)
                {
                    IEnumerable<XElement> messages = xmlDom.GetElements("./message/e");

                    //  获取UI项的Prefab
                    GameObject messageItemPrefab = Resources.Load<GameObject>("UIService/MessageMenu/MessageItem");
                    
                    //  清空当前UI中的内容
                    Assist.DestroyAllChildren(_contentGameObject);

                    //  构造UI项并绑定到窗口中
                    foreach (var item in messages)
                    {
                        GameObject messageGo = Instantiate(messageItemPrefab);
                        var ctrlScript = messageGo.GetComponent<UIUserMessageItem>();

                        UserMessage userMsg = new UserMessage();
                        userMsg.trueName = item.Element("trueName").Value;
                        userMsg.content = item.Element("content").Value;
                        userMsg.sendTime = MathfEx.ConvertIntDataTime(
                            Convert.ToDouble(item.Element("sendTime").Element("time").Value));

                        ctrlScript.Init(userMsg);
                        messageGo.transform.SetParent(_contentGameObject.transform, false);
                    }
                }
                else
                {
                    UIManager.Instance.OpenConfirmWindow("错误", 
                        string.Format("加载留言信息失败，错误代码：{0}, {1}"
                        ,errorCode, errorInfo));
                }
                

            }, uploadForm).Start();
        }
    }
}

