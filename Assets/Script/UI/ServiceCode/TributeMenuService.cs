﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class TributeMenuService : MonoBehaviour
    {
        [SerializeField] private GameObject _allTributeContent;
        [SerializeField] private GameObject _myTributeContent;
        [SerializeField] private GameObject _tributeSelectContent;

        private Dictionary<int, TributeInfo> _cacheTributeInfos = new Dictionary<int, TributeInfo>();

        public void Init()
        {
            _cacheTributeInfos.Clear();

            //  Get all tributes as cache
            UIManager.Instance.SetDataLoadingWindowState(true);
            new RequestWWW(
                Config.GetAllTributePort,
                (www) =>
                {
                    XMLDom dom = new XMLDom();
                    dom.LoadWithData(www.data);

                    int errorCode = Convert.ToInt32(dom.GetElement("./errorCode").Value);
                    string errorInfo = dom.GetElement("./errorInfo").Value;

                    if (errorCode == Config.InterfaceNoError)
                    {
                        IEnumerable<XElement> tributes = dom.GetElements("./tributes/e");

                        //  Analysis and create tributes
                        foreach (var t in tributes)
                        {
                            TributeInfo tributeInfo = new TributeInfo();
                            tributeInfo.id = Convert.ToInt32(t.Element("id").Value);
                            tributeInfo.description = t.Element("description").Value;
                            tributeInfo.name = t.Element("name").Value;
                            tributeInfo.spritePath = Config.FileServerBasePath + t.Element("portrait").Value;

                            _cacheTributeInfos.Add(tributeInfo.id, tributeInfo);
                        }
                    }
                    else
                    {
                        UIManager.Instance.OpenConfirmWindow("错误",
                            string.Format("贡品下载信息错误，错误号码：{0}, {1}", errorCode, errorInfo));
                    }

                    UIManager.Instance.SetDataLoadingWindowState(false);

                    //   Set tab
                    SetAllTributeTab();
                }).Start();
        }

        public void SetAllTributeTab()
        {
            GetAllSubmitTributes(_allTributeContent);
        }

        public void SetMyTributeTab()
        {
            GetAllSubmitTributes(_myTributeContent, User.Id);
        }


        public void GetAllSubmitTributes(GameObject go, int userIdFilter = -1)
        {
            WWWForm uploadForm = new WWWForm();
            uploadForm.AddField("id", User.virtualMourningHallId);

            //  Get all tributes for this room
            new RequestWWW(Config.GetAllTributePortByRoomId,
                (www) =>
                {
                    XMLDom xmlDom = new XMLDom();
                    xmlDom.LoadWithData(www.data);

                    int errorCode = Convert.ToInt32(xmlDom.GetElement("./errorCode").Value);
                    string errorInfo = xmlDom.GetElement("./errorInfo").Value;

                    if (errorCode == Config.InterfaceNoError)
                    {
                        IEnumerable<XElement> tributes = xmlDom.GetElements("./tributeHalls/e");
                        List<UserTributeInfo> userTributeInfos = new List<UserTributeInfo>();

                        foreach (var item in tributes)
                        {
                            UserTributeInfo singleInfo = new UserTributeInfo();
                            DateTime sendTime =
                                MathfEx.ConvertIntDataTime(
                                    Convert.ToDouble(item.Element("updateTime").Element("time").Value));
                            singleInfo.userTributeDate = sendTime;
                            singleInfo.userID = Convert.ToInt32(item.Element("userId").Value);
                            singleInfo.trueName = item.Element("trueName").Value;
                            singleInfo.tributeID = Convert.ToInt32(item.Element("tributeId").Value);

                            //  过滤用户
                            if (userIdFilter == -1 || userIdFilter == singleInfo.userID)
                                userTributeInfos.Add(singleInfo);
                        }

                        UpdateTributeTab(go, userTributeInfos);
                    }
                    else
                    {
                        UIManager.Instance.OpenConfirmWindow("错误",
                            string.Format("加载该房间上贡信息失败，错误代码:{0}, {1}", errorCode, errorInfo));
                    }
                }, uploadForm).Start();
        }

        /***
         * 添加上贡信息
         */

        private void UpdateTributeTab(GameObject go, List<UserTributeInfo> tributeInfos, int userId = -1)
        {
            //  Remove all childs
            Assist.DestroyAllChildren(go);

            List<GameObject> allChilds = new List<GameObject>();

            //  Create title
            GameObject titlePrefab = Resources.Load<GameObject>("UIService/AllTributeTab/TributeTitle");
            allChilds.Add(Instantiate(titlePrefab));

            GameObject borderPrefab = Resources.Load<GameObject>("UIService/AllTributeTab/LayoutBorder");
            allChilds.Add(Instantiate(borderPrefab));

            //  Create all kinds of item
            GameObject itemPrefab = Resources.Load<GameObject>("UIService/AllTributeTab/UserTributeItem");

            foreach (UserTributeInfo single in tributeInfos)
            {
                GameObject item = Instantiate(itemPrefab);
                TributeInfo tributeInfo;
                _cacheTributeInfos.TryGetValue(single.tributeID, out tributeInfo);

                //  初始化用户上传贡品信息表中的每一个项
                if (tributeInfo != null)
                {
                    single.tributeInfo = tributeInfo;

                    UIUserTributeItem ctrlScript = item.GetComponent<UIUserTributeItem>();
                    ctrlScript.Init(single);
                }

                //  将项添加到打印表中
                allChilds.Add(item);
            }

            //  将打印表中所有信息写入UI
            foreach (var item in allChilds)
                item.transform.SetParent(go.transform, false);
        }

        /***
         *  向摆放供品的UI添加可选择的贡品信息
         */

        public void SetTributeSelectTab()
        {
            Assist.DestroyAllChildren(_tributeSelectContent);

            //  将缓存中的所有TributeInfo转化为UI中的项
            foreach (var kv in _cacheTributeInfos)
            {
                TributeInfo tributeInfo = kv.Value;

                //  创建贡品项并加入到UI界面中
                GameObject tributeItemPrefab = Resources.Load<GameObject>("UIService/SetTributeTab/TributeItem");
                GameObject item = Instantiate(tributeItemPrefab);
                UITributeItem ctrlScript = item.GetComponent<UITributeItem>();
                ctrlScript.SetTributeInfo(tributeInfo);
                item.transform.SetParent(_tributeSelectContent.transform, false);

                //  添加分隔线
                GameObject cutlinePrefab = Resources.Load<GameObject>("UIService/SetTributeTab/CutLine");
                Instantiate(cutlinePrefab).transform.SetParent(_tributeSelectContent.transform, false);
            }
        }
    }
}