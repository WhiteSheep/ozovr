﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using Ozovr.SceneObject;
using UnityEngine;
using VBGames.PlayMaker.Actions;
using UnitTemplate = Ozovr.SceneObject.UnitTemplate;

namespace VBGames.PlayMaker.UI
{
    public class UserObjectReplaceMenuService : MonoBehaviour
    {
        [SerializeField] private GameObject _content;
        [SerializeField] private GameObject _viewer;

        private GameObject _needDestroyObj;
        public string replaceTemplateId;

        public void Init(GameObject selected)
        {
            //  初始化信息记录
            replaceTemplateId = "";
            _needDestroyObj = selected;

            //  摧毁目前菜单中的所有内容
            Assist.DestroyAllChildren(_content);
            Assist.DestroyAllChildren(_viewer);

            //  To-do : 找到被选中单位的类型，然后把所有相关类型的Unit都过滤出来，再进一步过滤Item
            GameObject itemPrefab = Resources.Load<GameObject>("UIService/SceneObjectReplaceMenu/SceneObjectItem");
            UnitTemplate selectedTemplate = selected.GetComponent<SceneObjectDataCtrl>().srcTemplate;
            List<ItemTemplate> items = new List<ItemTemplate>();
            IEnumerable<UnitTemplate> units = SceneObjectTemplateMgr.Instance.GetAllTemplateByType<UnitTemplate>();
           
            foreach (var unit in units)
            {
                if (unit.constructProp == selectedTemplate.constructProp)
                {
                    var item = SceneObjectTemplateMgr.Instance.GetTemplateById<ItemTemplate>(unit.itemBindId);
                    if (item != null)
                    {
                        //  items.Add(item);
                        SceneObjectInfo sceneObjectInfo = new SceneObjectInfo();
                        sceneObjectInfo.name = item.name;
                        sceneObjectInfo.binderId = unit.id;
                        sceneObjectInfo.description = item.description;
                        sceneObjectInfo.sprite = Sprite.Create(item.iconTex2D,
                            new Rect(0, 0, item.iconTex2D.width, item.iconTex2D.height), new Vector2(0.5f, 0.5f));
                        sceneObjectInfo.type = unit.constructProp;

                        //  初始化Item
                        GameObject itemObject = Instantiate(itemPrefab);
                        var ctrlScript = itemObject.GetComponent<UISceneObjectItem>();
                        ctrlScript.Init(sceneObjectInfo, gameObject);

                        //  将Item添加到界面中
                        itemObject.transform.SetParent(_content.transform, false);

                    }
                        
                }
            }
        }

        public void Show3DModel(GameObject gameObject)
        {
            Assist.DestroyAllChildren(_viewer);
            gameObject.transform.SetParent(_viewer.transform, false);
        }

        public void OnModelApply()
        {
            if (string.IsNullOrEmpty(replaceTemplateId))
                return;

            //  To-do : 替换场景中的模型
            GameObject copyObj = SceneObjectTemplateMgr.Instance.GetTemplateById(replaceTemplateId).CreateSceneObject();
            ReplaceModel(_needDestroyObj, copyObj);

            //  关闭窗体
            UIManager.Instance.SetUserObjectReplaceMenuState(false);
        }

        public void ReplaceModel(GameObject removeModel, GameObject newModel)
        {
            newModel.transform.position = removeModel.transform.position;
            newModel.transform.rotation = removeModel.transform.rotation;

            //  计算模型替换前后的高度差，并且修正新模型高度差
            float yDelta = Assist.GetAABBSize(newModel).y - Assist.GetAABBSize(removeModel).y;
            newModel.transform.position = Assist.ChangeVec3Y(newModel.transform.position,
                newModel.transform.position.y + yDelta / 2f);

            Assist.DestroyObject(removeModel);
            Assist.SetActive(newModel, true);
        }
    }
}
