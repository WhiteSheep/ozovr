﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    [Serializable]
    public class TweenInfo
    {
        [SerializeField] public TweenPlayType playType = TweenPlayType.Once;
        [SerializeField] public float duration = 1;
        [SerializeField] public float delay = 0;
    }
}
