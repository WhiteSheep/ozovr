﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    public class TweenEnable : MonoBehaviour
    {
        private bool _isForward = true;
        public void PlayTween()
        {
            if (gameObject != null)
            {
                gameObject.SetActive(_isForward);
                _isForward = !_isForward;
            }
        }
    }
}
