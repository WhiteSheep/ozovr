﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace VBGames.PlayMaker.UI
{
    [RequireComponent(typeof(Image))]
    public class TweenSpriteAnimation : MonoBehaviour
    {
        [SerializeField] private bool _isLoop = true;
        [SerializeField] private int _framePerSprite = 1;
        [SerializeField] private float speed = 1.0f;
        [SerializeField] private List<Sprite> _sprites = new List<Sprite>();

        private float _frameElapsed = 0;
        private Image _image;
        // Use this for initialization
        void Start()
        {
            _image = gameObject.GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            _frameElapsed += speed;
            if (_isLoop)
                _frameElapsed = _frameElapsed%(_framePerSprite*_sprites.Count);

            int idx = (int) Mathf.Floor(_frameElapsed / _framePerSprite);
            _image.sprite = _sprites[idx];
        }
    }
}
