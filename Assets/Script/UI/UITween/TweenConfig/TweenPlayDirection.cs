﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBGames.PlayMaker.UI
{
    public enum TweenPlayDirection
    {
        Toggle = 0,
        Forward = 1,
        Reverse = -1
    }
}
