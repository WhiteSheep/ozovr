﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    public class TweenTabShow : MonoBehaviour
    {
        private List<GameObject> _tabs = new List<GameObject>();

        private GameObject _nowSelectedTab;
        public void OnEnable()
        {
            //  Get all child tab
            for (int idx = 0; idx < gameObject.transform.childCount; ++idx)
            {
                GameObject go = gameObject.transform.GetChild(idx).gameObject;
                go.SetActive(false);
                _tabs.Add(go);
            }

            //  Default set first is active
            if (_tabs.Count > 0)
            {
                _nowSelectedTab = _tabs.First();
                _nowSelectedTab.SetActive(true);
            }      
        }

        public void SelectTab(GameObject obj)
        {
            _nowSelectedTab.SetActive(false);
            _nowSelectedTab = obj;
            _nowSelectedTab.SetActive(true);
        }
    }
}
