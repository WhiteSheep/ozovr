﻿using System;
using UnityEngine;
using System.Collections;
using AnimationOrTween;
using DG.Tweening;
using UnityEngine.UI;
using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class TweenFade : MonoBehaviour
    {
        [SerializeField] protected float _from = 0;
        [SerializeField] protected float _to = 1;
        [SerializeField] protected TweenInfo _tweenInfo = new TweenInfo();

        protected Tween _nowTween;
        protected TweenPlayDirection _nowDirection = TweenPlayDirection.Forward;
        protected TweenCallback _callbackAction;
        void Start()
        {
            DOTween.Init();
        }

        protected virtual void _Binding()
        {
            Debug.LogError("Don't use TweenFade, you can consider to use TweenFadeXXX series");
        }

        protected virtual Tween _DOFade(float f, float t)
        {
            Debug.LogError("Don't use TweenFade, you can consider to use TweenFadeXXX series");
            return null;
        }

        protected virtual void _UnenableTweenTarget()
        {
            gameObject.SetActive(false);

            if (_callbackAction != null)
                _callbackAction();
        }

        public Tween PlayTween(TweenPlayDirection direction, bool forceEnable, TweenCallback onComplete = null)
        {
            _Binding();
            
            //  there is no need to play if not active and not force
            if (!gameObject.activeSelf && !forceEnable)
                return null;

            _callbackAction = (onComplete != null) ? onComplete : () => { };
            gameObject.SetActive(true);

            //  check last tween 
            if (_nowTween != null)
                _nowTween.Pause();

            switch (_tweenInfo.playType)
            {
                case TweenPlayType.Once:
                    _PlayOnce(direction);
                    break;
                case TweenPlayType.Loop:
                    _PlayLoop(direction);
                    break;
                case TweenPlayType.Pingpong:
                    _PlayPingPong(_from, _to);
                    break;
            }

            //  set delay
            _nowTween.SetDelay(_tweenInfo.delay);

            return _nowTween;
        }

        protected void _PlayOnce(TweenPlayDirection prmDirection)
        {
            TweenPlayDirection direction = 
                (prmDirection == TweenPlayDirection.Toggle) ? _nowDirection : prmDirection;

            //  play tween
            if (direction == TweenPlayDirection.Forward)
            {
                _nowTween = _DOFade(_from, _to).OnComplete(_callbackAction);
                _nowDirection = TweenPlayDirection.Reverse;
            }
            else if (direction == TweenPlayDirection.Reverse)
            {
                _nowTween = _DOFade(_to, _from).OnComplete(_UnenableTweenTarget);
                _nowDirection = TweenPlayDirection.Forward;
            }
        }

        protected void _PlayLoop(TweenPlayDirection prmDirection)
        {
            TweenPlayDirection direction =
                (prmDirection == TweenPlayDirection.Toggle) ? _nowDirection : prmDirection;

            float f = _from, t = _to;
            if (direction == TweenPlayDirection.Reverse)
                MathfEx.Swap(ref f, ref t);

            _nowTween = _DOFade(f, t).OnComplete(() =>
                {
                    _PlayLoop(prmDirection);
                });
        }

        protected void _PlayPingPong(float f, float t)
        {
            _nowTween = _DOFade(f, t).OnComplete(() =>
                {
                    _PlayPingPong(t, f);
                });
        }
    }
}

