﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    [RequireComponent(typeof(TweenFadeCanvasGroup))]
    public class TweenFadeCanvasGroup : TweenFade
    {
        protected CanvasGroup _canvasGroup;

        protected override void _Binding()
        {
            _canvasGroup = gameObject.GetComponent<CanvasGroup>();
        }

        protected override Tween _DOFade(float f, float t)
        {
            _canvasGroup.alpha = f;
            return _canvasGroup.DOFade(t, _tweenInfo.duration);
        }
    }
}
