﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using VBGames.PlayMaker.UI;

[RequireComponent(typeof(Text))]
public class TweenFadeText : TweenFade
{
    protected Text _text;
    protected override void _Binding()
    {
        _text = gameObject.GetComponent<Text>();
    }

    protected override Tween _DOFade(float f, float t)
    {
        return _text.DOFade(t, _tweenInfo.duration);
    }
}
