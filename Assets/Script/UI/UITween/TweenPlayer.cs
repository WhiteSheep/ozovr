﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VBGames.PlayMaker.UI
{
    public class TweenPlayer : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private GameObject _targetPlayer;
        [SerializeField] private TweenPlayDirection _playDirection;
        [SerializeField] private TweenTriggerType triggerType;
        [SerializeField] private bool _forceEnable = true;
        [SerializeField] private UnityEvent _onFinishCallback = new UnityEvent();

        private List<Tween> _tweens = new List<Tween>();
        private int _finishTweenCount = 0;

        public void OnEnable()
        {
            if (triggerType == TweenTriggerType.OnEnable)
                PlayTween();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (triggerType == TweenTriggerType.OnClick)
                PlayTween();
        }

        public void Update()
        {
            if (_tweens.Count > 0 && _tweens.Count <= _finishTweenCount)
            {
                _onFinishCallback.Invoke();
                _tweens.Clear();
            }
        }

        public void PlayTween()
        {
            if (_targetPlayer == null)
                return;

            //  force clear
            _tweens.Clear();
            _finishTweenCount = 0;

            TweenFade tweenFade = _targetPlayer.GetComponent<TweenFade>();
            if (tweenFade != null)
            {
                Tween tween = tweenFade.PlayTween(_playDirection, _forceEnable, 
                    () => { _finishTweenCount += 1; });
                if (tween != null)
                    _tweens.Add(tween);
            }
        }

        public static void PlayTween(GameObject target, TweenPlayDirection direction, bool forceEnable = true)
        {
            TweenFade tweenFade = target.GetComponent<TweenFade>();
            if (tweenFade != null)
                tweenFade.PlayTween(direction, forceEnable);
        }
    }
}