﻿using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject _confirmWindow;
        [SerializeField] private GameObject _dataLoadingWindow;
        [SerializeField] private GameObject _bigLoadingWindow;
        [SerializeField] private GameObject _userObjectReplaceMenu;
        
        private static UIManager _instance = null;

        public static UIManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UIManager();

                return _instance;
            }
        }

        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void OpenConfirmWindow(string titleInfo, string contentInfo)
        {
            var ctrlScript = _confirmWindow.GetComponent<ConfirmWindowService>();
            ctrlScript.SetTitle(titleInfo);
            ctrlScript.SetContent(contentInfo);

            TweenPlayer.PlayTween(_confirmWindow, TweenPlayDirection.Forward);
        }

        public void SetDataLoadingWindowState(bool enable)
        {
            _dataLoadingWindow.SetActive(enable);
        }

        public void SetBigLoadingWindowState(bool enable)
        {
            if (enable)
                TweenPlayer.PlayTween(_bigLoadingWindow, TweenPlayDirection.Forward);
            else
                TweenPlayer.PlayTween(_bigLoadingWindow, TweenPlayDirection.Reverse);
        }

        public void SetUserObjectReplaceMenuState(bool enable, GameObject selected = null)
        {
            if (enable)
            {
                TweenPlayer.PlayTween(_userObjectReplaceMenu, TweenPlayDirection.Forward);
                InitUserObjectRelaceMenu(selected);
            }
            else
                TweenPlayer.PlayTween(_userObjectReplaceMenu, TweenPlayDirection.Reverse);
        }

        public void InitUserObjectRelaceMenu(GameObject selected)
        {
            var ctrlScript = _userObjectReplaceMenu.GetComponent<UserObjectReplaceMenuService>();
            ctrlScript.Init(selected);
        }
    }
}
