﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VBGames.PlayMaker.UI
{
    public class TributeInfo
    {
        public int id;
        public Sprite sprite;
        public string name;
        public string description;
        public string spritePath;

        public TributeInfo() { }

        public TributeInfo(int prmId, string prmName, string prmDesc, string prmSpritePath)
        {
            id = prmId;
            name = prmName;
            description = prmDesc;
            spritePath = prmSpritePath;
        }
    }
}
