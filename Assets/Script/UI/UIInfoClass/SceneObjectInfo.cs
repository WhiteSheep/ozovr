﻿using UnityEngine;
using System.Collections;
using Ozovr.SceneObject;
using VBGames.PlayMaker.Actions;

public class SceneObjectInfo
{
    public string name;
    public string binderId;
    public string description;
    public UnitConstructProp type;
    public Sprite sprite;
}
