﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace VBGames.PlayMaker.UI
{
    public class UserTributeInfo
    {
        public int userID;
        public string trueName;
        public DateTime userTributeDate;
        public int tributeID;

        // 链接到TributeInfo上，用于刷新TributeInfo的缓存
        public TributeInfo tributeInfo;

        public UserTributeInfo() { }

    }
}
