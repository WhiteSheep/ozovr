﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VBGames.PlayMaker.Actions;

namespace VBGames.PlayMaker.UI
{
    public class UserMessage
    {
        public string trueName;
        public string content;
        public DateTime sendTime;

        public UserMessage() { }

        public UserMessage(string prmTrueName, string prmContent, DateTime prmSendTime)
        {
            trueName = prmTrueName;
            content = prmContent;
            sendTime = prmSendTime;
        }

        public string GetMessageTitle()
        {
            return string.Format("{0} {1}", trueName, sendTime.ToString(AppSetting.DEFAULT_DATE_FORMAT));
        }

        public string GetContent()
        {
            return content;
        }

    }
}
