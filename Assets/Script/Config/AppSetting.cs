﻿using UnityEngine;
using System.Collections;

namespace VBGames.PlayMaker.Actions
{
    /// <summary>
    /// Platfrom Type
    /// </summary>
    public enum TargetPlatformType
    {
        Pc = 0,
        Webgl = 1
    }

    /// <summary>
    /// Used to create config for different platform
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PlatformConfig<T>
    {
        public T Pc;
        public T Webgl;

        public PlatformConfig(T prmPc, T prmWebgl)
        {
            Pc = prmPc;
            Webgl = prmWebgl;
        }
    }

    /// <summary>
    /// Application setting informations
    /// </summary>
    public class AppSetting
    {
        public static void Setup()
        {
            if (string.IsNullOrEmpty(Application.absoluteURL))
                CONFIG_FILE_PATH.Webgl = "http://localhost:8080/OlineSacrifice/config.xml";
            else
            {
                string url = Application.absoluteURL;
                CONFIG_FILE_PATH.Webgl = url.Substring(0, url.LastIndexOf('/')) + "/config.xml";
            }
        }

        //  Setting application compile target platform
        public static TargetPlatformType TARGET_PLATFORM_TYPE = TargetPlatformType.Pc;

        //  Application config file path
        public static PlatformConfig<string> CONFIG_FILE_PATH =
            new PlatformConfig<string>(
                "config/config.xml",
                ""
                );

        //  If true, debug mode will open
        public static bool DEBUG_MODE = false;

        //  Float number compare accuration
        public static float FLOAT_MAX_LIMIT = 0.01f;

        //  When something is valid, use VALID_SETTING_COLOR
        public static Color VALID_SETTING_COLOR = new Color(1.0f, 1.0f, 1.0f);
        public static Color INVALID_SETTING_COLOR = new Color(1.0f, 0.5f, 0.5f);

        //  World Size
        public static WorldSize MAX_WORLD_SIZE = new WorldSize(0f, 0f, 0f, 150f, 120f, 30f);

        //  WWW Time out
        public static float WWW_TIME_OUT = 20f;

        //  WWW Downloading max count
        public static int DOWNLOADING_MAX_COUNT = 3;

        /***
         * ShaderCore Config
         */

        //  Degree of camera shot
        public static int DEGREE_OF_CAMERA_SHOT = 30;

        //  Max numbers of shader pictures per frame
        public static int MAX_SHADER_TIMES_PER_FRAME = 10;

        //  Max numbers of download assetbundle times per frame
        public static int MAX_DOWNLOAD_ASSETBUNDLE_TIMES_PER_FRAME = 5;

        //  Max numbers of output files once (Thread)
        public static int MAX_OUTPUTFILES_ONCE = 10;

        //  Command split char
        public static char CMD_SPLIT = ';';

        //  Default Name
        public static string DEFAULT_CAMERA_NAME = "Camera";
        public static string DEFAULT_ROOM_NAME = "EmptyRoom";

        //  Defalut Date Format
        public static string DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static string FULL_SPACE = "　";

        /// <summary>
        /// The Command Struct is :
        ///     UserID ; Command ; Params
        /// Command contains :
        ///     Rotate - with 2 params, x and y
        ///     Move - with 1 param, distance
        ///     Login - no param
        /// </summary>
        /// 
        public enum Command
        {
            Rotate,
            Move
        };
    }

    /// <summary>
    /// Descript World size
    /// </summary>
    public class WorldSize
    {
        public Vector3 Min;
        public Vector3 Max;

        public WorldSize(float prmMinX, float prmMinY, float prmMinZ, float prmMaxX, float prmMaxY, float prmMaxZ)
        {
            Min = new Vector3(prmMinX, prmMinY, prmMinZ);
            Max = new Vector3(prmMaxX, prmMaxY, prmMaxZ);
        }
    }
}