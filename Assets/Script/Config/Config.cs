﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using UnityEngine;
using VBGames.Utils.Logger;

namespace VBGames.PlayMaker.Actions
{
    public class Tag
    {
        public static string ExportGameObject = "ExportGameObject";
        public static string Untagged = "Untagged";
    }

    /// <summary>
    /// User Info 
    /// </summary>
    public class User
    {
        public static int Id;
        public static int virtualMourningHallId = -1;
        public static string Name;
        public static RoleType Role;
        public static string SceneFilePath;

        public static bool IsAdmin()
        {
            return Role == RoleType.Admin;
        }

        public static bool IsUserOrGuest()
        {
            return Role == RoleType.Guest || Role == RoleType.NormalUser;
        }
    }

    public enum RoleType
    {
        NormalUser = 0,
        Admin = 1,
        Guest = 2
    }

    public enum UploadType
    {
        Picture = 0,
        Video = 1,
        RenderResults = 3
    }


    public class RenderPlaceInfo
    {
        public int xCount;
        public int yCount;
        public int xSize;
        public int ySize;
    }

    /// <summary>
    /// Config contains the information from config.xml
    /// </summary>
    public class Config : ISetup

    {
        //  Exe
        public static bool RunInBackground;
        public static string ScreenShotExtension;
        public static float CameraCaptureDegree;
        public static RenderPlaceInfo RPlaceInfo = new RenderPlaceInfo();
        //  Server
        public static string MonitorIp;
        public static int MonitorPort;

        //  Database
        public static string MysqlURL;
        public static string MysqlUsername;
        public static string MysqlPassword;
        public static DBConfig MysqlConfig;

        //  Datas
        public static string AssetBundleDownloadBasePath;
        public static string SceneObjectConfigPath;
        public static string UnitConfigPath;
        public static string AudioConfigPath;
        public static XMLDom UnitConfig;
        public static XMLDom AudioConfig;
        public static XMLDom SceneObjectConfig;

        //  Update/Download Port
        public static string PortBasePath;
        public static string FileServerBasePath;
        public static string UploadFilePort;
        public static string UploadUserSceneInfoPort;
        public static string UploadScreenshotInfoPort;
        public static string GetUserSceneInfoPort;
        public static string GetUserInfoPort;
        public static string GetRoomInfoPort;
        public static string GetCommentsPort;
        public static string GetAllTributePort;
        public static string GetAllTributePortByRoomId;
        public static string AddTributeHallPort;
        public static string DeleteTributeHallPort;
        public static int InterfaceNoError = 3000;

        private LoadingState _loadingState;
        private static Config _instance;
        private static XMLDom _xmlDom;

        protected Config()
        {
            _xmlDom = new XMLDom();
            UnitConfig = new XMLDom();
            AudioConfig = new XMLDom();
            SceneObjectConfig = new XMLDom();

            _loadingState = new LoadingState("Loading config ... please wait for a moment ...");
            _loadingState.AddDlCount();
        }

        //-----------------------------------------------------------------------
        public static Config Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Config();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public bool FinishedSetup()
        {
            return _loadingState.IsFinished();
        }

        //-----------------------------------------------------------------------
        public void Setup()
        {
            //  Select properly method to load configuration file
            switch (AppSetting.TARGET_PLATFORM_TYPE)
            {
                case TargetPlatformType.Pc:
                {
                    _xmlDom.LoadWithRoute(AppSetting.CONFIG_FILE_PATH.Pc);
                    _LoadConfig();
                    break;
                }
                case TargetPlatformType.Webgl:
                {
                    Assist.CentralMono().StartCoroutine(_DownloadConfigFile(AppSetting.CONFIG_FILE_PATH.Webgl));
                    break;
                }
                default:
                {
                    DebugEx.Log("Platform type error, system can't discover");
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------
        private IEnumerator _DownloadConfigFile(string prmFilePath)
        {
            DownloadingRequest configReq = new RequestWWW(prmFilePath).Start();
            while (!configReq.IsDone())
                yield return null;
            _xmlDom.LoadWithData(configReq.www.text);

            _LoadConfig();
        }

        private void _LoadConfig()
        {
            //  Load config
            _LoadExeConfig();
            _LoadServerConfig();
            _LoadPortConfig();
            Assist.StartCoroutine(_LoadDataConfig);
        }

        //-----------------------------------------------------------------------
        private void _LoadExeConfig()
        {
            //  Screenshot Config
            ScreenShotExtension = _xmlDom.GetElementValue("./ExeConfig/ScreenShotExtension");

            //  Gloabl Config
            RunInBackground = bool.Parse(_xmlDom.GetElementValue("./ExeConfig/RunInBackground"));

            //  LogLevel
            VBLogger.level = VBLib.ParseEnum(_xmlDom.GetElementValue("./ExeConfig/LogLevel"), VBLoggerLevel.Log);
            
            //  CameraCaptureDegree
            CameraCaptureDegree = float.Parse(_xmlDom.GetElementValue("./ExeConfig/CameraCaptureDegree"));

            //  RenderPlaceInfo
            RPlaceInfo.xCount = int.Parse(_xmlDom.GetElementValue("./ExeConfig/RenderPlaceInfo/XCount"));
            RPlaceInfo.yCount = int.Parse(_xmlDom.GetElementValue("./ExeConfig/RenderPlaceInfo/YCount"));
            RPlaceInfo.xSize= int.Parse(_xmlDom.GetElementValue("./ExeConfig/RenderPlaceInfo/XSize"));
            RPlaceInfo.ySize = int.Parse(_xmlDom.GetElementValue("./ExeConfig/RenderPlaceInfo/YSize"));
        }

        //-----------------------------------------------------------------------
        private void _LoadServerConfig()
        {
            //  Server Config
            MonitorIp = _xmlDom.GetElementValue("./Server/MonitorIP");
            MonitorPort = int.Parse(_xmlDom.GetElementValue("./Server/MonitorPort"));
        }

        
        //-----------------------------------------------------------------------
        private void _LoadPortConfig()
        {
            //  Get base path
            PortBasePath = _xmlDom.GetElementValue("./InterfaceConfig/PortBasePath");
            
            //  Get Ports
            UploadFilePort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/UploadFilePort");
            UploadUserSceneInfoPort = PortBasePath +
                                      _xmlDom.GetElementValue("./InterfaceConfig/UploadUserSceneInfoPort");
            UploadScreenshotInfoPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/UploadScreenshotInfoPort");

            GetUserSceneInfoPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetUserSceneInfoPort");
            GetUserInfoPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetUserInfoPort");
            GetRoomInfoPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetRoomInfoPort");
            GetCommentsPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetCommentsPort");
            GetAllTributePort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetAllTributePort");
            GetAllTributePortByRoomId = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/GetAllTributeByRoomIdPort");
            AddTributeHallPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/AddTributeHallPort");
            DeleteTributeHallPort = PortBasePath + _xmlDom.GetElementValue("./InterfaceConfig/DeleteTributeHallPort");
        }

        //-----------------------------------------------------------------------
        private IEnumerator _LoadDataConfig()
        {
            string fileServerIp = _xmlDom.GetElementValue("./DatasConfig/FileServerIp");

            // Temp code, used to auto-adjust file server ip
#if !UNITY_EDITOR
            if (AppSetting.TARGET_PLATFORM_TYPE.Equals(TargetPlatformType.Webgl) && !string.IsNullOrEmpty(Application.absoluteURL))
            {
                Regex regex = new Regex("[/]*(.+?):([0-9]+)[/]*");
                Match matcher = regex.Match(Application.absoluteURL);
                if (matcher.Groups.Count > 0)
                    fileServerIp = matcher.Groups[1].Value + ":" + matcher.Groups[2].Value;
            }
#endif
            DebugEx.Log("File server address is : " + fileServerIp);
      
            FileServerBasePath = fileServerIp + _xmlDom.GetElementValue("./DatasConfig/FileServerBasePath");
            UnitConfigPath = FileServerBasePath + _xmlDom.GetElementValue("./DatasConfig/UnitConfigPath");
            AudioConfigPath = FileServerBasePath + _xmlDom.GetElementValue("./DatasConfig/AudioConfigPath");
            SceneObjectConfigPath = FileServerBasePath + _xmlDom.GetElementValue("./DatasConfig/SceneObjectConfigPath");
            AssetBundleDownloadBasePath = FileServerBasePath +
                                          _xmlDom.GetElementValue("./DatasConfig/AssetBundleDownloadBasePath");

            yield return
                    Assist.CentralMono().StartCoroutine(
                        DownloadManager.DownloadSync(new RequestWWW(UnitConfigPath,
                            (www) => { UnitConfig.LoadWithData(www.text); })));

            yield return
                Assist.CentralMono().StartCoroutine(
                    DownloadManager.DownloadSync(new RequestWWW(AudioConfigPath,
                        (www) => { AudioConfig.LoadWithData(www.text); })));

            yield return
                Assist.CentralMono().StartCoroutine(
                    DownloadManager.DownloadSync(new RequestWWW(SceneObjectConfigPath,
                        (www) => { SceneObjectConfig.LoadWithData(www.text); })));
            
            //  Flag when finish
            _loadingState.RemoveDlCount();
        }
    }
}