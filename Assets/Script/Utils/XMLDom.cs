﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace VBGames.PlayMaker.Actions
{
    public class XMLDom
    {
        protected string _sXmlFilePath;
        protected XDocument _xDoc;
        protected XElement _xRoot;
        protected string _sXmlData;

        public XElement Root
        {
            get { return _xRoot; }
        }

        public string XMLData
        {
            set { _sXmlData = value; }
        }

        public XMLDom(string xmlFilePath, string root = "")
        {
            _xDoc = XDocument.Load(xmlFilePath);
            _xRoot = _xDoc.Root;
        }

        public XMLDom()
        {

        }

        public void LoadWithData(string prmXmlData)
        {
            _xDoc = XDocument.Parse(prmXmlData);
            _xRoot = _xDoc.Root;
        }

        public void LoadWithRoute(string prmXmlRoute)
        {
            _xDoc = XDocument.Load(prmXmlRoute);
            _xRoot = _xDoc.Root;
        }

        public XElement GetElement(string prmXPath)
        {
            return _xRoot.XPathSelectElement(prmXPath);
        }

        public IEnumerable<XElement> GetElements(string prmXPath)
        {
            return _xRoot.XPathSelectElements(prmXPath);
        }

        public IEnumerable<XElement> GetAllByXPath(string xPath)
        {
            return _xRoot.XPathSelectElements(xPath);
        }

        public List<string> GetAllElementValue(string xPath)
        {
            List<string> retList = new List<string>();

            IEnumerable<XElement> xelements = GetAllByXPath(xPath);
            foreach (XElement xelement in xelements)
            {
                retList.Add(xelement.Value);
            }

            return retList;
        }

        public string GetElementValue(string xPath)
        {
            XElement element = GetElement(xPath);
            if (element == null)
                DebugEx.LogError(string.Format("xPath <{0}> not found", xPath));
            return element.Value;
        }

        public string GetElementAttr(string xPath, string attrName)
        {
            XElement element = null;
            element = _xRoot.XPathSelectElement(xPath);

            if (element != null)
            {
                return element.Attribute(attrName).Value;
            }

            //  If nothing found return null
            return null;
        }
    }
}