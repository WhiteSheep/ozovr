﻿using UnityEngine;
using System.Collections;
using System;
using Object = UnityEngine.Object;

namespace VBGames.PlayMaker.Actions
{
    //-----------------------------------------------------------------------
    /// LoadingState show the loading state for user
    //-----------------------------------------------------------------------
    public class LoadingState
    {
        public int itemCount = 0;
        public int loadedCount = 0;
        public string nowLoadingInfo;

        public float NowPercent
        {
            get { return ((float) loadedCount)/((float) itemCount); }
        }

        //-----------------------------------------------------------------------
        public string Info
        {
            get { return nowLoadingInfo; }
            set { nowLoadingInfo = value; }
        }

        //-----------------------------------------------------------------------
        public bool IsDone
        {
            get { return itemCount <= loadedCount; }
        }

        //-----------------------------------------------------------------------
        public bool IsFinished()
        {
            return itemCount <= loadedCount;
        }

        //-----------------------------------------------------------------------
        public LoadingState()
        {
            nowLoadingInfo = "";
        }

        //-----------------------------------------------------------------------
        public LoadingState(string prmInfo)
        {
            nowLoadingInfo = prmInfo;
        }

        //-----------------------------------------------------------------------
        public void AddDlCount(int prmVal = 1)
        {
            itemCount += prmVal;
        }

        //-----------------------------------------------------------------------
        public void RemoveDlCount(int prmVal = 1)
        {
            loadedCount += prmVal;
        }

        //-----------------------------------------------------------------------
        public static LoadingState CreateSingle(string prmInfo = "")
        {
            LoadingState retState = new LoadingState();
            retState.itemCount = 1;
            retState.Info = prmInfo;

            return retState;
        }
    }

    /// <summary>
    /// MathfEx provide extend methods for math calculations
    /// </summary>
    public class MathfEx
    {
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public static float Round(float prmNum)
        {
            return (float) Math.Round(prmNum, MidpointRounding.AwayFromZero);
        }

        //-----------------------------------------------------------------------
        public static int RoundToInt(float prmNum)
        {
            return (int) Round(prmNum);
        }

        //-----------------------------------------------------------------------
        public static Vector3 Vec3Abs(Vector3 prmVec3)
        {
            return new Vector3(Mathf.Abs(prmVec3.x), Mathf.Abs(prmVec3.y), Mathf.Abs(prmVec3.z));
        }

        //-----------------------------------------------------------------------
        public static DateTime ConvertIntDataTime(double prmSecond)
        {
            DateTime time = DateTime.MinValue;
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            time = startTime.AddMilliseconds(prmSecond);

            return time;
        }
    }

    //-----------------------------------------------------------------------
    /// Assist provide useful methods for unity3d
    //-----------------------------------------------------------------------
    public class Assist
    {
        public static GameObject GetChildGameObjectByName(GameObject prmGo, string prmName)
        {
            Transform[] ts = prmGo.transform.GetComponentsInChildren<Transform>();
            foreach (Transform t in ts)
            {
                if (t.gameObject.name.Equals(prmName))
                    return t.gameObject;
            }
            return null;
        }

        //-----------------------------------------------------------------------
        public static CentralMonoBehaviour CentralMono()
        {
            return CentralMonoBehaviour.Instance;
        }

        //-----------------------------------------------------------------------
        public static void StartCoroutine(CentralMonoBehaviour.CoroutineMethod prmCoroutineMethod)
        {
            CentralMonoBehaviour.Instance.StartCoroutine(prmCoroutineMethod());
        }

        //-----------------------------------------------------------------------
        public static void DestroyAllChildren(GameObject prmGo)
        {
            CentralMonoBehaviour.Instance.DestroyAllChildren(prmGo);
        }

        //-----------------------------------------------------------------------
        public static void DestroyObject(Object prmObject)
        {
            CentralMonoBehaviour.Instance.DestroyObject(prmObject);
        }

        //-----------------------------------------------------------------------
        public static GameObject Instantiate(GameObject prmGo)
        {
            return CentralMonoBehaviour.Instance.Instantiation(prmGo);
        }

        //-----------------------------------------------------------------------
        /// Return true if two float numbers are similiar
        //-----------------------------------------------------------------------
        public static bool FloatEqual(float lhs, float rhs)
        {
            return (Mathf.Abs(lhs - rhs) < AppSetting.FLOAT_MAX_LIMIT);
        }

        //-----------------------------------------------------------------------
        /// Return true if lhs less than rhs
        //-----------------------------------------------------------------------
        public static bool Vec3L(Vector3 lhs, Vector3 rhs)
        {
            if (Vec3Equal(lhs, rhs))
                return false;

            return Vec3LE(lhs, rhs);
        }

        //-----------------------------------------------------------------------
        /// Return true if lhs less or equal to rhs
        //-----------------------------------------------------------------------
        public static bool Vec3LE(Vector3 lhs, Vector3 rhs)
        {
            return lhs.x <= rhs.x && lhs.y <= rhs.y && lhs.z <= rhs.z;
        }

        //-----------------------------------------------------------------------
        /// Return true if two Vector3 are similiar
        //-----------------------------------------------------------------------
        public static bool Vec3Equal(Vector3 prmVec3A, Vector3 prmVec3B)
        {
            return Vector3.SqrMagnitude(prmVec3A - prmVec3B) < AppSetting.FLOAT_MAX_LIMIT;
        }

        //-----------------------------------------------------------------------
        /// Return true if number is even, the number will make Round
        //-----------------------------------------------------------------------
        public static bool IsEven(float prmNum)
        {
            return MathfEx.RoundToInt(prmNum)%2 == 0;
        }

        //-----------------------------------------------------------------------
        /// Return new Vector3 with new X-Axis value
        //-----------------------------------------------------------------------
        public static Vector3 ChangeVec3X(Vector3 prmVec3, float prmX)
        {
            return new Vector3(prmX, prmVec3.y, prmVec3.z);
        }

        //-----------------------------------------------------------------------
        /// Return new Vector3 with new Y-Axis value
        //-----------------------------------------------------------------------
        public static Vector3 ChangeVec3Y(Vector3 prmVec3, float prmY)
        {
            return new Vector3(prmVec3.x, prmY, prmVec3.z);
        }

        //-----------------------------------------------------------------------
        /// Return new Vector3 with new Z-Axis value
        //-----------------------------------------------------------------------
        public static Vector3 ChangeVec3Z(Vector3 prmVec3, float prmZ)
        {
            return new Vector3(prmVec3.x, prmVec3.y, prmZ);
        }

        //-----------------------------------------------------------------------
        /// Change a GameObject layer and it's also effective with all childs
        //-----------------------------------------------------------------------
        public static void ChangeLayer(GameObject prmGameObject, int prmLayer)
        {
            prmGameObject.layer = prmLayer;

            for (int idx = 0; idx < prmGameObject.transform.childCount; ++idx)
            {
                GameObject child = prmGameObject.transform.GetChild(idx).gameObject;
                child.layer = prmLayer;
                ChangeLayer(child, prmLayer);
            }
        }

        //-----------------------------------------------------------------------
        /// Open or close a gameobject collision propeties, including all childs
        //-----------------------------------------------------------------------
        public static void ChangeColliderState(GameObject prmGameObject, bool prmIfOpen)
        {
            BoxCollider[] boxColliders = prmGameObject.GetComponentsInChildren<BoxCollider>();
            MeshCollider[] meshColliders = prmGameObject.GetComponentsInChildren<MeshCollider>();

            foreach (BoxCollider collider in boxColliders)
                collider.enabled = prmIfOpen;


            foreach (MeshCollider collider in meshColliders)
                collider.enabled = prmIfOpen;
        }

        //-----------------------------------------------------------------------
        /// Change a gameobject opacity
        //-----------------------------------------------------------------------
        public static void ChangeOpacity(GameObject prmGameObject, float prmOpacity, bool prmAffectChilds = true)
        {
            MeshRenderer[] rends = new MeshRenderer[1];

            if (prmAffectChilds)
                rends = prmGameObject.GetComponentsInChildren<MeshRenderer>();
            else
                rends[0] = prmGameObject.GetComponent<MeshRenderer>();

            foreach (MeshRenderer rend in rends)
            {
                foreach (Material material in rend.materials)
                {
                    material.shader = Shader.Find("Transparent/Diffuse");
                    material.color = new Color(material.color.r, material.color.g, material.color.b, prmOpacity);
                }
            }
        }

        //-----------------------------------------------------------------------
        public static void ChangeColor(GameObject prmGameObject, Color prmColor, bool prmAffectChilds = true)
        {
            MeshRenderer[] rends = new MeshRenderer[1];

            if (prmAffectChilds)
                rends = prmGameObject.GetComponentsInChildren<MeshRenderer>();
            else
                rends[0] = prmGameObject.GetComponent<MeshRenderer>();

            foreach (MeshRenderer rend in rends)
            {
                foreach (Material material in rend.materials)
                {
                    material.color = new Color(prmColor.r, prmColor.g, prmColor.b, material.color.a);
                }
            }
        }

        //-----------------------------------------------------------------------
        /// Return true if gameobject contain any collider (BoxCollider or MeshCollider), don't check childs
        //-----------------------------------------------------------------------
        public static bool ContainCollider(GameObject prmGameObject)
        {
            BoxCollider boxCollider = prmGameObject.GetComponent<BoxCollider>();
            MeshCollider meshCollider = prmGameObject.GetComponent<MeshCollider>();

            return boxCollider || meshCollider;
        }

        //-----------------------------------------------------------------------
        /// Get a gameobject AABB size when gameobject has MeshRender
        //-----------------------------------------------------------------------
        public static void GetAABBSize(GameObject prmGameobject, out Vector3 prmMin, out Vector3 prmMax)
        {
            //  get the AABB of this object
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(-float.MaxValue, -float.MaxValue, -float.MaxValue);

            MeshRenderer[] rends = prmGameobject.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer rend in rends)
            {
                min = Vec3MinCombine(min, rend.bounds.min);
                max = Vec3MaxCombine(max, rend.bounds.max);
            }

            //  output the max and min
            prmMax = max;
            prmMin = min;
        }

        //-----------------------------------------------------------------------
        public static Vector3 GetAABBSize(GameObject prmGameobject)
        {
            Vector3 min, max;
            GetAABBSize(prmGameobject, out min, out max);
            return max - min;
        }

        //-----------------------------------------------------------------------
        /// combine two vector3 with each dimension max
        //-----------------------------------------------------------------------
        public static Vector3 Vec3MaxCombine(Vector3 prmVec3A, Vector3 prmVec3B)
        {
            //  calculate the max combine
            Vector3 retCombined = Vector3.zero;
            retCombined.x = Mathf.Max(prmVec3A.x, prmVec3B.x);
            retCombined.y = Mathf.Max(prmVec3A.y, prmVec3B.y);
            retCombined.z = Mathf.Max(prmVec3A.z, prmVec3B.z);

            return retCombined;
        }

        //-----------------------------------------------------------------------
        /// Combine two vector3 with each dimension min
        //-----------------------------------------------------------------------
        public static Vector3 Vec3MinCombine(Vector3 prmVec3A, Vector3 prmVec3B)
        {
            //  calculate the max combine
            Vector3 retCombined = Vector3.zero;
            retCombined.x = Mathf.Min(prmVec3A.x, prmVec3B.x);
            retCombined.y = Mathf.Min(prmVec3A.y, prmVec3B.y);
            retCombined.z = Mathf.Min(prmVec3A.z, prmVec3B.z);

            return retCombined;
        }

        //-----------------------------------------------------------------------
        /// Divide two vector3 by each dimension
        //-----------------------------------------------------------------------
        public static Vector3 Vec3Divide(Vector3 prmVec3A, Vector3 prmVec3B)
        {
            Vector3 retDivide = prmVec3A;
            try
            {
                retDivide.x = retDivide.x/prmVec3B.x;
                retDivide.y = retDivide.y/prmVec3B.y;
                retDivide.z = retDivide.z/prmVec3B.z;
            }
            catch (Exception e)
            {
                DebugEx.Log(e.Message);
            }

            return retDivide;
        }

        //-----------------------------------------------------------------------
        public static void SetActive(GameObject prmGameObject, bool prmActive)
        {
            for (int idx = 0; idx < prmGameObject.transform.childCount; ++idx)
            {
                GameObject child = prmGameObject.transform.GetChild(idx).gameObject;
                SetActive(child, prmActive);
                child.SetActive(prmActive);
            }

            prmGameObject.SetActive(prmActive);
        }

        //-----------------------------------------------------------------------
        public static void SetLayer(GameObject prmGameObject, int prmLayer)
        {
            for (int idx = 0; idx < prmGameObject.transform.childCount; ++idx)
            {
                GameObject child = prmGameObject.transform.GetChild(idx).gameObject;
                SetLayer(child, prmLayer);
                child.layer = prmLayer;
            }

            prmGameObject.layer = prmLayer;
        }

        //-----------------------------------------------------------------------
        /// Get model triangle Count
        //-----------------------------------------------------------------------
        public static double GetModelTrianglesCount(GameObject prmGameObject, bool bRecursion = false)
        {
            double triNums = 0;

            if (bRecursion)
            {
                for (int idx = 0; idx < prmGameObject.transform.childCount; ++idx)
                    triNums += GetModelTrianglesCount(prmGameObject.transform.GetChild(idx).gameObject, bRecursion);
            }

            MeshFilter meshFilter = prmGameObject.GetComponent<MeshFilter>();
            if (meshFilter != null)
            {
                triNums += meshFilter.mesh.triangles.Length / 3.0;
            }

            return triNums;
        }

        //-----------------------------------------------------------------------
        /// Get model vertex Count
        //-----------------------------------------------------------------------
        public static double GetModelVertexsCount(GameObject prmGameObject, bool bRecursion = false)
        {
            double vertexNums = 0;

            if (bRecursion)
            {
                for (int idx = 0; idx < prmGameObject.transform.childCount; ++idx)
                    vertexNums += GetModelVertexsCount(prmGameObject.transform.GetChild(idx).gameObject, bRecursion);
            }

            MeshFilter meshFilter = prmGameObject.GetComponent<MeshFilter>();
            if (meshFilter != null)
            {
                vertexNums += meshFilter.mesh.vertices.Length;
            }

            return vertexNums;
        }
    }
}