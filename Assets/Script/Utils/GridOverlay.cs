﻿using UnityEngine;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class GridOverlay : MonoBehaviour
    {
        private static Material _lineMaterial;
        private static Color _mainColor = new Color(1.0f, 0.1f, 0.1f, 1.0f);
        private static List<GameObject> _drawList = new List<GameObject>();

        private void CreateLineMaterial()
        {
            if (!_lineMaterial)
            {
                _lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
                                             "SubShader { Pass { " +
                                             "    Blend SrcAlpha OneMinusSrcAlpha " +
                                             "    ZWrite Off Cull Off Fog { Mode Off } " +
                                             "    BindChannels {" +
                                             "      Bind \"vertex\", vertex Bind \"color\", color }" +
                                             "} } }");
                _lineMaterial.hideFlags = HideFlags.HideAndDontSave;
                _lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
            }
        }

        private void OnPostRender()
        {
            CreateLineMaterial();
            _lineMaterial.SetPass(0);

            GL.Begin(GL.LINES);
            GL.Color(_mainColor);

            foreach (GameObject obj in _drawList)
            {
                if (obj)
                {
                    Vector3 minPoint, maxPoint;
                    Assist.GetAABBSize(obj, out minPoint, out maxPoint);

                    //  Create 8 Point
                    Vector3[] vecs = new Vector3[8];
                    vecs[0] = minPoint;
                    vecs[1] = new Vector3(minPoint.x, minPoint.y, maxPoint.z);
                    vecs[2] = new Vector3(maxPoint.x, minPoint.y, maxPoint.z);
                    vecs[3] = new Vector3(maxPoint.x, minPoint.y, minPoint.z);
                    vecs[4] = new Vector3(minPoint.x, maxPoint.y, minPoint.z);
                    vecs[5] = new Vector3(minPoint.x, maxPoint.y, maxPoint.z);
                    vecs[6] = maxPoint;
                    vecs[7] = new Vector3(maxPoint.x, maxPoint.y, minPoint.z);

                    //  Create 12 Lines
                    for (int idx = 0; idx < 4; ++idx)
                    {
                        GL.Vertex(vecs[idx]);
                        GL.Vertex(vecs[(idx + 1)%4]);
                    }

                    for (int idx = 0; idx < 4; ++idx)
                    {
                        GL.Vertex(vecs[idx + 4]);
                        GL.Vertex(vecs[(idx + 1)%4 + 4]);
                    }

                    for (int idx = 0; idx < 4; ++idx)
                    {
                        GL.Vertex(vecs[idx]);
                        GL.Vertex(vecs[idx + 4]);
                    }

                }

            }

            GL.End();

            //  Clear for next draw
            _drawList.Clear();
        }

        public static void DrawBox(GameObject prmGameObject)
        {
            if (prmGameObject != null)
                _drawList.Add(prmGameObject);
        }


    }
}