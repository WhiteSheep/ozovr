﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

namespace VBGames.PlayMaker.Actions
{
    public class CentralMonoBehaviour : MonoBehaviour
    {
        private static CentralMonoBehaviour _instance;

        public delegate IEnumerator CoroutineMethod();

        protected CentralMonoBehaviour()
        {
        }

        //-----------------------------------------------------------------------
        public static CentralMonoBehaviour Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CentralMonoBehaviour();
                return _instance;
            }
        }

        //-----------------------------------------------------------------------
        public void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }

        //-----------------------------------------------------------------------
        public void RunCoroutine(CoroutineMethod prmCoroutineMethod)
        {
            StartCoroutine(prmCoroutineMethod());
        }

        //-----------------------------------------------------------------------
        public void DontDestroyGameObject(GameObject prmGo)
        {
            DontDestroyOnLoad(prmGo);
        }

        //-----------------------------------------------------------------------
        public GameObject Instantiation(GameObject prmGo)
        {
            return Instantiate(prmGo);
        }

        //-----------------------------------------------------------------------
        public void DestroyObject(Object prmObject)
        {
            Destroy(prmObject);
        }

        //-----------------------------------------------------------------------
        public void DestroyAllChildren(GameObject prmParent)
        {
            for (int idx = prmParent.transform.childCount - 1; idx >= 0; --idx)
            {
                var obj = prmParent.transform.GetChild(idx).gameObject;
                obj.transform.parent = null;
                Destroy(obj);
            }
        }
    }
}