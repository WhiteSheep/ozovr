﻿using UnityEngine;
using System.Collections.Generic;

namespace VBGames.PlayMaker.Actions
{
    public class ConsoleCtrl : MonoBehaviour
    {

        public UITextList textList = null;
        private UIInput _input;

        // Use this for initialization
        private void Start()
        {
            _input = GetComponent<UIInput>();
            _input.label.maxLineCount = 1;
        }

        // Update is called once per frame
        private void Update()
        {

        }

        public void Submit()
        {
            if (textList != null)
            {
                string text = NGUIText.StripSymbols(_input.value);

                if (RunConsoleCommand(text))
                    AddtoTextList(text);
            }
        }

        public void AddtoTextList(string prmStr)
        {
            if (!string.IsNullOrEmpty(prmStr))
            {
                textList.Add(string.Format("[FFFF00][[FF0000]{0}[-]][-]-{1}", Time.frameCount, prmStr));
                _input.value = "";
            }
        }

        public bool RunConsoleCommand(string prmCmd)
        {
            bool validCmds = false;

            prmCmd = prmCmd.ToLower();
            string[] cmds = prmCmd.Split(' ');

            if (cmds[0].Equals("exit")) // exit
            {
                World.Clear();
                //UIController.Instance.SetStartMenuActive(true);
                validCmds = true;
            }
            else if (cmds[0].Equals("start")) // start with no scene
            {
                //UIController.Instance.SetStartMenuActive(false);
                validCmds = true;
            }

            if (!validCmds)
                AddtoTextList(string.Format("Unknown Command"));

            return validCmds;
        }

    }
}