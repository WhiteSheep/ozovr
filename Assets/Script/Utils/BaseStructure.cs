﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VBGames.PlayMaker.Actions
{
    //-----------------------------------------------------------------------
    /// RenderCommand 
    //-----------------------------------------------------------------------
    public class RenderCommand : IComparable<RenderCommand>
    {
        public string cmdID;
        public string roomID;

        public RenderCommand() { }

        public RenderCommand(string prmCmdId, string prmRoomId)
        {
            cmdID = prmCmdId;
            roomID = prmRoomId;
        }

        public int CompareTo(RenderCommand that)
        {
            return roomID.CompareTo(that.roomID);
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}", cmdID, roomID);
        }
    }

    //-----------------------------------------------------------------------
    /// Command Class is used to store every command in this application 
    //-----------------------------------------------------------------------
    public class Command
    {
        public string sUserID;
        public string sRoomID;
        public AppSetting.Command cType;
        public List<string> alParams;

        public Command(string cmd)
        {
            alParams = new List<string>();

            string[] cmdBlock = cmd.Split(AppSetting.CMD_SPLIT);

            try
            {
                if (cmdBlock.Length < 3)
                    throw new ArgumentException("Command params' count is not suitable");

                sUserID = cmdBlock[0];
                sRoomID = cmdBlock[1];
                cType = (AppSetting.Command) Enum.Parse(typeof (AppSetting.Command), cmdBlock[2]);

                for (int idx = 3; idx < cmdBlock.Length; ++idx)
                {
                    alParams.Add(cmdBlock[idx]);
                }
            }
            catch (Exception e)
            {
                DebugEx.Log(e.ToString());
            }
        }
    }

    //-----------------------------------------------------------------------
    /// Class MapListFilter contain a list and a dictionary, and can be used to
    /// deal with coordination between command recieve and run
    //-----------------------------------------------------------------------
    public class MapListFilter<TMapKeyType, TContentType>
    {
        // NextRoundCmds store the commands which will run on next frame
        protected Dictionary<TMapKeyType, TContentType> _mNextRoundCmds;

        public Dictionary<TMapKeyType, TContentType> NextRoundCmds
        {
            get { return _mNextRoundCmds; }
        }

        // WaitCmds store the commands which are still wait to run
        protected List<TContentType> _lWaitCmds;

        public List<TContentType> WaitCmds
        {
            get { return _lWaitCmds; }
        }

        public MapListFilter()
        {
            _mNextRoundCmds = new Dictionary<TMapKeyType, TContentType>();
            _lWaitCmds = new List<TContentType>();
        }

        public void Add(TContentType content)
        {
            _lWaitCmds.Add(content);
        }

        /// <summary>
        /// ThisDelegate insist a delegate function which contains a this pointer params
        /// </summary>
        /// <param name="_this"></param>
        /// <returns></returns>
        public delegate bool ThisDelegate(MapListFilter<TMapKeyType, TContentType> _this);

        protected ThisDelegate _funcPermitDelegate = null;

        public ThisDelegate FuncPermitDelegate
        {
            set { _funcPermitDelegate = value; }
        }


        /// <summary>
        /// ContentDelegate insist a delegate function which chontains a content param
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public delegate TMapKeyType ContentDelegate(TContentType content);

        protected ContentDelegate _funcCreateKey = null;

        public ContentDelegate FuncCreateKeyDelegate
        {
            set { _funcCreateKey = value; }
        }


        protected virtual bool IsPermit()
        {
            if (_funcPermitDelegate == null)
                return false;
            else
                return _funcPermitDelegate(this);
        }

        public virtual void PermitIn()
        {
            //  Remove latest frame commands
            _mNextRoundCmds.Clear();

            while (IsPermit() && _lWaitCmds.Count > 0)
            {
                TContentType value = _lWaitCmds[0];
                _lWaitCmds.RemoveAt(0);

                try
                {
                    if (_funcCreateKey == null)
                        throw new Exception("CreateKeyDelegate must have a value");

                    TMapKeyType key = _funcCreateKey(value);
                    _mNextRoundCmds[key] = value;
                }
                catch (Exception e)
                {
                    DebugEx.Log(e.ToString());
                }
            }
        }

        public delegate void KeyContentDelegate(TMapKeyType key, TContentType content);

        public virtual void Foreach(KeyContentDelegate function)
        {
            foreach (var item in _mNextRoundCmds)
            {
                function(item.Key, item.Value);
            }
        }

        public int WaitCount()
        {
            return _lWaitCmds.Count;
        }
    }

    /// <summary>
    /// DoubleDictionary contains two keys and one value
    /// </summary>
    /// <typeparam name="TK1">key 1 type</typeparam>
    /// <typeparam name="TK2">key 2 type</typeparam>
    /// <typeparam name="T">value type</typeparam>
    public class DoubleDictionary<TK1, TK2, T>
    {
        protected Dictionary<KeyValuePair<TK1, TK2>, T> _doubleKeyDic;

        public Dictionary<KeyValuePair<TK1, TK2>, T> All
        {
            get { return _doubleKeyDic; }
        }

        public DoubleDictionary()
        {
            _doubleKeyDic = new Dictionary<KeyValuePair<TK1, TK2>, T>();
        }

        public void Add(TK1 prmK1, TK2 prmK2, T prmVal)
        {
            _doubleKeyDic[new KeyValuePair<TK1, TK2>(prmK1, prmK2)] = prmVal;
        }

        public T GetValue(TK1 prmK1, TK2 prmK2)
        {
            T retVal;
            _doubleKeyDic.TryGetValue(new KeyValuePair<TK1, TK2>(prmK1, prmK2), out retVal);

            return retVal;
        }

        public void Clear()
        {
            _doubleKeyDic.Clear();
        }
    }

    /// <summary>
    /// Serializable Dictionary
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField] private List<TKey> keys = new List<TKey>();

        [SerializeField] private List<TValue> values = new List<TValue>();

        // save the dictionary to lists
        public void OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();
            foreach (KeyValuePair<TKey, TValue> pair in this)
            {
                keys.Add(pair.Key);
                values.Add(pair.Value);
            }
        }

        // load dictionary from lists
        public void OnAfterDeserialize()
        {
            this.Clear();

            if (keys.Count != values.Count)
                throw new System.Exception(
                    string.Format(
                        "there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable."));

            for (int i = 0; i < keys.Count; i++)
                this.Add(keys[i], values[i]);
        }
    }
}