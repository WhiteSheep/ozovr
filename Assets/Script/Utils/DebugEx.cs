﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VBGames.Utils.Logger;
using Object = UnityEngine.Object;

namespace VBGames.PlayMaker.Actions
{
    public static class DebugEx
    {
        public static bool Mode = true;
        public static bool PrintToConsole = false;

        public static void Update()
        {
            MemoryDectect();
        }

        //-----------------------------------------------------------------------
        public static void Log(object prmMsg)
        {
            if (Mode)
            {
                if (PrintToConsole)
                {
                    VBLogger.Log(prmMsg.ToString());
                    /*string msg = string.Format("[FFFF00][Log,{0}][-]-{1}",
                        Time.frameCount, prmMsg);*/
                    //UIController.Instance.PrintMsgToConsole(msg);
                }
                VBLogger.Log(prmMsg.ToString());
            }
        }

        //-----------------------------------------------------------------------
        public static void LogError(object prmMsg)
        {
            if (Mode)
            {
                if (PrintToConsole)
                {
                    VBLogger.Err(prmMsg.ToString());
                    /*string msg = string.Format("[FF0000][Error,{0}][-]-{1}",
                        Time.frameCount, prmMsg);*/
                   // UIController.Instance.PrintMsgToConsole(msg);
                }

                VBLogger.Err(prmMsg.ToString());
            }
        }

        //-----------------------------------------------------------------------
        public static string CreateErrMsg(int prmCode, string prmInfo)
        {
            return string.Format("error code {0} : {1}", prmCode, prmInfo);
        }

        //-----------------------------------------------------------------------
        public static void GcNow()
        {
            GC.Collect();
            Log("Gc collection complete");
        }

        //-----------------------------------------------------------------------
        public static void MemoryDectect()
        {
            //  Memory Detected
            object[] objects = Object.FindObjectsOfType(typeof (Object));

            var dictionary = new Dictionary<string, int>();

            foreach (var obj in objects)
            {
                var key = obj.GetType().ToString();
                if (dictionary.ContainsKey(key))
                {
                    dictionary[key]++;
                }
                else
                {
                    dictionary[key] = 1;
                }
            }

            var myList = new List<KeyValuePair<string, int>>(dictionary);
            myList.Sort((firstPair, nextPair) => nextPair.Value.CompareTo(firstPair.Value));

            var inspectorText = GameObject.Find("MemLeakText");
            var showText = "";
            myList.ForEach(entry => { showText += entry.Key + ": " + entry.Value + "\n"; });

            inspectorText.GetComponent<Text>().text = showText;
        }
    }
}