<?php
	$file = $_FILES["file"]["tmp_name"];
	$filename = $_FILES["file"]["name"];
	
	$fh = fopen($file, 'r');
	$content = '';
	while (!feof($fh))
	{
		$content .= fgets($fh);
	}
	fclose($fh);
	
	$path = 'UploadFiles/';
	$fp = fopen($path . $filename, 'w+');
	fwrite($fp, $content);
	fclose($fp);
	
	$xmlDoc = new DOMDocument();
	$xmlDoc->load('UploadFiles/UploadReturn.xml');
	header("Content-type: text/xml");
	print $xmlDoc->saveXML();
?>
	
