<?php
	header("Access-Control-Allow-Origin: *"); 
	header("Access-Control-Allow-Credentials: true"); 
	header("Access-Control-Allow-Headers: Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time"); 
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
	$xmlDoc = new DOMDocument();
	$xmlDoc->load('UploadFiles/MournPerson.xml');
	header("Content-type: text/xml");
	print $xmlDoc->saveXML();
?>