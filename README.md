※※※※※※※※※关于操作※※※※※※※※※

公共：
BackQuote（~键） 打开/关闭Console
WASD 方向键移动

管理员：
C 开启创造模式 （这时数字1-3按键可以选择物品，鼠标左键放置，滚轮旋转）
E 开启删除模式 （鼠标左键移除）
I 空状态
F10 打开系统菜单

用户：（用户当前也能调用管理员功能，没有区分）
拖动鼠标左键 移动镜头


※※※※※※※※※程序启动顺序※※※※※※※※※

1、读取基本配置信息
2、读取用户信息（需要配置用户信息获取接口）
3、读取当前访问场景信息（需要配置当前页面js的场景获取方法，并在方法中调用对应的方法返回结果）
4、初始化单位模板管理类
5、判定用户权限 —— 加载场景（需要配置场景布置文件获取接口）
6、获取到场景布置文件后分析并开始加载场景，加载结束后程序即可正常运行


※※※※※※※※※关于程序的配置※※※※※※※※※

1、将OzovrTest文件夹迁移到所要部署的目录下
2、index.html是UnityWebgl展示页面，需要修改的配置都在同目录下的config.xml中，请勿改动配置文件的位置，webgl会自动识别当前路径根据相对路径找到config.xml
3、config.xml中需要配置的内容如下
	（1）DatasConfig下的DataBasePath请修改到对应位置
	（2）InterfaceConfig下的InterfaceBasePath是接口地址的统一基址，请根据实际情况修改
	（3）InterfaceConfig下的其他信息是接口的具体位置，接口内容详见xml注释，请根据实际情况进行修改，内部自带的测试接口由php完成，可供参考

	
※※※※※※※※※关于Apache服务器的配置※※※※※※※※※

因为Webgl的www访问机制实际上是采用js的httprequest，因此会出现跨域访问的问题，作为提供接口的java + apache服务器，应该配置相关的CORS，方法如下：

1、找到Apache目录下的conf文件夹，并找到httpd.config文件
2、找到LoadModule headers_module modules/mod_headers.so去掉前面的#使其生效
3、找到如下内容
<Directory />
    AllowOverride none
    Require all denied
</Directory>

修改为

<Directory />
    # AllowOverride none
    Require all denied
	Header set Access-Control-Allow-Origin *
	Header set Access-Control-Allow-Credentials true
	Header set Access-Control-Allow-Credentials "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time"
	Header set Access-Control-Allow-Credentials "GET, POST, OPTIONS"
</Directory>

4、上述配置完成后请重启Apache以让所有配置生效
5、如果还是不行，请在对应的服务器端口代码中加入如下内容（下面的内容是PHP的header添加形式，其他语言请自己查询）
<?php
	header("Access-Control-Allow-Origin: *"); 
	header("Access-Control-Allow-Credentials: true"); 
	header("Access-Control-Allow-Headers: Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time"); 
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
?>

※※※※※※※※※关于XML格式问题※※※※※※※※※

服务器在接收xml保存的时候请注意，务必保存成UTF8无Bom版本，有的时候系统自动保存Xml的时候会存储为UTF8有Bom版本，这会导致Unity3d无法识别该Xml

※※※※※※※※※其他问题※※※※※※※※※

如果有其他问题直接找我本人吧